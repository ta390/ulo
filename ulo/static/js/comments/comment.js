/* Profile Page Javascript File */
/* Dependencies: jQuery */

/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";
/* ------------------------------------------------------------------------------------ */
	
	function submit(e){

		e.preventDefault()


		// TO DO !!!! if text area is empty return

		var request = {
			type: "POST",
			url: $(this).attr("action"),
			data: $(this).serialize(),
			cache:false,
		}

		$.ajax(request)
			/* params: server data, status code, xhr */
			.done(function(url, sc, xhr){
				/* Redirect successful requests */
				console.log('success: ', url)
				// Reload the current page, without using the cache
				document.location.reload(true);
			})
			/* params: xhr, status code, error type */
			.fail(function(xhr){
				console.log('failed');
				var json = $.parseJSON(xhr.responseText);

			});
	}

	function enable_submit(e){

		// var emptyRegExp = /^[\s]*$/;
		// var backspaceKey = (e.keyCode===8 || e.keyCode===46)
		// var len = this.value.length

	}

	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */

	$(function(){

		/* SEE: https://developer.mozilla.org/en-US/docs/Web/Events/input */
		/* NEED TO HANDLE COMPATIBILITY ISSUES IF USING 'INPUT' EVENT */
		$("#comment_form > #id_text").on("input", function(e){

			var empty_regex = /^[\s]*$/;

			if(empty_regex.test(this.value)){
				$("#comment_form > #id_submit").attr('disabled', 'true')
			} else{
				$("#comment_form > #id_submit").removeAttr('disabled')
			}

		})

		$("#comment_form").on("submit", submit)


	});

/* ------------------------------------------------------------------------------------ */
}());