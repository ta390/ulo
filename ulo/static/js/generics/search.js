/* Search page javascript file */
/* Dependencies: jQuery */


/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";
/* ------------------------------------------------------------------------------------ */
	
	function Search(){

		

		var context = $("#search_container");
		this.search_box = $("#search_input", context);
		this.container = $("#search_suggestions", context);

		$(window).one("beforeunload", {search_box: this.search_box}, this.dumpSearchValue);
		this.autocomplete = new Autocomplete(this.search_box[0], this.container[0]);
		this.filters = new Filters(context);

		$("#search_form", context).on("submit", {self: this}, this.submit_handler)

	}

	Search.prototype = {

		dumpSearchValue: function(e){
			$("#nav_search #search_input").val( e.data.search_box.val() );
		},
		userHTML: function(user, following){
			var following = ($.inArray(user.pk, following) >= 0) ? 'following' : 'not_following'
			return "<img src='"+user.picture+"'><span>"+following+"</span> \
				<div>"+user.name+"</div> \
			<div>"+user.username+"</div>"
		},
		postHTML: function(post){
			return "<h3>"+post.title+"</h3> \
				<div><img src='"+post.thumbnail+"'></div> \
				<span>"+post.name+"</span> \
				<span>"+post.username+"</span>"
		},
		submit_handler: function(e){
			
			var self = e.data.self;
			var value = self.search_box.val().trim();

			if( value !== "" ){
				
				self.autocomplete.cancel();

				var sc =  {
					500: function() {
						// display flash message for server error
					}
				}
				var url = $(this).attr('action')+"?q="+fixedEncodeURIComponent(value);
				url+= self.filters.filter_query();

				console.log(url)
				$.ajax({type:"GET", url:url, timeeout:30, cache:true, statusCode: sc})
				
					.done(function(data, sc, xhr){

						var results = $("#results");

						var following = data.following;
						var user_results = data.user_results;
						var post_results = data.post_results;

						var result, i;
						for(i in user_results){
							result = user_results[i];
							results.append(self.userHTML(result));
						}

						for(i in post_results){
							result = post_results[i];
							results.append(self.postHTML(result));
						}

					})
					.fail(function(){
				})

			}


			e.preventDefault();

		}
	}

	function Filters(context){

		this.container = $("#options", context);

		this.type_filter = $("#id_type_filter", this.container);
		this.type_filters = $("li", this.type_filter);
		
		this.date_filter = $("#id_date_filter", this.container);
		this.date_filters = $("li", this.date_filter);
		
		this.sort_filter = $("#id_sort_filter", this.container);
		this.sort_filters = $("li", this.sort_filter);


		this.remove_html = "<button class='remove_filter' type='button'> \
				<span class='hide'>Remove filter</span> \
			</button>"
		$("#type_filter_heading").append(this.cancel_html);

		$("#filter_btn", context).on(click_events, {self: this}, this.toggle_filters);
		this.type_filters.on(click_events, {self: this, key: 'type_filter'}, this.select_filter);
		this.date_filters.on(click_events, {self: this, key: 'date_filter'}, this.select_filter);
		this.sort_filters.on(click_events, {self: this, key: 'sort_filter'}, this.select_filter);

	}
	Filters.prototype = {
		toggle_filters: function(e){
			e.data.self.container.toggleClass("hide");
		},
		filter_query: function(){
			var q = "&type="+this.type_filter.attr('data-filter');
			q += "&date="+this.date_filter.attr('data-filter');
			q += "&sort="+this.sort_filter.attr('data-filter');
			return q;
		},
		select_filter: function(e){

			var elem = $(this);
			if(elem.hasClass('filter_disabled')){
				return;
			}

			var container = e.data.self[e.data.key];
			container.attr('data-filter', elem.attr('data-value'));
			elem.siblings(".active").removeClass('active')
			elem.addClass('active');

			
			e.data.self.addRemoveButton(container, e.data.key);
			e.data.self.disableFilters(elem);


		},
		addRemoveButton: function(container, key){

			var btn = $('button.remove_filter', container);
			if(btn.length === 0){
				$('.heading', container).append(this.remove_html);
				$('button.remove_filter', container).one(
					'click', {self: this, key: key}, this.remove_handler
				)
			}
			
		},
		remove_handler: function(e){
			var container = e.data.self[e.data.key];
			container.removeAttr('data-filter').children().removeClass('active');
			$(this).remove();
			var elem = $("li.active:first", this.container);
			e.data.self.disableFilters(elem);

		},
		disableFilters: function(elem){

			if(elem.length === 0){

				$("li", this.container).removeClass('filter_disabled');

			} else if(elem.is("[data-type='account']")){
				$("li:not([data-type='account'])", this.container).addClass('filter_disabled')
				$("li[data-type='account']", this.container).removeClass('filter_disabled')
			} else{
				
				$("li:not([data-type='account'])", this.container).removeClass('filter_disabled')
				$("li[data-type='account']", this.container).addClass('filter_disabled')
			}
		}

	}


	/* DOCUMENT READY FUNCTION */
	/* -------------------------------------------------------------------------------- */
	$(function(){
		try{

			new Search();
			

		} catch(e){
			debug(e);
		}
	});

/* ------------------------------------------------------------------------------------ */
}());



// (function () {
// "use strict";

// 	var suggestions = $("#es")

// 	/* DOCUMENT READY FUNCTION */
// 	/* ------------------------------------------------------------------------------------ */
// 	$(function(){

		

// 		$("#search_input").on("input", function(e){

// 			e.preventDefault()

// 			if (/^[\s]*$/.test(this.value)){
// 				suggestions.html('')
// 				return
// 			}

// 			var request = {
// 				type: "GET",
// 				url: $("#search_form").attr("action"),//+"video/",
// 				data: "q="+this.value,
// 			}

// 			/* WHEN THE FILE IS LARGE YOU WILL GET A NETWORK ERROR SO DISPLAY ERROR MESSAGE FOR IT */
// 			 SEE SERVER SIDE FOR WHAT CAUSES IT. USUALLY FILE TOO BIG OR RESOLUTION TOO BIG OR TYPE 
// 				NOT SUPPORTED 

// 			$.ajax(request)
// 				.done(function(options, sc, xhr){
					
// 					var data = ''
// 					for (var i in options.html){
// 						data += "<div>"+options.html[i].text+"</div>"
// 					}
// 					suggestions.html(data)

// 				})
// 				/* params: xhr, status code, error type */
// 				.fail(function(xhr){
// 					/* display error message for all fields */
// 					var json = $.parseJSON(xhr.responseText);
// 					console.log(json)					
// 			});
			
// 		})


// 	});

// }());