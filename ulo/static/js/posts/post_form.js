/* Profile Page Javascript File */
/* Dependencies: jQuery */

/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";
/* ------------------------------------------------------------------------------------ */
	
	function submit(e){
		e.preventDefault()
		var request = {
			type: "POST",
			url: $("#post_form").attr("action"),//+"video/",
			data: new FormData($("#post_form")[0]),
			processData: false, // Don't process the files
			contentType: false
		}

		console.log(request.url)

		/* WHEN THE FILE IS LARGE YOU WILL GET A NETWORK ERROR SO DISPLAY ERROR MESSAGE FOR IT */
		/* SEE SERVER SIDE FOR WHAT CAUSES IT. USUALLY FILE TOO BIG OR RESOLUTION TOO BIG OR TYPE 
			NOT SUPPORTED */

		$.ajax(request)
			.done(function(url, sc, xhr){
					console.log('success: ', url)
				})
				/* params: xhr, status code, error type */
				.fail(function(xhr){
					/* display error message for all fields */
					var json = $.parseJSON(xhr.responseText);
					console.log(json)					
				});
	}

	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */

	$(function(){

		$("#id_title").on("keydown", function(e){
			//console.log(this.value)
		})

		/* If user hits the enter key to submit the form */
		$("#post_form").on("submit", submit)
		/* If user submits the form by pressing the button */
		$("#id_submit").on("click", submit)


	});

/* ------------------------------------------------------------------------------------ */
}());