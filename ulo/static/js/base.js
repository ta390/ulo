/* Site wide javascript file */
/* Dependencies: jQuery */


/* HANDLE ANY UNCAUGHT EXCEPTIONS */
/* ------------------------------------------------------------------------------------ */
$(window).on("error", function(e){});


/****************************************************************************************/
/* GLOABAL VARIABLES */
/****************************************************************************************/

/* list of click/touch events used when creating click/touch events through jQuery */
var click_events = "click";

/* global handle to the autcomplete class */
var Autocomplete = null;

/****************************************************************************************/
/* END GLOABAL VARIABLES */
/****************************************************************************************/


/****************************************************************************************/
/* GLOABAL HELPER FUNCTIONS */
/****************************************************************************************/

/* DEBUG */
/* ------------------------------------------------------------------------------------ */
var _debug = "mobile";

function debug(e){
	
	e = "DEBUG: "+e

	if(_debug==="browser"){
		console.log(e);
	} else if(_debug==="mobile"){
		alert(e);
	}
}


/* IE SUPPORT */
/* ------------------------------------------------------------------------------------ */
/*
	Function to test the version of IE and return true if the browser is a version
	less than 'no'. Only supports detection of <= 8, 9, or 10.
	@param no: ie version number 
*/
function isIeLessThanEqual(no){
	if(no === 8){
		return (document.all && !document.addEventListener);
	} else if (no === 9){
		return (document.all && !window.atob);
	} else if(no === 10){
		return !!(document.all)
	}
	throw new "IE Version Support only tests for 8, 9, and 10";
}


/* ATTRIBUTE SUPPORT */
/* ------------------------------------------------------------------------------------ */
/*
	Return true if the browser supports a particular feature or false otherwise.
	@param el: name of the element to test the attribute for (e.g. input)
	@param attr: name of the attribute to test for (e.g. mulitple)
*/
function attributeSupported(el, attr){
	var a = document.createElement(el);
	return a[attr] !== undefined;
}

/* EVENT SUPPORT */
/* ------------------------------------------------------------------------------------ */
/*
	http://perfectionkills.com/detecting-event-support-without-browser-sniffing/
*/
var isEventSupported = (function(){
	
	var TAGNAMES = {
		'select':'input','change':'input', 'input': 'input',
		'submit':'form','reset':'form',
		'error':'img','load':'img','abort':'img'
	}
	
	function isEventSupported(eventName) {
		
		var el = document.createElement(TAGNAMES[eventName] || 'div');
		eventName = 'on' + eventName;
		var isSupported = (eventName in el);
		
		if (!isSupported) {
			el.setAttribute(eventName, 'return;');
			isSupported = typeof el[eventName] == 'function';
		}
		
		el = null;
		return isSupported;
	}
	return isEventSupported;
})();

/* EVENT BINDING */
/* ------------------------------------------------------------------------------------ */
/*
	Return true if an element does not have an event bound to it or false otherwise.
	@param el: a document element (e.g. the value returned by document.getElementById)
	@param evt: name of the event to test for (e.g. "click")
*/
function isEventUnbound(el, evt){
	 var evts = $._data(el, "events");
	 return !evts || !evts[evt];
}


/* ROUNDING VALUES */
/* ------------------------------------------------------------------------------------ */

/* 
	Functions to round the value n to the nearest m
*/
function roundTo(n, m){
	/* round to the nearest m */
	return Math.round(n/m)*m;
}
function roundUpTo(n, m){
	/* round up to the nearest m */
	return Math.ceil(n/m)*m;
}
function roundDownTo(n, m){
	/* round down to the nearest m */
	return Math.floor(n/m)*m;
}


/* ENCODING */
/* ------------------------------------------------------------------------------------ */
/* 
	https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/encodeURI.
	(5-Feb-16)
*/
function fixedEncodeURI (str) {
    return encodeURI(str).replace(/%5B/g, '[').replace(/%5D/g, ']');
}
/*
	https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/
		encodeURIComponent.
	(5-Feb-16)
*/
function fixedEncodeURIComponent (str) {
  return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
    return '%' + c.charCodeAt(0).toString(16);
  });
}

/****************************************************************************************/
/* END GLOABAL HELPER FUNCTIONS */
/****************************************************************************************/



/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";
/* ------------------------------------------------------------------------------------ */
	/* jQuery animation switch */
	//$.fx.off = true;

	/* VARIABLES */
	/* ------------------------------------------------------------------------------------ */

	/* reference to the Page class */
	var PageLoader = null;


	/* CSRF TOKEN */
	/* ------------------------------------------------------------------------------------ */
	/*
		Return the cookie value of 'name' or null.
		@param name: name of the cookie.
	*/
	function getCookie(name) {
		var cookieValue = null;
		if (document.cookie && document.cookie != '') {
			var cookies = document.cookie.split(';');
			for (var i = 0; i < cookies.length; i++) {
				var cookie = jQuery.trim(cookies[i]);
				// Does this cookie string begin with the name we want?
				if (cookie.substring(0, name.length + 1) == (name + '=')) {
					cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
					break;
				}
			}
		}
		return cookieValue;
	}
	var csrftoken = getCookie('csrftoken');
	/*
		Return true if the http request method does not require a csrf token.
		@param method: http method.
	*/
	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
	/*
		Set the header of the ajax request to include the token.
	*/
	$.ajaxSetup({
		beforeSend: function(xhr, settings) {
			if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
				xhr.setRequestHeader("X-CSRFToken", csrftoken);
			}
		}
	});


	/* AJAX PAGE LOADER */
	/* ------------------------------------------------------------------------------------ */
	/*
		Manage page changes using the HTML5 History API.
	*/
	function Page(){

		if (window.history && window.history.pushState){

			/* params: state object, title, url (set to the current url if not specified) */
			history.replaceState({}, document.title);

			$(window).on("popstate", this.history_handler);
			
			this.register();
			
			return this;
		}

		return null
	}
	Page.prototype = {
		
		constructor: Page,

		/*
			Register a click event on all <a> tags. Links marked with the data attribute
			data-ex are excluded.
			@param context: optional context passed to jQuery when searching for <a> tags.
			to narrow down the scope and stop multiple events being added to menu links.
		*/
		register: function(context){

			$("a:not([data-ex])", context).on(click_events, this.page_load);
			return context;
		},
		/*
			When a user moved backwards or forwards through their browser history update
			the contents of the page without performing a full page refresh.
		*/
		history_handler: function(e){
			try{

				if(e.originalEvent.state !== null){

					/* PageLoader is the file wide reference to this class */
					PageLoader._ajax_request(document.location.pathname, false);
					e.preventDefault();
				}

			} catch(e){
				debug(e);
			}
		},
		/*
			When a link is clicked perform an ajax get request for the page and use the html 
			to update the current page without performing a full page refresh.
		*/
		page_load: function(e){
			try{

				/* PageLoader is the file wide reference to this class */
				PageLoader._ajax_request(e.currentTarget.href, true);
				e.preventDefault();

			} catch(e){
				debug(e);
			}
		},
		/*
			Refresh the page using the value of the returned url (curr_url)
			@param prev_url: the requested url
			@param curr_url: the returned url
			@param force: boolean to force a refresh.
		*/
		refresh_page: function(prev_url, curr_url, force){

			if(force === true || /\/logout\/$/.test(prev_url)){
				window.location.replace(curr_url);
				return true;
			}
			return false;
		},
		/*
			Update the browser history if the url has changed and push_state is true.
			@param url: url returned by the ajax get request.
			@param push_state: boolean to indicate if the url should be added to the history
			instead of replacing the current entry.
		*/
		update_history: function(url, push_state){
			try{

				if(location.pathname+location.search !== url){
					if(push_state === true){
						history.pushState({}, document.title, url);
					} else{
						history.replaceState({}, document.title, url);
					}
				}
			} catch(e){}
		},
		/*
			Get a new page and update the content, css and javascript without performing a 
			full page refresh.
			@param url: get request url.
			@param push_state: boolean to indicate if the url should be added to the history.
		*/
		_ajax_request: function(url, push_state){

			var sc =  {
				500: function() {
					// display flash message for server error
				}
			}

			$.ajax({type:"GET", url:url, timeeout:30, cache:true, statusCode: sc})
				
				.done(function(data, sc, xhr){

					/* PageLoader is the file wide reference to this class */
					if(PageLoader.refresh_page(url, data.url, false) === false){

						$(window).trigger('beforeunload');
					
						var div = $("<div>").append(data.html);
						var content = div.find("#content").addClass("hidden");
						var head = div.find("#page_head");
						var js = div.find("#page_js");
						
						// var js_files = document.getElementById("page_js");
						// while (js_files.firstChild) {
						// 	js_files.removeChild(js_files.firstChild);
						// }

						// while (js.firstChild) {
						// 	js_files.appendChild(js.firstChild);
						// }


						$.when(
							$("#page_head").replaceWith(head),
							$("#content").replaceWith(content),
							$("#page_js").replaceWith(js)
						).done(function(){

								/* PageLoader is the file wide reference to this class */
								PageLoader.register($("#content")).removeClass("hidden");
								PageLoader.update_history(data.url, push_state);

						}).fail(function(a,b,c){
							console.log(a,b,c)
						});

						
					}
					
				})
				.fail(function(){
			})

		}
	}



	/* AUTOCOMPLETE */
	/* ------------------------------------------------------------------------------------ */
	/*
		Manage the fetch/display/hide cycle of search suggestions as a user types into a
		seach input field.
		@param search_input: javascript search input field.
		@param container: reference to the container used to show suggestions.
	*/
	function _Autocomplete(search_input, container){

		/* request url */
		this.url = "/search/autocomplete/";

		/* setTimeout return value - used to cancel a timeout function */
		this.timeout_id = null;
		/* current request object - used to abort requests */
		this.jqxhr = null;
		/* store the id of the search input field */
		this.search_id = search_input.id
		/* store reference to the container */
		this.container = $(container);

		
		/* EVENTS */
		$(search_input).on("input", {self: this}, this.timeout);
			//.on("focus", {self: this}, this.showSuggestionsHandler);

	}
	_Autocomplete.prototype = {
		
		constructor: _Autocomplete,

		/* AJAX REQUEST */
		/* ------------------------------------------------------------------------------------ */
		/*
			Send an ajax with the query string to fetch autocomplete suggestions.
			@param q: query string.
		*/
		autocomplete: function(q){

			var self = this;

			var request = {
				type: "GET",
				url: self.url+q,
				cache:false,
			}
			self.jqxhr = $.ajax(request)
				.done(function(data, sc, xhr){

					var posts = self.createList(data.post_suggestions, self.createPostHTML);
					var users = self.createList(data.user_suggestions, self.createUserHTML);
					self.updateSuggestions(posts, users);

				})
				.fail(function(xhr){

					self.hideSuggestions();
			});
		},

		abort: function(){
			this.jqxhr && this.jqxhr.abort();
		},

		cancel: function(){
			this.abort();
			clearTimeout(this.timeout_id);
			this.hideSuggestions();
		},

		/* END AJAX REQUEST */
		/* ------------------------------------------------------------------------------------ */


		/* EVENT HANDLERS */
		/* ------------------------------------------------------------------------------------ */
		/*
			Set a delay on each request to give the user time to complete part of their search.
		*/
		timeout: function(e){

			var self = e.data.self;

			clearTimeout(self.timeout_id);
			self.abort()

			if(/^\s*$/.test(e.target.value) === false){
				self.timeout_id = setTimeout(
					function(){ self.autocomplete(e.target.value) }, 300
				);
			} else{
				self.hideSuggestions().empty();
			}
		},
		/*
			Show the current suggestions and add a document click event to hide the suggestions
			if the user clicks outside of the search input field.
		*/
		// showSuggestionsHandler: function(e){
		// 	e.data.self.showSuggestions();
		// 	e.data.self.addCloseHandler();
		// },
		/*
			Hide the current suggestions if the user clicks outside of the search input field.
		*/
		hideSuggestionsHandler: function(e){

			if(e.target.id !== e.data.self.search_id){
				e.data.self.hideSuggestions();
				$(document).off(click_events);
			}
		},
		/*
			Add a document event to call hideSuggestionsHandler()
		*/
		addCloseHandler: function(){
			$(document).on(
				click_events, 
				{self: this}, 
				this.hideSuggestionsHandler
			);
		},

		/* END EVENT HANDLERS */
		/* ------------------------------------------------------------------------------------ */


		/* PRESENTATION/DISPLAY */
		/* ------------------------------------------------------------------------------------ */

		createList: function(data, callback){
			var ul = $("<ul class='suggestions' role='listbox'>");
			for(var i in data[0].options){
				var info = data[0].options[i];
				ul.append(callback(info));
			}
			return ul;
		},
		createSearchAll: function(){
			return "<ul class='search_all'> \
				<li><a href='#'>Search all posts</a></li> \
				<li><a href='#'>Search all users</a></li> \
			</ul>"
		},

		createPostHTML: function(post){
			return "<li class='post'> \
				<a href='/post/"+post.payload.id+"/' rel='ignore'> \
					<span class='text'>"+post.text+"</span> \
				</a> \
			</li>"

		},
		createUserHTML: function(user){
			return "<li class='user'> \
				<a href='/user/"+user.text+"/' rel='ignore'> \
					<span class='image'><img src='"+user.payload.picture+"'></span> \
					<span class='text'> \
						<span class='name'>"+user.payload.name+"</span> \
						<span class='username'>"+user.text+"</span> \
					</span> \
				</a> \
			</li>"
		},
		showSuggestions: function(){
			return this.container.addClass("show");
		},
		hideSuggestions: function(){
			return this.container.removeClass("show");
		},

		updateSuggestions: function(posts, users){
			
			if(posts.is(":empty") && users.is(":empty")){
				
				this.hideSuggestions().empty();
			
			} else{
				
				var search_all = this.createSearchAll()
				this.hideSuggestions().empty().append(posts, users, search_all);
				
				/* PageLoader is the file wide reference to the Page class */
				if(PageLoader){
					PageLoader.register( this.container );
				}

				this.showSuggestions();
				this.addCloseHandler();
				
			}
		},
	}
	/* END PRESENTATION/DISPLAY */
	/* ------------------------------------------------------------------------------------ */

	/* GLOBAL AUTOCOMPLETE HANDLE */
	/* ------------------------------------------------------------------------------------ */
	
	Autocomplete = _Autocomplete;
	
	/* ------------------------------------------------------------------------------------ */


	/* SEARCH */
	/* ------------------------------------------------------------------------------------ */
	/*
		Manage the opening, closing and submission of the nav bar search form.
	*/
	function NavSearch(){

		var search = $("#nav_search");
		var search_box = $("#search_input", nav_search);

		var context = {
			search: search,
			search_box: search_box,
			toggle_class: "open_search",
			autocomplete: new Autocomplete(search_box[0], $("#search_suggestions", nav_search)[0])
		};

		$("#search_form", search).on("submit", context, this.search_handler);
		$("#close_search", search).on(click_events, context, this.close_handler);
	}
	NavSearch.prototype = {

		constructor: NavSearch,
		/*
			When the search button is clicked perform the required action depending on its
			current state. Action include opening the search box, submitting the search form
			or ignoring the click if the search input is empty.
		*/
		search_handler: function(e){

			var url = $(this).attr("action");
			
			if(new RegExp(url).test(location.pathname)){

				PageLoader._ajax_request(url, true);
			
			} else{

				var search_box = e.data.search_box;
			
				if(search_box.is(":visible")){

					e.data.autocomplete.cancel();

					var value = search_box.val().trim();

					if( value !== "" ){
						
						/* do not preventDefault if we have a value but the Page class is null */
						if(!PageLoader){
							return
						}
						
						e.data.search.removeClass(e.data.toggle_class);
						url += "?q="+fixedEncodeURIComponent(value);
						PageLoader._ajax_request(url, true);

					}

				} else{

					e.data.search.addClass(e.data.toggle_class);
					search_box.focus();
					e.stopPropagation();
				}
			}

			/* the function will return early if preventDefault is not required */
			e.preventDefault();

		},
		/*
			Close the open search box.
		*/
		close_handler: function(e){

			e.data.search.removeClass(e.data.toggle_class);
			e.preventDefault();
			
		},

	}



	/* SUB MENU */
	/* ------------------------------------------------------------------------------------ */
	/*
		Toggle the sub meuu.
	*/
	function SubMenu(){

		this.sub_menu = $("#nav_sm");
		this.btn_id = "nav_sm_btn";
		this.toggle_class = "hide";
		this.register();
		
	}
	SubMenu.prototype = {

		constructor: SubMenu,

		/*
			Register a one time use click event on the sub menu button which toggles its 
			display.
		*/
		register: function(){

			$("#"+this.btn_id).one(click_events, {self: this}, this.openSubMenu);
		},
		/*
			Show the sub menu adding a one time use document click event to close it.
		*/
		openSubMenu: function(e){

			var self = e.data.self;
			self.sub_menu.removeClass(self.toggle_class);
			$(document).one(click_events, {self: self}, self.closeSubMenu);

			/* prevent the event from bubbling */
			e.stopPropagation();
			e.preventDefault();
		},
		/*
			Close the sub menu adding a one time use click event to the button that opens it.
		*/
		closeSubMenu: function(e){

			var self = e.data.self;
			self.sub_menu.addClass(self.toggle_class);
			self.register();

			if(e.target.id === self.btn_id){ 
				e.preventDefault();
			}
		}
	}


	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */
	$(function(){
		try{
			
			try{
				/* Polyfiller to remove the 300ms delay on mobile browsers */
				Origami.fastclick(document.body);
			} catch(e){}

			/* update each page with an ajax call */
			PageLoader = new Page();
			/* toggle nav search and submission */
			new NavSearch();
			/* toggle drop down menu */
			new SubMenu();


		} catch(e){
			debug(e);
		}
	});

/* ------------------------------------------------------------------------------------ */
}());

