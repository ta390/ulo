/* Profile Page Javascript File */
/* Dependencies: jQuery */

/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";
/* ------------------------------------------------------------------------------------ */
	
	function cssPropertySupport(p) {
		/* https://gist.github.com/jackfuchs/556448 */
		var b = document.body || document.documentElement,
			s = b.style;

		/* test for standard property */
		if (typeof s[p] == 'string') { return true; }
		
		/* test for vendor specific property */
		var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
		p = p.charAt(0).toUpperCase() + p.substr(1);
		for (var i=0; i<v.length; i++) {
			if (typeof s[v[i] + p] == 'string') { return true; }
		}

		return false;
	}

	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */
	
	var degree = 180;
	var toggle = true

	$(function(){
		$("#edit_profile").on("click", function(e){
			e.preventDefault();
			
			$(".flipper").toggleClass("flip");
	

		})

	});

/* ------------------------------------------------------------------------------------ */
}());