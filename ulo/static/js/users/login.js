/* Sign Up Form javascript file */
/* Dependencies: jQuery, base.js */


/* ------------------------------------------------------------------------------------ */
(function () {
"use strict";


	/*
		**********************************************************************************
		FORM VALIDATION CLASS
		**********************************************************************************

		Manage form validation, displaying error messages to the user after they submit 
		the form.
		@param form_id: login form id.
		@param error_id: id of the div used to display error messages.

	*/	
	function FormValidation(form_id, error_id){

		/* REGEX VALIDATORS */
		this.input = {

			"id_email": {
				regexp: [
					/^(?!\s*$)/,
					/^(?=.{3,256}$)[^@\s]+@[^@\s\.]+(\.[^@\s\.]+)+$/
				],
			},
			"id_password": {
				regexp: [
					/^(?!\s*$)/,
					/^.{6,}$/,
					/^.{6,128}$/,
					/^(?![a-zA-Z]+$)/,
					/^(?!\d+$)/,
					/^(?!(.)\1+$|password(?:\d{0,2}|(\d)\2{1,})$)/
				],
			}
		}

		/* FORM HANDLES */
		this.jqxhr = null;
		this.error_div = $("#"+error_id);
		this.submit_btn = $("#id_submit", this.form);
		this.form = $("#"+form_id).on("submit", {self: this}, this.submitForm);
		
	}
	FormValidation.prototype = {

		constructor: FormValidation,
		/*
			Enable the form's submit button.
		*/
		enableSubmit: function(){
			this.submit_btn.removeAttr("disabled");
		},
		/*
			Disable the form's submit button.
		*/
		disableSubmit: function(){
			this.submit_btn.attr("disabled", "disabled");
		},
		/*
			Add an error message to the form. If no error message was provided use
			a fallback message.
			@param error: error message.
		*/
		addFormError: function(error){
			error = error || "We could not log you in, Please try again later.";
			this.error_div.empty().append(
				"<span class='error_icon'></span><p class='nonfield'>"+error+"</p>"
			);
		},
		/*
			Run all regex validators on the form fields and return true if all succeed
			and false if not. Display an error message if it fails.
		*/
		isFormValidated: function(){
			
			for(var id in this.input){

				var value = document.getElementById(id).value;
				var regexp = this.input[id].regexp;

				for(var i in regexp){
					if(regexp[i].test(value) === false){

						var self = this;
						setTimeout(function(){

							self.addFormError("The email address and password do not match.");
							self.enableSubmit();

						}, 500)
						
						return false;
					}
				}
			}

			return true;
		},

		/*
			Submit the form and redirect the page if successful or display an error message
			back to the user.
		*/
		submitForm: function(e){

			e.preventDefault();

			var self = e.data.self;
			self.disableSubmit();

			if(self.jqxhr === null && self.isFormValidated()){

				var request = {
					type: "POST",
					url: $(this).attr("action"),
					data: $(this).serialize(),
					cache:false,
				}

				self.jqxhr = $.ajax(request)
					/* params: server data, status code, xhr */
					.done(function(data){
						
						window.location.replace(data.redirect_url);

					})
					/* params: xhr, status code, error type */
					.fail(function(xhr){

						/* 
							Display the error to the user. The log in form will always 
							return non_field error messages with the key "__all__".
						*/
						self.addFormError(xhr.responseJSON.__all__);
					})
					.always(function(){
						self.jqxhr = null;
						self.enableSubmit();
				});
			}

			
		}
	}


	/* DOCUMENT READY FUNCTION */
	/* ------------------------------------------------------------------------------------ */
	$(function(){
		try{

			new FormValidation("login_form", 'form_info');

		} catch(e){
			debug(e);
		}
	});

/* ------------------------------------------------------------------------------------ */
}());

