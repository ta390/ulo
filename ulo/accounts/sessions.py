# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.signing import BadSignature, SignatureExpired, dumps, loads
from django.utils.translation import LANGUAGE_SESSION_KEY, ugettext_lazy as _

# thrid party app imports

# project imports
from emails.emails import reset_email


# ----------------------------------------------------------------------------------------

class PasswordResetSession(object):
	"""
	Class to manage the password reset process for logged out users. The class is a
	wrapper for the request.session object.
	"""
	# session key
	DATA_KEY = 'user_data'
	# session complete value
	SESSION_COMPLETE = 'complete'
	# date encoding
	salt = 'reset'
	# date lifespan (not session)
	max_age = 60 * 5
	# cached/logged in user
	user = None

	def __init__(self, request, check_user=False):
		# assign user if they are logged in
		if check_user and request.user.is_authenticated():
			self.user = request.user
		# assign the current session to the class (i.e. request.session)
		self.session = request.session


	def begin_session(self, email):
		"""
		Start a new password reset session by adding data to the current session.
		Return true if data was added and False if it was not
		"""
		try:
			# delete old/current session data and cycle the key
			self.delete_session_data()
			self.session.cycle_key()

			# store all user data needed to create the password reset email/token
			UserModel = get_user_model()
			user_dict = UserModel.objects.values(
				'pk', 'email', 'name', 'picture', 'password'
			).get(email=email)

			# store data as an encoded string
			self.set( dumps(user_dict, salt=self.salt) )
			# expire when the browser is closed
			#self.session.set_expiry(0)
			
		except UserModel.DoesNotExist:
			return False
		return True

	def end_session(self):
		"""
		Remove data from the session and return True if data was deleted and False
		if it was not.
		"""
		try:
			if self.session[self.DATA_KEY] == self.SESSION_COMPLETE:
				# remove session data and cycle the key
				self.delete_session_data()
				self.session.cycle_key()
				return True
		except KeyError:
			pass	
		return False

	# ------------------------------------------------------------------------------------

	def get(self):
		"""
		Return the current data or None.
		"""
		try:
			if self.user:
				return self.user

			user_dict = loads(
				self.session[self.DATA_KEY], salt=self.salt, max_age=self.max_age
			)
			# return user object created from the stored data dictionary
			return get_user_model()(**user_dict)

		except (KeyError, BadSignature, SignatureExpired):
			self.delete_session_data()

		return None

	def set(self, value):
		self.session[self.DATA_KEY] = value

	# ------------------------------------------------------------------------------------

	def delete_session_data(self):
		"""
		Delete data from the current session.
		"""
		try:
			del self.session[self.DATA_KEY]
			return True
		except KeyError:
			return False

	def flush_session(self):
		"""
		Flush the enitre session keeping the language setting intact.
		"""
		language = self.session.get(LANGUAGE_SESSION_KEY)
		self.session.flush()

		if language is not None:
			self.session[LANGUAGE_SESSION_KEY] = language

# ----------------------------------------------------------------------------------------

class BeginPasswordReset(forms.Form):
	"""
	Form to search for the user's account. If valid a new session is started else an
	error message is displayed back to the user.
	"""

	account = forms.EmailField(
		label=_('Enter your email address.'),
		label_suffix='',
		max_length=255
	)

	def __init__(self, request, *args, **kwargs):
		self.session = PasswordResetSession(request=request)
		super(BeginPasswordReset, self).__init__(*args, **kwargs)

	def is_valid(self):

		# if the form is valid and a new session was started successfully return True
		if super(BeginPasswordReset, self).is_valid() and \
			self.session.begin_session(self.cleaned_data.get('account')):
			return True

		# else add error to form and return False
		self.add_error(
			None, _('We could not find an account with the information provided.'),
		)
		return False

# ----------------------------------------------------------------------------------------

class EmailPasswordReset(object):
	"""
	Wrapper arround PasswordResetSession to handle the sending of an email or cancelling
	of a running password reset session.
	"""

	def __init__(self, request, check_user):
		self.cancel_btn = 'cancel'
		self.session_context = None
		self.session = PasswordResetSession(request=request, check_user=check_user)


	def context(self, user=None):
		"""
		Context getter/setter. Create a new context for the template or return the 
		existing one.
		"""
		if user is not None:
			self.session_context = {
				'name': user.name,
				'src': user.picture.url,
				'cancel': self.cancel_btn
			}
		return self.session_context

	def get_request(self):
		"""
		Handles a get request returning the template context if session data was found
		or None.
		"""
		user = self.session.get()
		if user is None:
			return None
		return self.context(user=user)

	def post_request(self, post_request):
		"""
		Handles a post request returning True if an email was successfully sent or
		False if it was not. The function will also return false if the user cancelled
		the password reset session.
		"""

		# if the user cancelled the session
		if post_request.get(self.cancel_btn):
			return self.cancel()

		# if no session data was found
		user = self.session.get()
		if user is None:
			return self.cancel()
		
		# if failed to send an email
		if reset_email(user=user) is 0:
			return self.email_failed()

		# else set session data to the value of session complete and return True
		self.session_context = None
		self.session.set(self.session.SESSION_COMPLETE)
		return True


	def cancel(self):
		"""
		Remove session data and context.
		"""
		self.session_context = None
		self.session.delete_session_data()
		return False

	def email_failed(self):
		"""
		Add email 'error' variable to context.
		"""
		if self.session_context is not None:
			self.context.update({
				'error': _('An email could not be sent. Please try again later.')
			})
		return False
		

# ----------------------------------------------------------------------------------------

def endPasswordReset(request):
	"""
	End the password reset session returning True if session data was found/delete or
	False if it was not.
	"""
	return PasswordResetSession(request=request).end_session()


# ----------------------------------------------------------------------------------------

