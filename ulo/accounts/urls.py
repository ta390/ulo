"""
Accounts URL Configuration
The `urlpatterns` list routes URLs to views.
"""
# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.conf.urls import include, url

# thrid party app imports

# project imports
from . import views

# ----------------------------------------------------------------------------------------


urlpatterns = [

	# Email verification
	url(r'^verification/(?P<token>[^<>\'"@]+)/$',
		views.EmailVerificationView.as_view(), name='verification'),
	url(r'^resend_verification/$',
		views.ResendEmailVerificationView.as_view(), name='resend_verification'),

	# Password reset
	url(r'^password_reset/$', 
		views.UserAccountView.as_view(), name='user_account'),
	url(r'^password_reset/email/$', 
		views.PasswordResetEmailView.as_view(), name='password_reset_email'),
	url(r'^password_reset/sent/$', 
		views.PasswordResetSentView.as_view(), name='password_reset_sent'),
	url(r'^password_reset/(?P<token>[^<>\'"@]+)/$',
		views.PasswordResetFormView.as_view(), name='password_reset_form'),

	url(r'^not_my_account/confirmed/$',
		views.NotMyAccountConfirmedView.as_view(), name='not_my_account_confirmed'),
	url(r'^not_my_account/not_registered/$',
		views.NotMyAccountNoUserView.as_view(), name='not_my_account_no_user'),
	url(r'^not_my_account/(?P<token>[^<>\'"@]+)/$',
		views.NotMyAccountView.as_view(), name='not_my_account'),
	

	# Token expired
	url(r'^expired/$', 
		views.LinkExpiredView.as_view(), name='expired'),
	# Account deactivated
	url(r'^deactivated/$', 
		views.DeactivatedAccountView.as_view(), name='deactivated'),
]

# ----------------------------------------------------------------------------------------