# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals
import re

# core django imports
from django.contrib import messages
from django.contrib.auth import get_user_model, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import Http404
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.cache import never_cache

# thrid party app imports

# project imports
from .forms import NewPasswordForm, NotMyAccountForm
from .sessions import BeginPasswordReset, EmailPasswordReset, endPasswordReset
from emails.emails import verify_email
from ulo.sessions import LinkViewsSession
from ulo.tokens import(
	GenericTokenGenerator, ResetPasswordTokenGenerator, VerifyEmailTokenGenerator
)
from ulo.utils import get_referer_path
from ulo.views import UloView, UloFormView, UloUpdateView
from ulo.viewmixins import LogoutRequiredMixin

# ----------------------------------------------------------------------------------------

def redirect_to_expired():
	return redirect(reverse('accounts:expired'))

def redirect_to_start():
	return redirect(reverse('accounts:user_account'))

class LinkExpiredView(UloView):
	template_name = 'accounts/expired.html'
	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, {})

# ----------------------------------------------------------------------------------------

class DeactivatedAccountView(UloView):
	"""
	Display page informing the user that their account has been deactivated.
	"""
	template_name = 'accounts/deactivated.html'

# ----------------------------------------------------------------------------------------

class NotMyAccountView(UloUpdateView):

	form_class = NotMyAccountForm
	template_name = 'accounts/not_my_account.html'
	success_url = reverse_lazy('accounts:not_my_account_confirmed')
	user = None

	def redirect_token(self, token):
		try:
			self.user = GenericTokenGenerator().check_token(
				string=token, salt='not_my_account', fail_silently=False, 
				max_age=864000 # 10 days
			)
			if self.user.email_confirmed or \
				self.user.block_type == self.user.NOT_MY_ACCOUNT:
				raise ValueError

			email = re.sub(
				r'^([^@]{1,2})[^@]*(@[^.]{1,2}).*(\.[^.]*)$', r'\1******\2******\3', 
				self.user.email
			)

			self.context = {'username': self.user.name, 'email':email}
			# have caller return a response
			return None

		except get_user_model().DoesNotExist:
			# user does not exist
			LinkViewsSession(self.request).set('not_my_account_no_user', True)
			return redirect(reverse('accounts:not_my_account_no_user'))
		except Exception:
			# invalid token
			return redirect_to_expired()


	def get(self, request, token, *args, **kwargs):		
		return self.redirect_token(token) or \
			super(NotMyAccountView, self).get(request, *args, **kwargs)

	def post(self, request, token, *args, **kwargs):
		return self.redirect_token(token) or \
			super(NotMyAccountView, self).post(request, *args, **kwargs)

	def get_object(self):
		return self.user

	def form_valid(self, form):
		form.save()
		LinkViewsSession(self.request).set('not_my_account', True)
		return super(NotMyAccountView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class NotMyAccountNoUserView(UloView):

	template_name = 'accounts/not_my_account_no_user.html'

	def get(self, request, *args, **kwargs):
		if LinkViewsSession(request).get('not_my_account_no_user'):
			return super(NotMyAccountNoUserView, self).get(request, *args, **kwargs)
		raise Http404()

# ----------------------------------------------------------------------------------------

class NotMyAccountConfirmedView(UloView):

	template_name = 'accounts/not_my_account_confirmed.html'

	def get(self, request, *args, **kwargs):
		if LinkViewsSession(request).get('not_my_account'):
			return super(NotMyAccountConfirmedView, self).get(request, *args, **kwargs)
		raise Http404()
	
# ----------------------------------------------------------------------------------------

@method_decorator(never_cache, name='get')
# TO DO: MAKE USER LOG IN BEFORE SETTING CONFIRMATION AND THEN DISPLAY A FLASH MESSAGE
class EmailVerificationView(LoginRequiredMixin, UloView):
	"""
	Check that the token is valid and set is_confirmed to True, else display error
	message to user informing them that the token is no longer valid
	"""
	redirect_field_name = 'redirect_to'
	
	def get(self, request, token, *args, **kwargs):

		user = VerifyEmailTokenGenerator().check_token(
			string=token, salt='email_verification', max_age=604800 # 7 days
		)

		if user:
			if request.user.is_authenticated() and request.user != user:
				messages.info(request, _(
					'Please sign is as %(username)s to confirm your email address.'
					) % {'username':user.username}
				)
			else:
				user.email_confirmed = True;
				user.save()
				messages.success(request, 
					_('Thank you for confirming your email address!')
				)

			return redirect(reverse('home'))

		# redirect to error page
		return redirect_to_expired()

# ----------------------------------------------------------------------------------------

class ResendEmailVerificationView(LoginRequiredMixin, UloView):
	"""
	Check that the token is valid and set is_confirmed to True, else display error
	message to user informing them that the token is no longer valid
	"""

	def get(self, request, *args, **kwargs):
		raise Http404()


	def post(self, request, *args, **kwargs):

		if verify_email(request.user):
			messages.success(request, 
				_('We have sent you a confirmation email.'), extra_tags="flash"
			)
		else:
			messages.error(request, 
				_('We failed to send you a confirmation email. Please try again later.'),
				extra_tags="flash"
			)

		return redirect( get_referer_path(request) )

# ----------------------------------------------------------------------------------------

class UserAccountView(LogoutRequiredMixin, UloFormView):
	"""
	Start the password reset process, diplaying the user with a form to find their
	account.
	"""

	template_name = 'accounts/user_account.html'
	success_url = reverse_lazy('accounts:password_reset_email')
	form_class = BeginPasswordReset

	def get_form_kwargs(self):
		kwargs = super(UserAccountView, self).get_form_kwargs()
		kwargs.update({'request': self.request })
		return kwargs

# ----------------------------------------------------------------------------------------

class PasswordResetEmailView(UloView):
	"""
	Display some account information to the user and provide them with the option to
	send themselves a password reset email or cancel.
	"""
	template_name = 'accounts/password_reset_email.html'
	session = EmailPasswordReset

	def get_response(self, request, context=None, *args, **kwargs):		
		if context is None:
			return redirect_to_start()
		# AjaxRequestMixin context: add context to template through UloView
		self.context = context
		return super(PasswordResetEmailView, self).get(request, *args, **kwargs)

	def get(self, request, *args, **kwargs):
		context = self.session(request, True).get_request()
		return self.get_response(request, context, *args, **kwargs)

	def post(self, request, *args, **kwargs):

		session = self.session(request, True)

		if session.post_request(request.POST) is True:
			return redirect(reverse('accounts:password_reset_sent'))
			
		context = session.context()
		return self.get_response(request, context, *args, **kwargs)

# ----------------------------------------------------------------------------------------

class PasswordResetSentView(UloView):
	"""
	End the password reset process and display a page informing the user that an email
	has been sent to them.
	"""
	template_name = 'accounts/password_reset_sent.html'

	def get(self, request, *args, **kwargs):
		if endPasswordReset(request):
			return render(request, self.template_name, {})
		return redirect(reverse('accounts:password_reset_email'))

# ----------------------------------------------------------------------------------------

class PasswordResetFormView(LogoutRequiredMixin, UloFormView):
	"""
	Check the password reset token and diplay a change password form to the user.
	"""

	user = None
	form_class = NewPasswordForm
	success_url = reverse_lazy('home')
	template_name = 'accounts/password_reset_form.html'
	
	def get_form(self):
		return self.get_form_class()(self.user, **self.get_form_kwargs())

	def valid_token(self, token):
		if token:
			self.user = ResetPasswordTokenGenerator().check_token(
				string=token, salt='password_reset', max_age=1800
			)
			if self.user:
				self.context = {'name': self.user.name, 'src':self.user.picture.url}
				return True

		return False

	def get(self, request, token, *args, **kwargs):
		if self.valid_token(token):
			return super(PasswordResetFormView, self).get(request, *args, **kwargs)
		return redirect_to_expired()

	def post(self, request, token, *args, **kwargs):
		if self.valid_token(token):
			return super(PasswordResetFormView, self).post(request, *args, **kwargs)
		return redirect_to_expired()

	def form_valid(self, form):
		user = form.save()
		if user:
			login(self.request, user)
		return super(PasswordResetFormView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

