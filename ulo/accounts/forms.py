# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django import forms
from django.contrib.auth import get_user_model, authenticate, login
from django.contrib.auth.password_validation import validate_password
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports
from ulo.fields import PasswordField

# ----------------------------------------------------------------------------------------

class NewPasswordForm(forms.Form):
	"""
	A form that lets a user change set their password without entering the old
	password.
	"""
	error_messages = {
		'mismatch': _("Passwords do not match."),
	}

	# use custom PasswordField for additional validation
	password1 = PasswordField( 
		label=_('Enter your new password'), 
		required=True, 
		error_messages = {'required': _('Please enter a new password.')}
	)
	password2 = forms.CharField(
		label=_('Re-enter your new password'),
		required=True,
		widget=forms.PasswordInput, 
		error_messages = {'required': _('Please re-enter your new password.')}
	)
	


	def __init__(self, instance, *args, **kwargs):
		self.instance = instance
		super(NewPasswordForm, self).__init__(*args, **kwargs)

	def clean_password2(self):
		password1 = self.cleaned_data.get('password1')
		password2 = self.cleaned_data.get('password2')
		print(password1, password2)
		if password1 and password2:
			if password1 != password2:
				raise forms.ValidationError(
					self.error_messages['mismatch'],
					code='mismatch',
				)
		validate_password(password=password2, user=self.instance)
		return password2

	def save(self, commit=True):
		password = self.cleaned_data['password1']
		self.instance.set_password(password)
		if commit:
			self.instance.save()
			return authenticate(email=self.instance.email, password=password)
		return self.instance

# ----------------------------------------------------------------------------------------

class NotMyAccountForm(forms.Form):
	"""
	On submission of this form update the user instance to block the account.
	"""

	def __init__(self, instance, *args, **kwargs):
		self.instance = instance
		super(NotMyAccountForm, self).__init__(*args, **kwargs)

	def save(self):
		# add a block to the account
		self.instance.block_type = self.instance.NOT_MY_ACCOUNT
		# deactivate the account so it can be deleted in 30 days
		self.instance.is_active = False
		self.instance.save(update_fields=['block_type', 'is_active'])

# ----------------------------------------------------------------------------------------

