"""
Settings URL Configuration
The `urlpatterns` list routes URLs to views.
"""
# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.conf.urls import include, url

# thrid party app imports

# project imports
from . import views

# ----------------------------------------------------------------------------------------


urlpatterns = [

	url(r'^account/$',
		views.AccountSettingsView.as_view(), name='account'),
	url(r'^deactivate/$',
		views.DeactivateAccountView.as_view(), name='deactivate'),

	url(r'^password/$',
		views.PasswordSettingsView.as_view(), name='password'),

	url(r'^notifications/$',
		views.NotificationSettingsView.as_view(), name='notifications'),
]

# ----------------------------------------------------------------------------------------