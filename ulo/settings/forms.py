# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports
from emails.emails import verify_email
from ulo.fields import PasswordField
from ulo.formmixins import CleanUsernameMixin
from ulo.forms import UloModelForm, UloSecureModelForm
from users.search import update_user


# ----------------------------------------------------------------------------------------

class AccountSettingsForm(CleanUsernameMixin, UloSecureModelForm):
	"""
	Display form to change user's account settings.
	"""

	class Meta:
		model = get_user_model()
		fields = ('name', 'username', 'email')

	@cached_property
	def changed_data(self):
		"""
		Generate a list of changed field. This is a case insensitive, simplified version 
		of Django's own changed_data. 
		https://docs.djangoproject.com/en/1.9/_modules/django/forms/forms/#Form
		"""
		changed = []
		for name, field in self.fields.items():
			if field.has_changed(
				self.initial.get(name, field.initial), 
				self.cleaned_data.get(name, '').lower()
			):
				changed.append(name)
		return changed


	def has_changed(self):
		return bool(self.changed_data)

	# CleanUsername Mixin implements clean_username() and returns the username in
	# lowercase.

	def clean_email(self):
		"""
		Normalise the email address (see users.models.UserManager)
		"""
		return self.cleaned_data['email'].lower()

	def is_valid(self):
		if super(AccountSettingsForm, self).is_valid():
			return True
		
		for name, field in self.fields.items():
			# set instance values back to their original values
			setattr(self.instance, name, self.initial.get(name, field.initial))
			# set form data back to its initial value for invalid fields
			if self.has_error(name, code='invalid'):
				self.data._mutable = True
				self.data[name] = self.initial[name]
				self.data._mutable = False

		return False

	def save(self, commit=True):
		"""
		Save changes and send verification email to the user if they changed their 
		emails address. NOTE: UloSecureModelForm stored the instance variable as
		instance_copy. See ulo.formmixins.UloBaseModelFormMixin for full explanation.
		"""
		# if the user changed their email set is_confirmed to False to force verification
		# of the user's new email address
		email_changed = ('email' in self.changed_data)
		if email_changed:
			self.instance.email_confirmed = False

		user = super(AccountSettingsForm, self).save(commit=False)
		
		if commit:
			# update elasticsearch and save user to db - raises DatabaseError
			update_user(user, self.changed_data)
			if email_changed:
				# send verification email if email changed
				verify_email(user)

		return user

# ----------------------------------------------------------------------------------------

class PasswordSettingsForm(UloSecureModelForm):
	"""
	Display form to change the user's password.
	"""

	new_password1 = PasswordField( 
		label=_('New password'),
		error_messages = {'required': _('Please enter a new password.')}
	)
	new_password2 = forms.CharField(
		label=_('Re-enter new password'),
		widget=forms.PasswordInput,
		error_messages = {'required': _('Please re-enter your new password.')}
	)

	class Meta:
		model = get_user_model()
		fields = ('password',)

	def clean_new_password2(self):
		password1 = self.cleaned_data.get('new_password1')
		password2 = self.cleaned_data.get('new_password2')

		if password1 and password2:
			if password1 != password2:
				raise forms.ValidationError(
					_('Passwords do not match.'),
					code='mismatch',
				)
		# run the validator(s) defined in the settings file (base.py)
		validate_password(password=password2, user=self.instance)
		return password2

	def save(self, commit=True):
		user = super(PasswordSettingsForm, self).save(commit=False)
		user.set_password(self.cleaned_data['new_password1'])

		if commit:
			user.save()

		return user

# ----------------------------------------------------------------------------------------

class NotificationSettingsForm(UloModelForm):
	"""
	Display form to change the user's notification settings.
	"""

	class Meta:
		model = get_user_model()
		fields = ('is_active', 'is_verified', 'email_confirmed', 'block_type')

# ----------------------------------------------------------------------------------------
