
# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.contrib import messages
from django.contrib.auth import logout, update_session_auth_hash
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.cache import cache_control, never_cache
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.vary import vary_on_headers

# thrid party app imports


# project imports
from .forms import AccountSettingsForm, NotificationSettingsForm, PasswordSettingsForm
from ulo.views import UloView, UloUpdateView


# ----------------------------------------------------------------------------------------

class DeactivateAccountView(LoginRequiredMixin, UloView):

	redirect_field_name = 'redirect_to'
	template_name = 'settings/deactivate.html'

	def post(self, request, *args, **kwargs):
		if request.POST.get('cancel'):
			return redirect(reverse('settings:account'))

		request.user.deactivate_account()
		logout(request)
		return redirect(reverse('accounts:deactivated'))

# ----------------------------------------------------------------------------------------

class AccountSettingsView(LoginRequiredMixin, UloUpdateView):

	redirect_field_name = 'redirect_to'
	form_class = AccountSettingsForm
	template_name = 'settings/account.html'
	success_url = reverse_lazy('settings:account')
	# UloUpdateView (AjaxFormMixin): specify the context name given to the form
	form_context_name = 'account_form'

	def get_object(self):
		return self.request.user

	def form_valid(self, form):
		if form.has_changed():
			form.save()
			messages.success(self.request, _('Your account settings have been updated.'))
		return super(AccountSettingsView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class PasswordSettingsView(LoginRequiredMixin, UloUpdateView):

	redirect_field_name = 'redirect_to'
	form_class = PasswordSettingsForm
	template_name = 'settings/password.html'
	success_url = reverse_lazy('settings:password')
	# UloUpdateView (AjaxFormMixin): specify the context name given to the form
	form_context_name = 'password_form'
	initial = {'password': ''}

	def get_object(self):
		return self.request.user

	def form_valid(self, form):
		update_session_auth_hash(self.request, form.save())
		messages.success(self.request, _('Your password has been changed.'))
		return super(PasswordSettingsView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class NotificationSettingsView(LoginRequiredMixin, UloUpdateView):

	redirect_field_name = 'redirect_to'
	form_class = NotificationSettingsForm
	template_name = 'settings/notifications.html'
	success_url = reverse_lazy('settings:notifications')
	# UloUpdateView (AjaxFormMixin): specify the context name given to the form
	form_context_name = 'notifications_form'

	def get_object(self):
		return self.request.user

	def form_valid(self, form):
		if form.has_changed():
			form.save()
			messages.success(self.request, 
				_('Your notification settings have been updated.'))
		return super(NotificationSettingsView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

