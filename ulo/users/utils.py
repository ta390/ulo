# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# standard library imports
from __future__ import unicode_literals

# core django imports
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports


# USER ACCOUNT VERIFICATION
# ----------------------------------------------------------------------------------------
NOT_VERIFIED = 0


# USER ACCESS CONSTANTS
# ----------------------------------------------------------------------------------------

NO_BLOCK = '_'
ABUSE = '0'
NOT_MY_ACCOUNT = '1'

BLOCK_TYPES = (
	(NO_BLOCK, ''),
	(NOT_MY_ACCOUNT, _('This account has been reported as belonging to another user.')),
	(ABUSE, _('Your account has been suspended due to misconduct.')),
)


# USER GENDER OPTIONS
# ----------------------------------------------------------------------------------------

NO_GENDER = '0'
MALE = 'M'
FEMALE = 'F'

GENDERS = (
	(NO_GENDER, '---'),
	(MALE, _('Male')),
	(FEMALE, _('Female')),
)

