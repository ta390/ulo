"""
Accounts URL Configuration
The `urlpatterns` list routes URLs to views.
"""
# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.conf.urls import include, url

# thrid party app imports

# project imports
from . import views

# ----------------------------------------------------------------------------------------


urlpatterns = [

	# UnFollow User
	url(r'^available/(?P<username>[\w]+)/$', views.UsernameAvailableView.as_view(), name='available'),

	# User Profile Edit Page
	url(r'^update/$', views.ProfileUpdateView.as_view(), name='profile_update'),
	# User Profile Page: username regex pattern used in users.models.User
	url(r'^(?P<username>[\w]+)/$', views.ProfileView.as_view(), name='profile'),

	# View User Followers
	url(r'^(?P<username>[\w]+)/followers/$', 
		views.FollowersView.as_view(), name='followers'),
	# View User Following
	url(r'^(?P<username>[\w]+)/following/$', 
		views.FollowingView.as_view(), name='following'),

	# Follow User
	url(r'^follow/(?P<pk>[\d]+)/$', 
		views.FollowUserView.as_view(), name='follow'),
	# UnFollow User
	url(r'^unfollow/(?P<pk>[\d]+)/$', 
		views.UnFollowUserView.as_view(), name='unfollow'),


	
	
]

# ----------------------------------------------------------------------------------------