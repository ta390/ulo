# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# standard library imports
from __future__ import unicode_literals
import re, requests

# core django imports
from django.db import DatabaseError, IntegrityError, transaction
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports
from .models import Connection
from generics.constants import ( CONNECTION_INDICES_ALIAS, CONNECTION_SEARCH_ALIAS,
	CONNECTION_INDEX_ALIAS, CONNECTION_INDEX_0, CONNECTION_TYPE_0, 
	USER_INDICES_ALIAS, USER_INDEX_ALIAS, USER_SEARCH_ALIAS, USER_INDEX_0, USER_TYPE_0, 
	USER_SUGGEST_0, POST_INDICES_ALIAS
)
from generics.serialisers import serialiser
from generics.utils import es, EsException
from posts.search import ES_POST_USER_FIELDS, ES_USER_FIELD_NAME, ES_USER_ID_FIELD_NAME


# USER DOCUMENT FIELDS
# ----------------------------------------------------------------------------------------

# ****************************************************************************************
# CALLBACK FUNCTION BELOW USES HARDCODED FIELD NAMES - SO UPDATE IT IF FIELDS CHANGE
# ****************************************************************************************

# all fields excluding id
ES_USER_FIELDS = ('name', 'username', 'picture', 'is_verified')
ES_IMAGE_FIELD = 'picture'


# USER ANALYSERS
# ----------------------------------------------------------------------------------------

# name field analysers
NAME_ANALYSER = 'user_name_analyser'
NAME_SEARCH_ANALYSER = 'user_search_analyser'

# custom analysers added to the index
user_analysers = {
	NAME_ANALYSER: {
		'type': 'custom',
		'tokenizer': 'whitespace',
		'filter': [
			'word_delimiter_filter',
			'lowercase',
			'asciifolding_filter',
			'unique_filter', # only_on_same_position: True
			'token_limit16_filter',
		]
	},
	NAME_SEARCH_ANALYSER: {
		'type': 'custom',
		'tokenizer': 'whitespace',
		'filter': [
			'lowercase',
		]
	},
}


# USER DOCUMENT SETTINGS
# ----------------------------------------------------------------------------------------
def _user_settings_v0():
	"""
	Return aliases, analysers and mappings for a new user index.
	"""
	# USER INDEX ALIASES
	aliases = {
		# alias for all user indices
		USER_INDICES_ALIAS: {},
		# alias for all searchable user indices
		USER_SEARCH_ALIAS: {}
	}

	# USER INDEX MAPPINGS
	mappings = {
		'name': {
			'type':'string',
			'analyzer': NAME_ANALYSER,
			'search_analyzer': NAME_SEARCH_ANALYSER,
		},
		'username': {
			'type':'string',
			'analyzer': NAME_ANALYSER,
			'search_analyzer': NAME_SEARCH_ANALYSER,
		},
		'picture': {
			'type': 'string', 
			'index': 'no',
			'include_in_all': False, 
			'doc_values': False
		},
		'is_verified': {
			'type': 'boolean',
			'index' : 'not_analyzed',
			'include_in_all': False
		},
		USER_SUGGEST_0: {
			'type': 'completion',
			'analyzer': NAME_SEARCH_ANALYSER,
			'search_analyzer': NAME_SEARCH_ANALYSER,
			'payloads': True,
		}
	}

	return aliases, user_analysers, mappings


# CREATE A NEW USER INDEX (VERSION 0)
# ----------------------------------------------------------------------------------------
def create_user_index(es):
	"""
	"""
	aliases, analysers, mappings = _user_settings_v0()
	# create the new index - raises an exception if it fails.
	es.create_index(USER_INDEX_0, aliases=aliases, analysers=analysers)
	# map the new index to the current index alias - raises an exception if it fails.
	es.replace_alias(USER_INDEX_ALIAS, index_remove='_all', index_add=USER_INDEX_0)
	# apply field mappings to index and specific type - raises an exception if it fails.
	es.mapping(index_type=USER_TYPE_0, mappings=mappings, indices=USER_INDEX_0)

	return True

# ----------------------------------------------------------------------------------------
def doc_mapping(doc):
	"""
	Generate the document mapping parameters for each user document. Return the tokens
	used for auto complete (tokens), the auto complete suggestion field (USER_SUGGEST_0)
	and additional document mapping parametes (params)
	@param doc: a single user document.
	"""
	regexp = re.compile(r'\b[^\s\u200b]+\b', re.U)
	tokens = [doc['name'], doc['username']]
	
	for token in tokens[:2]:
		tokens += regexp.findall(token)[1:]

	# token_regexp = re.compile(r'[^\W\d_]+|[\d]+', re.U)
	# tokens2 = []
	# for token2 in tokens[2:]:
	# 	tokens2 += token_regexp.findall(token)

	# tokens = es.normalise_tokens(tokens+tokens2, max_tokens=8)


	# a dictionay containing document mapping parameters/values
	params = {
		'payload': {'name': doc['name'], 'picture': doc['picture']},
		'output': doc['username']
	}

	# return document mapping parameters.
	return es.document_mapping(tokens, USER_SUGGEST_0, **params)


# CREATE A NEW USER IN THE DB AND IN ELASTICSEARCH
# ----------------------------------------------------------------------------------------
def create_user(user, **kwargs):
	"""
	Call save() on a new user instance to commit it to the database and then create its 
	elasticsearch document. The two operations are atomic.
	@param user: new user instance. 
	"""
	doc = {}
	for f in ES_USER_FIELDS:
		doc[f] = getattr(user, f).url if f == ES_IMAGE_FIELD else getattr(user, f)

	doc.update( doc_mapping(doc) )
	return es.create_instance(user, doc, USER_INDEX_0, USER_TYPE_0, **kwargs)


# UPDATE A USER IN THE DB AND IN ELASTICSEARCH
# ----------------------------------------------------------------------------------------
def _update_user_data(hit, **kwargs):
	"""
	Build a bulk request to update user information held in a post.
	"""
	# partial doc
	doc = kwargs['doc']
	# bulk request
	data =  serialiser.dumps({ 'update': es._build_header(hit) }) + '\n'
	data += serialiser.dumps({'doc': doc}) + '\n'
	return data

def update_post_user_data(user_id, user_data, **kwargs):
	"""
	Each post contains some user information. If a user changes any information that is
	held in a post then update all user posts.
	@param user_id: user's id
	@param user_data: e.g. {'user': {'name': 'B'}}
	"""
	# max size of each batch
	size = kwargs.get('size', 1000)
	# timeout for the scroll request
	time = kwargs.get('time', '1m')

	data = serialiser.dumps({
		'query': {
			'match': {
				ES_USER_ID_FIELD_NAME : user_id
			}
		},
		'_source': ['_id'],
		'sort': ['_doc'],
		# size is applied to each shard so it will return size * number_or_primary_shards
		# in each batch
		'size': size,
	})

	# get scroll initial response
	url = es.get_url(POST_INDICES_ALIAS, '_search', params={'scroll':time} )
	response = requests.get(url, data=data).json()
	
	return es.process_scroll(
		response, 
		time, 
		callback=_update_user_data, 
		doc={ ES_USER_FIELD_NAME: user_data }
	)

def update_user(user, changed):
	"""
	Save the form to update the user's information. If a user has changed a field that
	is also an elasticsearch user document field (ES_USER_FIELDS) then update the
	es document too. If the user has changed a field that is held in each post
	document (ES_POST_USER_FIELDS) then update all posts too.
	@param user: updated user instance 
	@param changed: list of changed field names
	"""

	if not changed:
		return user

	doc = {}
	user_obj = {}
	update_required = False
	
	# build the user doc and check if anything has changed.
	for f in ES_USER_FIELDS:

		doc[f] = getattr(user, f).url if f == ES_IMAGE_FIELD else getattr(user, f)
		
		if f in changed:
			update_required = True

			if f in ES_POST_USER_FIELDS:
				user_obj[f] = getattr(user, f)

	

	if update_required:

		# most user fields form the doc mapping so always rebuild it for simplicity.
		doc.update( doc_mapping(doc) )

		try:
			with transaction.atomic():
				# save the user to the db.
				user.save()
				# update the current user doc - use _mi function if indices are added.
				es.update_instance(user, doc, USER_INDEX_0, USER_TYPE_0, **kwargs)
				# update all posts if a post-user field was changed.
				if user_obj:
					update_post_user_data(user.pk, user_obj)

		except (DatabaseError, EsException) as e:
			return None
	return user

# ----------------------------------------------------------------------------------------


# USER DOCUMENT SETTINGS
# ----------------------------------------------------------------------------------------
def _connection_settings_v0():
	"""
	Return aliases, analysers and mappings for a new user index.
	"""
	# USER INDEX ALIASES
	aliases = {
		# alias for all user indices
		CONNECTION_INDICES_ALIAS: {},
		# alias for all searchable user indices
		CONNECTION_SEARCH_ALIAS: {}
	}

	# USER INDEX MAPPINGS
	mappings = {
		'user_id': {
			'type': 'string',
			'index' : 'not_analyzed',
			'include_in_all': False
		},
		'following_id': {
			'type': 'string',
			'index' : 'not_analyzed',
			'include_in_all': False
		},
	}

	return aliases, None, mappings

def create_connection_index(es):
	"""
	"""
	aliases, analysers, mappings = _connection_settings_v0()
	# create the new index - raises an exception if it fails.
	es.create_index(CONNECTION_INDEX_0, aliases=aliases, analysers=analysers)
	# map the new index to the current index alias - raises an exception if it fails.
	es.replace_alias(CONNECTION_INDEX_ALIAS, index_remove='_all', index_add=CONNECTION_INDEX_0)
	# apply field mappings to index and specific type - raises an exception if it fails.
	es.mapping(index_type=CONNECTION_TYPE_0, mappings=mappings, indices=CONNECTION_INDEX_0)

	return True


def create_connection(user, following_id, **kwargs):
	try:
		connection = None
		with transaction.atomic():

			doc = {
				'user_id': user.pk,
				'following_id': following_id
			}
			connection = Connection.objects.create(from_user=user, to_user_id=following_id)
			kwargs['params'] = {'routing': str(user.pk)}
			es.create_document(doc, CONNECTION_INDEX_ALIAS, CONNECTION_TYPE_0, connection.pk, **kwargs)
		
	except IntegrityError:
		# record already exists as database has unique together constraint.
		pass

	except (DatabaseError, EsException) as e:
		return None

	return connection


def delete_connection(user, following_id, **kwargs):

	try:
		with transaction.atomic():

			connection = Connection.objects.filter(from_user=user, to_user_id=following_id)
			pk = connection[0].pk
			connection.delete()
			kwargs['params'] = {'routing': str(user.pk)}
			es.delete_document(CONNECTION_INDEX_ALIAS, CONNECTION_TYPE_0, 36, **kwargs)

	except IndexError:
		pass

	except (DatabaseError, EsException) as e:
		return False

	return True
	




