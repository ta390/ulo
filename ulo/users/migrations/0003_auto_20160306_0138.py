# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-06 01:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20160305_1726'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='is_confirmed',
            new_name='email_confirmed',
        ),
        migrations.RemoveField(
            model_name='user',
            name='is_blocked',
        ),
        migrations.AddField(
            model_name='user',
            name='block_type',
            field=models.CharField(choices=[('_', ''), ('B', 'The account has been reported as belonging to another user.'), ('A', 'Your account has been suspended due to misconduct.')], default='_', help_text='Designates whether this has had their account suspended', max_length=1, verbose_name='account suspended'),
        ),
    ]
