# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django import forms
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.password_validation import validate_password
from django.db import transaction
from django.utils.translation import ugettext_lazy as _


# thrid party app imports

# project imports
from .search import create_user
from emails.emails import reactivation_email, verify_email
from ulo.fields import PasswordField, UloDOBField
from ulo.formmixins import CleanUsernameMixin
from ulo.forms import UloForm, UloModelForm



USER_MIN_AGE = 13

# USERS FORMS
# ----------------------------------------------------------------------------------------

class SignUpForm(CleanUsernameMixin, UloModelForm):

	# use custom PasswordField for additional validation
	password = PasswordField( label=_('Password'), )
	# use custom DateSelectorWidget to list dates in a drop down select menu
	dob = UloDOBField(label=_('Birthday'), min_age=USER_MIN_AGE)

	class Meta:
		model = get_user_model()
		fields = ('name', 'username', 'email', 'password', 'dob', 'gender',)
		localized_fields = ('dob',)
		

	def __init__(self, *args, **kwargs):
		"""
		OVERRIDE
		Define custom error messages and placeholder values for each field
		"""
		super(SignUpForm, self).__init__(*args, **kwargs)
		self.unknown_error = ('We could not create your account at this moment in time. '
			'Please try again later, we are looking into the problem.')

		for i, field in self.fields.items():
			# set placeholder value and required attribute for each field
			field.widget.attrs.update({
				'placeholder': _(field.label),
				'autocomplete': 'off',
				#'required': 'required',
			})

		# Add field specific attributes
		self.fields['name'].widget.attrs.update({
			'autofocus': 'autofocus',
		})

		# make '0' an invalid value to match the choices.
		self.fields['gender'].empty_values.append('0');


	def clean_password(self):
		# create a user object using the USER_ATTRS fields. This will create a user
		# with only the fields required for the UserAttributeSimilarityValidator to 
		# test if a password is similar to the user's personal information
		attrs = settings.USER_ATTRS
		user_params = {}
		for attr in attrs:
			user_params[attr] = self.cleaned_data.get(attr)
		user = get_user_model()(**user_params)
		# get the password value
		password = self.cleaned_data.get('password')
		# run the validator(s) defined in the settings file (base.py)
		validate_password(password=password, user=user)
		user=None

		return password

	def save(self, commit=True):
		"""
		OVERRIDE
		Save the form data to the database, storing a hashed version of the
		user's password and send an email confirmation email to the user.
		"""
		user = super(SignUpForm, self).save(commit=False)
		user.set_password(self.cleaned_data['password'])
		if commit:
			# Save user instance and create elasticsearch doc - raises DatabaseError
			create_user(user)
			transaction.on_commit( lambda: verify_email(user) )

		return user

# ----------------------------------------------------------------------------------------

class LoginForm(UloForm):

	email = forms.EmailField(
		label=_('Email address'),
		widget=forms.TextInput(attrs={
			'autofocus': 'autofocus',
			'placeholder': _('Email address'),
			#'required': 'required',
		}),
	)
	# use PasswordField to validate the password before accessing the db needlessly
	password = PasswordField(
		label=_('Password'),
		widget=forms.PasswordInput(attrs={
			'placeholder': _('Password'),
			#'required': 'required',
		}),
	)

	remember = forms.BooleanField(
		label=_('Remember me'),
		required=False,
	)

	redirect_to = forms.CharField(
		required = False,
		widget=forms.HiddenInput()
	)

	error_messages = {
		'invalid_login': _('The email address and password do not match.'),
	}

	def __init__(self, redirect_to=None, *args, **kwargs):
		"""
		Remove label suffix for all fields, add request and user variables to the
		class and get the name of the field used to identify a user.
		"""
		self.user=None
		self.remember=False
		super(LoginForm, self).__init__(*args, **kwargs)
		self.unknown_error = ('We could not log you in at this moment in time. '
			'Please try again later.')
			
		self.fields['redirect_to'].initial = redirect_to or ''
		UserModel = get_user_model()
		self.user_id = UserModel._meta.get_field(UserModel.USERNAME_FIELD)

	def clean(self):
		email = self.cleaned_data.get('email')
		password = self.cleaned_data.get('password')

		try:
			# if no field errors, try and authenticate the user
			if len(self.errors) == 0:
				self.user = authenticate(email=email, password=password)
				if self.user is None:
					raise forms.ValidationError('Authentication failed')
			else:
				# remove all errors and display generic message to user
				self.errors.clear()
				raise forms.ValidationError('Field error')
					
		except forms.ValidationError:
			raise forms.ValidationError(
				self.error_messages['invalid_login'],
				code='invalid_login'
			)

		self.validate_login(self.user)	
		return self.cleaned_data

	def validate_login(self, user):
		# check is user has had their account revoked
		if user.is_blocked():
			raise forms.ValidationError(
				user.get_block_message(),
				code='blocked'
			)
		# check is user is active. if not set to active and send email
		if not user.is_active:
			user.is_active = True
			user.save()
			reactivation_email(user)
		
		# assign the remember value to the form to be accessed from the view
		self.remember = self.cleaned_data.get('remember', False)


# ----------------------------------------------------------------------------------------

class ProfileUpdateForm(UloModelForm):

	# DateFile that checks the user's age is gte min_age
	dob = UloDOBField(label=_('Birthday'), min_age=USER_MIN_AGE)
	blurb = forms.CharField(label=_('Blurb'), widget=forms.Textarea(), required=False)

	class Meta:
		model = get_user_model()
		fields = ('location', 'dob', 'blurb')

# ----------------------------------------------------------------------------------------

