# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.contrib import messages
from django.contrib.auth import authenticate, get_user_model, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.http import urlquote_plus, urlunquote_plus
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports
from .forms import LoginForm, ProfileUpdateForm, SignUpForm
from .models import Connection
from .search import create_connection, delete_connection
from ulo.utils import get_referer_path, UloPaginator
from ulo.views import UloView, UloFormView, UloRedirectView, UloUpdateView
from ulo.viewmixins import LogoutRequiredMixin


# ----------------------------------------------------------------------------------------

class SignUpView(LogoutRequiredMixin, UloFormView):
	"""
	Display sign up form and create new user account. Log the user in after successfully
	creating the account and send them an email to verify their address.
	"""

	template_name = 'users/signup.html'
	form_class = SignUpForm
	
	# MAKE POST mutable
	# self.request.POST._mutable = True
	# self.request.POST['name'] = ''

	def get_success_url(self):
		"""
		Redirect to the home page after creating an account.
		"""
		return reverse('home')

	def form_valid(self, form):
		"""
		Save the user to the database and log them into their account.
		"""

		form.save()

		# use post data to authenticate the user
		email = form.cleaned_data.get('email')
		password = form.cleaned_data.get('password')
		user = authenticate(email=email, password=password)

		if user:
			# log in the new user
			 login(self.request, user)
			 return super(SignUpView, self).form_valid(form)
		else:
			# failed to log user into their account
			return redirect(reverse('login'))

# ----------------------------------------------------------------------------------------

class UsernameAvailableView(UloView):

	def get(self, request, *args, **kwargs):
		raise Http404()

	def head(self, request, username, *args, **kwargs):

		if request.is_ajax():
			response = HttpResponse('')
			response['username_exists'] = get_user_model().objects.filter(username=username).exists();
			return response

		raise Http404()

# ----------------------------------------------------------------------------------------

# TODO: Handle cookie error in better way (i.e. if user does not allow cookies)
class LoginView(LogoutRequiredMixin, UloFormView):

	template_name = 'users/login.html'
	form_class = LoginForm
	redirect_name = 'redirect_to'

	def get(self, request, redirect_to, *args, **kwargs):
		"""
		Initialise form data with the redirect path if one is given.
		"""
		# Initialise hidden form field with the redirect path
		name = self.redirect_name
		path = self.request.GET.get(name, '')
		self.initial.update({name: path})
		# AjaxRequestMixin context variable. Append redirect path to the forms url.
		self.context.update({name: path and '?'+name+'='+urlquote_plus(path)})

		return super(LoginView, self).get(request, *args, **kwargs)

	def get_success_url(self):
		"""
		Return the url to redirect to after a successful post.
		"""
		print(self.request.POST.get('redirect_to'))
		return self.request.POST.get('redirect_to') or reverse('home')

	def form_valid(self, form):
		"""
		Log the user in and set session expiry if 'remember me' was not selected.
		"""
		if form.remember is False:
			# expire session when the browser window is closed
			self.request.session.set_expiry(0)
				
		login(self.request, form.user)
		return super(LoginView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class LogoutView(UloRedirectView):
	
	def get_redirect_url(self):
		# destroy all session data for the current request / user
		logout(self.request)
		return reverse('home')

# ----------------------------------------------------------------------------------------

class ProfileView(UloView):
	"""
	"""
	template_name = 'users/profile.html'

	def get_context_data(self, **kwargs):
		# get default context information
		context = super(ProfileView, self).get_context_data(**kwargs)
		# retrieve user information
		user = get_object_or_404(
			get_user_model(), username=self.kwargs.get('username', None), is_active=True
		)

		# add profile information to context dictionary
		context.update({
			'user_profile': user,
			'followers': user.to_user.count(),
			'following': user.from_user.count(),
		})
		if self.request.user.is_authenticated():
			context.update({
				'form': ProfileUpdateForm(instance=self.request.user),
				'is_following': self.request.user.is_following(user)
			})

		#!!!!! TESTING !!!!!!#
		if self.request.user.is_authenticated():
			context.update({
				'connections': self.request.user.connections.all()
			})
		#!!!!! END TESTING !!!!!!#

		# Add to the context dictionary
		return context


# ----------------------------------------------------------------------------------------

class ProfileUpdateView(LoginRequiredMixin, UloUpdateView):
	"""
	"""
	redirect_field_name = 'redirect_to'
	template_name = 'users/profile_update.html'
	form_class = ProfileUpdateForm

	def get_object(self):
		return self.request.user

	def get_success_url(self):
		return reverse('users:profile', args={self.request.user.username})

	def form_valid(self, form):
		form.save()
		messages.success(self.request, _('Profile Updated!'))
		return super(ProfileUpdateView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class FollowersView(UloView):

	template_name = 'users/followers.html'
	paginator = UloPaginator

	def get_context_data(self, **kwargs):
		# get default context information
		context = super(FollowersView, self).get_context_data(**kwargs)
		# retrieve user information
		user = get_object_or_404(
			get_user_model(), username=self.kwargs.get('username', None), is_active=True
		)

		page = self.request.GET.get('page', 1)
		paginator = self.paginator(object_list=user.to_user.all(), page=page)
		
		context.update({
			'followers': paginator.get_page(select_related='from_user'),
			'user_profile': user,
			'paginate': paginator.get_page_buttons()
		})

		return context

# ----------------------------------------------------------------------------------------

class FollowingView(UloView):

	template_name = 'users/following.html'
	paginator = UloPaginator

	def get_context_data(self, **kwargs):
		# get default context information
		context = super(FollowingView, self).get_context_data(**kwargs)
		# retrieve user information
		user = get_object_or_404(
			get_user_model(), username=self.kwargs.get('username', None), is_active=True
		)

		page = self.request.GET.get('page', 1)
		paginator = self.paginator(object_list=user.from_user.all(), page=page)

		context.update({
			'following': paginator.get_page(select_related='to_user'),
			'user_profile': user,
			'paginate': paginator.get_page_buttons()
		})

		return context

# ----------------------------------------------------------------------------------------

class FollowUserView(UloView):

	def get(self, request, pk, *args, **kwargs):
		raise Http404()

	def post(self, request, pk, *args, **kwargs):
		
		if not request.user.is_authenticated():
			return redirect( get_referer_path(request, redirect=True) )

		if request.user.pk != pk:
			create_connection(request.user, pk)

		return redirect( get_referer_path(request) )

# ----------------------------------------------------------------------------------------

class UnFollowUserView(UloView):

	def get(self, request, pk, *args, **kwargs):
		raise Http404()

	def post(self, request, pk, *args, **kwargs):

		if not request.user.is_authenticated():
			return redirect( get_referer_path(request, redirect=True) )
		
		delete_connection(request.user, pk)
		return redirect( get_referer_path(request) )

# ----------------------------------------------------------------------------------------

