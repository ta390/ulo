"""
Ulo URL Configuration
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports

# core django imports
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

# thrid party app imports

# project imports
from generics.views import HomeView, NavigationView,  SearchView
from users.views import LoginView, LogoutView, SignUpView


# TO SERVE MEDIA FILE IN DEVELOPMENT ONLY
from django.conf.urls.static import static


# ----------------------------------------------------------------------------------------

urlpatterns = [

	# Landing Page
	url(r'^$', HomeView.as_view(), name='home'),
	# Search Page
	url(r'^search/$', SearchView.as_view(), name='search'),
	url(r'^search/', include('generics.urls', namespace='generics')),


	# User Posts (Post detail, Post form)
	url(r'^post/', include('posts.urls', namespace='posts')),
	# User Pages (Profile, Profile update)
	url(r'^user/', include('users.urls', namespace='users')),
	# User Comments
	url(r'^comment/', include('comments.urls', namespace='comments')),

	# Sign Up Page
	url(r'^signup/$', SignUpView.as_view(), name='signup'),
	# Login Page
	url(r'^login/(\?redirect_to=(?P<redirect_to>[^<>\'"@]*/))?$', 
		LoginView.as_view(), name='login'),
	# Logout Url
	url(r'^logout/$', LogoutView.as_view(), name='logout'),
	
	# Accounts Settings
	url(r'^accounts/', include('accounts.urls', namespace='accounts')),
	# User Settings
	url(r'^settings/', include('settings.urls', namespace='settings')),

	# Admin web interface
	url(r'^admin/', include(admin.site.urls)),

	# Error views
	url(r'^500/$', 'django.views.defaults.server_error', name='server_error'),

	# Dedicated site navigation when js is disabled.
	url(r'^navigation/$', NavigationView.as_view(), name='navigation'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# ----------------------------------------------------------------------------------------