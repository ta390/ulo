# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# standard library imports
from __future__ import unicode_literals
from math import ceil
import re

# core django imports
from django.core.exceptions import DisallowedHost
from django.core.urlresolvers import reverse
from django.forms.utils import flatatt
from django.utils.html import format_html
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe

# thrid party app imports

# project imports


# HTTP REFERER PATH METHOD
# ----------------------------------------------------------------------------------------

def get_referer_path(request, redirect=False):
	"""
	Return the previous path or 'home' if one could not be found.
	"""
	try:
		if redirect:
			prefix = reverse('login')+'?redirect_to='
		else:
			prefix = ''

		referer = request.META.get('HTTP_REFERER')
		if referer is not None:
			host, path = re.sub('^https?:\/\/', '', referer).split('/', 1)
			if host != request.get_host():
				raise ValueError('Host and Referer are not equal.')
		else:
			path = request.POST.get('http_referer', '')[1:]
			if path is None:
				raise ValueError('Http Referer not found.')

		return prefix+'/'+path

	except (DisallowedHost, ValueError):
		return prefix+reverse('home')

# ----------------------------------------------------------------------------------------

class UloPaginator(object):

	def __init__(self, object_list, page, per_page=20, max_results=1000):
		self.overflow = False
		self.max_results = max_results
		self.per_page = int(per_page)
		self.object_list = object_list
		self.num_pages = self.get_num_pages(self.object_list, self.per_page, self.max_results)
		self.page = self.validate_number(page, self.num_pages)

	def get_page(self, select_related=''):
		
		start = (self.page-1) * self.per_page

		results = self.page*self.per_page
		if results > self.max_results:
			end = start + (results - self.max_results)
			self.overflow = True
		else:
			end = start + self.per_page
			self.overflow = False
		
		if select_related:
			return self.object_list[start:end].select_related(select_related)
		return self.object_list[start:end]


	def get_page_buttons(self, attrs={}, span=5):
		offset = int(span/2)
		start = max( (self.page-offset), 1 )
		end = min(start+span, self.num_pages)
		
		output = []
		if self.has_previous():
			output.append( self.as_html('back', self.page-1, attrs) )

		for pg in range(start, end):
			disable = (pg == self.page)
			output.append( self.as_html(pg, pg, attrs, disable) )

		if self.has_next(offset):
			output.append( self.as_html('next', self.page+1, attrs) )
		elif self.num_pages > 1:
			disable = (self.num_pages == self.page)
			output.append( self.as_html(self.num_pages, self.num_pages, attrs, disable) )

		return mark_safe('\n'.join(output))


	def as_html(self, name, pg, attrs={}, disable=False):
		attrs['href'] = '?page='+force_text(pg) if disable == False else '#'
		return format_html('<a {}>'+force_text(name)+'</a>', flatatt(attrs))

	def has_next(self, offset):
		return (self.page+offset) < self.num_pages

	def has_previous(self):
		return self.page > 1

	def get_num_pages(self, object_list, per_page, max_results):
		num_of_pages = ceil(object_list.count() / per_page)
		return num_of_pages if num_of_pages*per_page < max_results else ceil(max_results / per_page)


	def validate_number(self, number, maximum):
		"""
		Validates the given 1-based page number.
		"""
		try:
			number = int(number)
		except (TypeError, ValueError):
			return 1

		if number < 1:
			return 1
		if number > maximum:
			return maximum

		return number

# ----------------------------------------------------------------------------------------

