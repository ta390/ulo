# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.files.uploadhandler import UploadFileException
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_control
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import FormView, RedirectView, View
from django.views.generic.base import ContextMixin


# thrid party app imports

# project imports
from .viewmixins import AjaxRequestMixin, AjaxFormMixin


# ----------------------------------------------------------------------------------------

class UloView(AjaxRequestMixin, ContextMixin, View):
	"""
	Minimal view to display a page and handle ajax get requests.
	"""
	pass

# ----------------------------------------------------------------------------------------

@method_decorator((sensitive_post_parameters(),), name='dispatch')
@method_decorator((cache_control(no_cache=True, must_revalidate=True, no_store=True, private=True),), name='get')
class UloRedirectView(RedirectView):
	"""
	"""
	permanent = False;
	query_string = True


# ----------------------------------------------------------------------------------------

class UloFormView(AjaxRequestMixin, AjaxFormMixin, FormView):
	"""
	Django's FormView with added ajax support.
	"""
	pass

# ----------------------------------------------------------------------------------------

class UloUpdateView(AjaxRequestMixin, AjaxFormMixin, FormView):
	"""
	Django's FormView with added ajax support / instance for updating a model.
	"""

	def get_object(self):
		raise NotImplementedError(
			'subclasses of UloUpdateView must define get_object()'
		)

	def get_form_kwargs(self):
		kwargs = super(UloUpdateView, self).get_form_kwargs()
		kwargs.update({'instance': self.get_object()})
		return kwargs	

# ----------------------------------------------------------------------------------------

@method_decorator(csrf_exempt, name='dispatch')
class UloUploadView(LoginRequiredMixin, UloView):
	"""
	A csrf exempt view to change the default upload handlers and then call the 
	'view_handler' to process the request. File uploads are restricted to js/ajax uploads
	only. Subclasses must define 'template_name', 'form_class' and 'view_handler' which
	must be a csrf_protected view that will handle the post and get requests once the 
	handlers have run.
	"""

	redirect_field_name = 'redirect_to'
	template_name = None
	view_handler = None
	form_class = None

	def set_handlers(self):
		pass

	def get_context_data(self, **kwargs):
		if 'form' not in kwargs:
			kwargs['form'] = self.form_class()
		return kwargs

	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, self.get_context_data(**kwargs))

	def post(self, request, *args, **kwargs):
		try:
			if request.is_ajax():
				self.set_handlers()
				return self.view_handler.as_view()(request)

		except UploadFileException:
			# If you land here the request object will be empty and a network error is 
			# likely to have occurred, but render the form with an error just in case.
			kwargs['form'] = self.get_error_form()

		# only accept post requests made via ajax.
		return self.get(request, *args, **kwargs)
	

	def get_error_form(self, ):
		form = self.form_class()
		form.add_error(None, 
			_('Sorry, we cannot process your request.')
		)
		return form

# ----------------------------------------------------------------------------------------

