# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.core.urlresolvers import reverse
from django.http import HttpResponseNotFound, JsonResponse
from django.utils.http import urlquote
from django.shortcuts import get_object_or_404, render, redirect
from django.template.loader import render_to_string
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_control, never_cache
from django.views.decorators.csrf import csrf_exempt, ensure_csrf_cookie
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.vary import vary_on_headers

# thrid party app imports

# project imports
from users.models import User

# ----------------------------------------------------------------------------------------

class AjaxRequestMixin(object):
	"""
	Mixin to add Ajax support to for get requests. Relies on the template_name and context 
	variables to render the page.
	"""
	# template context
	context = {}

	# prevents the history API back/forward from rendering JSON on cached pages.
	@method_decorator(vary_on_headers('X-Requested-With'))
	def dispatch(self, *args, **kwargs):
		return super(AjaxRequestMixin, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(AjaxRequestMixin, self).get_context_data(**kwargs)
		context.update(self.context)
		return context
	
	def get(self, request, *args, **kwargs):
		if request.is_ajax():
			html = render_to_string(
				template_name=self.template_name, 
				context=self.get_context_data(**kwargs),
				request=self.request
			)
			url = urlquote(request.get_full_path(), safe="?/=")
			return JsonResponse({'html':html, 'url':url}, status=200)

		return render(request, self.template_name, self.get_context_data(**kwargs))

# ----------------------------------------------------------------------------------------
@method_decorator((sensitive_post_parameters(), ensure_csrf_cookie), name='dispatch')
class AjaxFormMixin(object):
	"""
	Mixin to add Ajax support to forms.
	Must be used with an object-based FormView
	"""

	# specify the name of the form to be used in the template context
	form_context_name = 'form'


	def dispatch(self, *args, **kwargs):
		return super(AjaxFormMixin, self).dispatch(*args, **kwargs)

	def get_initial(self):
		initial = self.initial.copy()
		initial.update({'http_referer': self.request.path_info or ""})
		return initial

	def form_invalid(self, form):
		if self.request.is_ajax():
			return JsonResponse(form.errors, status=400)
		else:
			# call render_to_response directly to pass form using the form_context_name
			return self.render_to_response(self.get_context_data(
				**{ self.form_context_name: form }
			))

	def form_valid(self, form):
		if self.request.is_ajax():
			return JsonResponse({'redirect_url': self.get_success_url()})
		else:
			return super(AjaxFormMixin, self).form_valid(form)

	def get_context_data(self, **kwargs):
		# alter FormMinin's behaviour to specify the name of the form.
		# https://github.com/django/django/blob/master/django/views/generic/edit.py
		if self.form_context_name not in kwargs:
			kwargs[self.form_context_name] = self.get_form()
		# copy of ContentMixin's behaviour.
		# https://github.com/django/django/blob/master/django/views/generic/base.py
		if 'view' not in kwargs:
			kwargs['view'] = self
		# return kwargs without calling super to prevent duplicate forms being added to
		# the context when form_context_name is not the default 'form'.
		return kwargs

# ----------------------------------------------------------------------------------------

@method_decorator(
	(cache_control(no_cache=True, must_revalidate=True, no_store=True, max_age=0, private=True),
	 vary_on_headers('User-Agent', 'Cookie'),), 
	 name='dispatch'
)
@method_decorator((sensitive_post_parameters(),), name='post')
class LogoutRequiredMixin(object):
	"""
	Mixin that ensures the view is only rendered if the user is logged out. Used for
	sign up and login views.
	"""
	# should the view raise an exception if the user is not logged in.
	raise_exception = False
	# parameter passed to reverse to determine the redirect url if permission is denied
	redirect_to = 'home'

	def dispatch(self, request, *args, **kwargs):
		if request.user.is_authenticated():
			return self.handle_no_permission()
		return super(LogoutRequiredMixin, self).dispatch(request, *args, **kwargs)

	def post(self, request, *args, **kwargs):
		return super(LogoutRequiredMixin, self).post(request, *args, **kwargs)

	def handle_no_permission(self):
		if self.raise_exception:
			return HttpResponseNotFound('page not found')
		return redirect(reverse(self.redirect_to))

# ----------------------------------------------------------------------------------------

class OwnerRequiredMixin(object):
	"""
	Redirect a request if the current user is not the user of the page being
	accessed
	"""
	# should the view raise an exception if the user is not logged in.
	raise_exception = False
	# parameter passed to reverse to determine the redirect url if permission is denied
	redirect_to = 'home'

	def is_owner(self):
		raise NotImplementedError(
			'subclasses of OwnerRequiredMixin must define is_owner'
		)

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_authenticated():
			if self.is_owner():
				return super(OwnerRequiredMixin, self).dispatch(*args, **kwargs)
			return self.handle_no_permission()

		return redirect(reverse('login'))

	def handle_no_permission(self):
		if self.raise_exception:
			return HttpResponseNotFound('page not found')
		return redirect(reverse(self.redirect_to))

# ----------------------------------------------------------------------------------------



