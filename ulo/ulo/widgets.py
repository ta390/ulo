# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from datetime import date

# core django imports
from django.forms import widgets
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _

from django.utils.html import conditional_escape, format_html, html_safe
from django.utils.safestring import mark_safe

# thrid party app imports

# project imports


# ----------------------------------------------------------------------------------------


class _UloSelectDateWidget(widgets.MultiWidget):
    """
    Split DateField into day, month and year select widgets. 
    (See value_from_datadict below for use or use within UloDOBField only)
    Django docs implementation:
    See https://docs.djangoproject.com/en/1.9/ref/forms/widgets/#multiwidget
    """

    def __init__(self, attrs=None, **kwargs):
        # age restriction
        self.min_age = kwargs.get('min_age') or 0

        # days
        days = [(0, _('Day'))] + [(day, day) for day in range(1, 32)]
        # months
        months = [ (0, _('Month')), (1, _('Jan')), (2, _('Feb')), (3, _('Mar')), 
            (4, _('Apr')), (5, _('May')), (6, _('Jun')), (7, _('Jul')), (8, _('Aug')),
            (9, _('Sep')), (10, _('Oct')), (11, _('Nov')), (12, _('Dec')),
        ]
        # years
        this_year = date.today().year - self.min_age
        year_range = range(this_year, this_year-(kwargs.get('no_of_years') or 120), -1)
        years = [(0, _('Year'))] + [(year,year) for year in year_range]

        _widgets = (
            widgets.Select(attrs=attrs, choices=days),
            widgets.Select(attrs=attrs, choices=months),
            widgets.Select(attrs=attrs, choices=years)
        )
        super(_UloSelectDateWidget, self).__init__(_widgets, attrs)


    def _settings_btn(self):
        "<button type='button' value='0'></button><br>"

    def decompress(self, value):
        if value:
            year, month, day = str(value).split('-')
            return [int(day), int(month), int(year)]
        return [None, None, None]

    def format_output(self, rendered_widgets):
        return rendered_widgets[0]+rendered_widgets[1]+rendered_widgets[2]
        return ''.join(rendered_widgets)

    def value_from_datadict(self, data, files, name):
        """
        Return date as a string if it is valid or return the raw list for invalid dates.
        The field should check the data type it receives in its clean method and handle
        it accordingly.
        """
        datelist = [
            widget.value_from_datadict(data, files, name + '_%s' % i)
            for i, widget in enumerate(self.widgets)]

        try:
            D = date(
                day=int(datelist[0]),
                month=int(datelist[1]),
                year=int(datelist[2]),
            )
        except ValueError:
            return datelist
        else:
            return str(D)

# ----------------------------------------------------------------------------------------

class UloUlWidget(widgets.Widget):


    outer_html = '<ul{id_attr}{class_attr}>{heading}{content}</ul>'
    inner_html = None
    heading_html = None

    def __init__(self, attrs=None, **kwargs):
        
        self.choices = kwargs.pop('choices')
        self.heading = kwargs.pop('heading', None)
        self.heading_html = kwargs.pop('heading_html', '<div class="heading"><h4>{}</h4></div>')

        self.li_attrs = kwargs.pop('li_attrs', {})
        self.inner_html = self.get_inner_html(self.li_attrs)

        super(UloUlWidget, self).__init__(attrs, **kwargs)


    def get_inner_html(self, attrs):

        li = '<li{}>{}</li>'
        li_attrs = ''

        for a in attrs:
            li_attrs += ' {'+a+'}'

        return li.format(li_attrs, '{value}')


    
    def generate_attrs(self, i):
            
        kwargs = {}
        for a in self.li_attrs:
            try:
                val = force_text(self.li_attrs[a][i])
                kwargs[a] = a+'='+val if val else ''

            except (KeyError, IndexError):
                kwargs[a] = ''

        return kwargs


    def choice_input_class(self, choice, i):
        return force_text(choice), self.generate_attrs(i)

    def render(self, name, value, attrs=None, **kwargs):
        
        """
        Outputs a <ul> for this set of choice fields.
        If an id was given to the field, it is applied to the <ul> (each
        item in the list will get an id of `$id_$i`).
        """
        
        output = []
        
        for i, choice in enumerate(self.choices):

            value, attrs = self.choice_input_class(choice, i)
            output.append(format_html(self.inner_html, value=value, **attrs))
        

        id_ = self.attrs.get('id')
        class_ = self.attrs.get('class')

        return format_html(
            self.outer_html,
            id_attr=format_html(' id="{}"', id_ or 'id_'+name),
            class_attr=format_html(' class="{}"', class_) if class_ else '',
            heading=format_html(self.heading_html, self.heading) if self.heading else '',
            content=mark_safe('\n'.join(output)),
        )

