# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# standard library imports
from __future__ import unicode_literals

# core django imports
from django.forms import CharField, Form, HiddenInput, PasswordInput
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports


# FORM MIXINS
# ----------------------------------------------------------------------------------------

class UloBaseFormMixin(object):

	unknown_error = 'Your form could not be processed. Please try again later.'

	def __init__(self, *args, **kwargs):
		kwargs.setdefault('label_suffix', '')
		super(UloBaseFormMixin, self).__init__(*args, **kwargs)
		
	def http_referer(self):
		from django.utils.safestring import mark_safe
		value = self.initial.get('http_referer', '')
		return mark_safe('<input type="hidden" name="http_referer" value="'+value+'">')

	def add_nonfield_error(self, msg=None):
		self.errors.clear()
		self.add_error(None, _(msg or self.unknown_error))

# ----------------------------------------------------------------------------------------

class CleanUsernameMixin(object):
	"""
	Mixin to check that the username is not a reserved username.
	"""

	_reserved_usernames = (
		'anonymous', 'help', 'support', 'ulo', 'user', 'users'
	)

	def clean_username(self):
		"""
		Check that the username is unique and return normalised string (all lowercase).
		"""
		username = self.cleaned_data.get('username', '').lower()
		if username in self._reserved_usernames:
			raise ValidationError(
				_('This username has been taken.'),
				code='reserved_username',
			)
		return username

# ----------------------------------------------------------------------------------------

