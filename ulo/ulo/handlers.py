# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# standard library imports
from __future__ import unicode_literals
from io import BytesIO
from PIL import Image
from sys import getsizeof
from uuid import uuid4

# core django imports
from django.core.files.uploadhandler import (
	FileUploadHandler, MemoryFileUploadHandler, StopUpload, TemporaryFileUploadHandler, 
	UploadFileException, SkipFile, StopFutureHandlers
)

from django.core.files.uploadedfile import TemporaryUploadedFile
from django.template.defaultfilters import filesizeformat
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports


# ----------------------------------------------------------------------------------------

# return the error message added to the request object when uplaod fails
def get_error(request):
	if hasattr(request, 'upload_error'):
		return request.upload_error
	return None

# ----------------------------------------------------------------------------------------

class JpegFileConversionMixin(object):
	"""
	Convert an image to a JPEG and compress the file using PIL.
	"""

	def __init__(self, *args, **kwargs):

		# max file resolution (resolution x resolution)
		self.res = int(kwargs.pop('resolution', 2048))
		# set max pixels so that PIL can check for a decompression bomb
		Image.MAX_IMAGE_PIXELS = self.res*self.res +1
		Image.warnings.simplefilter('error', Image.DecompressionBombWarning)

		super(JpegFileConversionMixin, self).__init__(*args, **kwargs)


	def file_conversion(self, image, file_size):
		"""
		Convert the file into a new format and set self.file to the new file.
		"""
		raise NotImplementedError(
			'subclasses of JpegFileCompleteMixin must define file_conversion'
		)

	def file_complete(self, file_size):
		"""
		Based on:
		http://dustindavis.me/django-custom-upload-handler-to-compress-image-files/
		Convert and compress the file using PIL.
		"""
		try:
			# check that the previous handler returned a file
			if not hasattr(self, 'file'):
				return None

			# go to the start of the file
			self.file.seek(0)
			# set file name
			self.file_name = uuid4().hex+'.jpg'
			# update the content type
			self.content_type = 'image/jpeg'


			# verify that the file is a valid image. See Django's ImageField
			# https://github.com/django/django/blob/master/django/forms/fields.py
			Image.open(self.file).verify()
			# reopen the image after running verify() to continue
			image = Image.open(self.file)
		

			# convert to RGB so the file is ready for jpeg conversion
			if image.mode != 'RGB': image = image.convert('RGB')
			# convert the file to jpeg
			self.file_conversion(image, file_size)

			return super(JpegFileConversionMixin, self).file_complete(file_size)
		
		# If an exception is raised add the error to the request and close the file so 
		# that an empty MultiValueDict is returned to the view.
		except IOError:
			self.request.upload_error = \
				_('We could not process your image, it may be corrupted.')
		except Image.DecompressionBombWarning:
			self.request.upload_error = \
				_('Please upload an image less than %s by %s') %(self.res, self.res)
		except UploadFileException as e:
			self.request.upload_error = _('Something has gone wrong, we are looking '
				'into the problem. Please try again later.')
		
		self.file.close()
		
# ----------------------------------------------------------------------------------------

class MemJpegCompressionHandler(JpegFileConversionMixin, MemoryFileUploadHandler):

	def file_conversion(self, image, file_size):
		"""
		Use a temporary file in memory.
		"""
		tmp = BytesIO()
		image.save(tmp, 'JPEG', quality=80)
		self.file = tmp

	def file_complete(self, file_size):
		if not self.activated:
			return
		return super(MemJpegCompressionHandler, self).file_complete(file_size)

# ----------------------------------------------------------------------------------------

class TmpJpegCompressionHandler(JpegFileConversionMixin, TemporaryFileUploadHandler):

	def file_conversion(self, image, file_size):
		"""
		Use a temporary file on disk.
		"""
		tmp = TemporaryUploadedFile(self.file_name, 'image/jpeg', file_size, 'binary')
		image.save(tmp.temporary_file_path(), 'JPEG', quality=80)
		self.file = tmp

# ----------------------------------------------------------------------------------------

class FileSizeAndTypeHandler(FileUploadHandler):
	"""
	Check that the file size and type are valid, otherwise abort the upload. This will
	cause a network error and not return any information back to the user.
	"""
	
	# list of allowed file types (excluding the prefix image/ or video/). '*' means all
	ALL_TYPES = '*'
	FILE_TYPES = { 
		'image': ('jpg','jpeg','pjpeg','png','gif', 'tiff'),
		'video': (ALL_TYPES,)
	} 

	def __init__(self, *args, **kwargs):
		# number of bytes uploaded.
		self.bytes = 0
		# max number of bytes that can be uploaded
		self.max_bytes = kwargs.pop('max_bytes', 10485760) #10MB
		# type to validate (image or video).
		self.type = kwargs.pop('type', 'image')

		super(FileSizeAndTypeHandler, self).__init__(*args, **kwargs)

	def new_file(self, field_name, file_name, content_type, content_length, charset, 
		content_type_extra):

		# check length of file if given.
		if not content_length and self.request:
			content_length = int(self.request.META.get('CONTENT_LENGTH', 0))
		if content_length and content_length > self.max_bytes:
			raise StopUpload(connection_reset=True)

		# check file mime type.
		if content_type and not self.valid_type(content_type):
			raise StopUpload(connection_reset=True)

		super(FileSizeAndTypeHandler, self).new_file(
			field_name, file_name, content_type, content_length, charset, content_type_extra
		)

	def receive_data_chunk(self, raw_data, start):
		"""
		Check that the number of bytes received does not exceed the max.
		"""
		self.bytes += getsizeof(raw_data)
		if self.bytes > self.max_bytes:
			raise StopUpload(connection_reset=True)
		return raw_data

	def file_complete(self, file_size):
		return None

	def valid_type(self, content_type):
		"""
		Check that the content_type is a valid type found in 'FILE_TYPES'
		"""
		try:
			media, ext = content_type.split('/', 1)
			# check that the type matches the expected type (image or video)
			if media != self.type:
				return False
			for _ext in self.FILE_TYPES[media]:
				if _ext == ext or _ext == self.ALL_TYPES:
					return True
		except (AttributeError, KeyError, TypeError, ValueError):
			pass
		return False

# ----------------------------------------------------------------------------------------

