# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals
from abc import ABCMeta, abstractmethod
from time import time

# core django imports
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.signing import (
	BadSignature, SignatureExpired, dumps, loads
)
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.six import text_type

# thrid party app imports

# project imports


# All classes are based on the source code of PasswordResetTokenGenerator
# https://github.com/django/django/blob/master/django/contrib/auth/tokens.py
# ----------------------------------------------------------------------------------------

class BaseTokenGenerator(PasswordResetTokenGenerator):
	"""
	Abstract class to create a token that can expire in minutes and serialise objects.
	Serialise objects using dumps and loads to store the token and user id using a 
	TimeStampSigner. Classes that inherit from this class can define the method 
	_make_hash_value to determine the hash value of each token.
	Note: settings.PASSWORD_RESET_TIMEOUT_DAYS will determine the max_days for all 
	subclasses of TokenGenerator
	"""
	__metaclass__ = ABCMeta

	def make_token(self, user, salt=None):
		"""
		Return a serialised object using a TimestampSigner (through dumps)
		@param user: instance of the User model
		@param salt: optional salt to namespace the signer
		"""
		# run base class make_token
		token = super(BaseTokenGenerator, self).make_token(user)
		# return a serialised time stamped object containing the token and id
		return dumps(obj={'token':token, 'uid':user.id}, salt=salt)

	def check_token(self, string, user=None, salt=None, max_age=None, fail_silently=True):
		"""
		Check that the token returned by make_token is valid and return the user, else 
		return None. If fail_silently is True raise DoesNotExist error is user is not 
		found.
		@param string: the string returned by make_token
		@param user: instance of the User model
		@param salt: optional salt used to namespace the signer
		@param max_age: optional time in seconds specifying how long the token is valid
		@param fail_silently: boolean to indicate is a user does not exist error
		should be raised.
		"""
		try:
			UserModel = get_user_model()
			# unsign/unserialise the data (token, user id)
			obj = loads(string, salt=salt, max_age=max_age)

			# check user id
			if user is None: 
				user = get_user_model().objects.get(pk=obj.get('uid'))
			elif user.pk != obj.get('uid'):
				return None

			# check token and return True if valid and False if not
			if super(BaseTokenGenerator, self).check_token(user, obj.get('token')):
				return user
			return None

		except (BadSignature, SignatureExpired, UserModel.DoesNotExist) as e:
			if fail_silently:
				return None
			raise e	

	@abstractmethod
	def _make_hash_value(self, user, timestamp):
		"""
		Each implementation should include the pk to identify the account.
		"""
		raise NotImplementedError(
			'subclasses of BaseTokenGenerator must define _make_hash_value'
		)

# ----------------------------------------------------------------------------------------

class ResetPasswordTokenGenerator(BaseTokenGenerator):
	"""
	Inherit PasswordResetTokenGenerator to add the user id to the token.
	"""

	def _make_hash_value(self, user, timestamp):
		# Ensure results are consistent across DB backends
		return (
			text_type(user.pk) +
			user.email + 
			user.password +
			text_type(timestamp)
		)

# ----------------------------------------------------------------------------------------

class VerifyEmailTokenGenerator(BaseTokenGenerator):
	"""
	Class to create a token that remains valid until the user confirms their email 
	address (or max_age).
	"""

	def _make_hash_value(self, user, timestamp):
		"""
		Create a hash value based on the user's 'email_confirmed' value which will only 
		change when a user verifies their email address and the email itself should 
		a user change the email address associated with their account.
		"""
		return (
			text_type(user.pk) +
			user.email +
			text_type(user.email_confirmed) +
			text_type(timestamp)
		)

# ----------------------------------------------------------------------------------------

class GenericTokenGenerator(BaseTokenGenerator):
	"""
	Class to create a generic token that is not linked to changing user profile
	information.
	"""
	
	def _make_hash_value(self, user, timestamp):
		"""
		Create a hash value based on the user's profile information that is likely to not
		change.
		"""
		return (
			text_type(user.pk) +
			user.email +
			text_type(timestamp)
		)

# ----------------------------------------------------------------------------------------

