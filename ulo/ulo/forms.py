# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# standard library imports
from __future__ import unicode_literals
from math import ceil

# core django imports
from django.contrib.auth import get_user_model
from django.forms import CharField, Form, ModelForm, TextInput, ValidationError
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports
from .formmixins import UloBaseFormMixin
from ulo.fields import ReadOnlyPasswordField

# ----------------------------------------------------------------------------------------

class UloForm(UloBaseFormMixin, Form):
	pass

# ----------------------------------------------------------------------------------------

class UloModelForm(UloBaseFormMixin, ModelForm):
	pass

# ----------------------------------------------------------------------------------------

class UloSecureModelForm(UloBaseFormMixin, ModelForm):
	"""
	Model form that requires a user to enter their password in order to change the model
	instance. Heavily based on Django's UserChangeForm.
	https://github.com/django/django/blob/master/django/contrib/auth/forms.py
	"""

	user = None

	# ReadOnlyPasswordField is a wrapper around ulo.fields.PasswordField that does not
	# bind the password to the form. Heavily based on Django's ReadOnlyPasswordHashField
	# https://github.com/django/django/blob/master/django/contrib/auth/forms.py
	password = ReadOnlyPasswordField(
		label=_('Current password'),
	)

	def __init__(self, *args, **kwargs):
		super(UloSecureModelForm, self).__init__(*args, **kwargs)
		f = self.fields.get('user_permissions')
		if f is not None:
			f.queryset = f.queryset.select_related('content_type')

	# check that the password entered matches their current password.
	def clean_password(self):
		password = self.cleaned_data.get('password', '')

		if not password:
			raise ValidationError(
				_('Please enter your current password.'),
				code='required',
			)

		user = self.instance if hasattr(self.instance, 'check_password') else self.user
		if not user.check_password(password):
			raise ValidationError(
				_('Password incorrect.'),
				code='password_incorrect',
			)
		return self.initial.get('password', '')

# ----------------------------------------------------------------------------------------

