# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.contrib import messages
from django.db.models import Q
from django.http import Http404, HttpResponse, JsonResponse
from django.shortcuts import render
# thrid party app imports

# project imports
from .utils import es
from posts.models import Post
from ulo.utils import get_referer_path
from ulo.views import UloView


# ----------------------------------------------------------------------------------------


# ----------------------------------------------------------------------------------------

class HomeView(UloView):
	template_name = 'generics/home.html'

	def get(self, request, *args, **kwargs):
		return super(HomeView, self).get(request, *args, **kwargs)

# ----------------------------------------------------------------------------------------

class NavigationView(UloView):
	template_name = 'generics/navigation.html'

	def get(self, request, *args, **kwargs):
		return super(NavigationView, self).get(request, *args, **kwargs)

# ----------------------------------------------------------------------------------------

class AutocompleteView(UloView):

	def get(self, request, q, *args, **kwargs):
		
		if request.is_ajax():
			
			return JsonResponse(es.auto_complete_all(q))

		raise Http404()

	def head(self, request, q, *args, **kwargs):
		pass
		

# ----------------------------------------------------------------------------------------

from django.contrib.auth import get_user_model
from users.search import create_user_index, create_connection_index
from posts.search import create_post_index, create_post

from .forms import SearchFilters
class SearchView(UloView):

	template_name = 'generics/search.html'

	def get(self, request, *args, **kwargs):
		# get the search query
		q = request.GET.get('q', '').strip()
		page = self.request.GET.get('page', 0)
		type_ = self.request.GET.get('type')
		date = self.request.GET.get('date')
		sort = self.request.GET.get('sort')

		users_following, posts = es.the_search(q, page, request.user.pk, type_, date, sort)

		
		kwargs['user_results'] = users_following[0]
		kwargs['following'] = users_following[1]
		kwargs['post_results'] = posts

		if request.is_ajax():
			return JsonResponse(kwargs, status=200)
		else:
			kwargs['q'] = q
			kwargs['filters'] = SearchFilters()
			return render(request, self.template_name, self.get_context_data(**kwargs))

		# posts = Post.objects.create(title='Its an exclusive', user_id=request.user.pk)
		# 4, 5, 6, 7, 8

		# search = Post.objects.extra(select={'_id': 'id'}).all().values()

		# search = create_post(posts, request.user, params={'routing': '123'})
		#search = es.reindex('posts_index_0', 'posts_index_1')
		# kwargs['es'] = search


	
		# 'posts_alias' #'posts_index_alias', #'posts_search_alias'
		# search = es.get_alias_indices('posts_alias')
		# search = es.get_index_aliases('posts_index_0')
		
		# search = es.reindex('posts', 'posts2', '2016-01-01', '2016-04-01')
		# search = es.search_filter()
		# search = update_post_user_data_v0(es, user['id'], user)

		# new_data = []
		# for d in data:
		# 	output = d.picture.url+' '+d.name+' '+d.username
		# 	mapping = es.document_mapping([d.name, d.username], 'user_suggest')
		# 	mapping.update( {'id': d.id} )
		# 	new_data.append( mapping )

		# search = es.create_document_bulk(data, 'users', 'user', field_map=['name', 'username'], suggest='user_suggest')
		#search = es.update_document_bulk(new_data, 'users', 'user')
		#search = es.list_all_indices()
		# mapping = es.mapping('user', 'user_suggest', 
		# 	{'name': {'type':'string', 'analyzer': 'name_analyzer', 'search_analyzer': 'name_search_analyzer'},
		# 	'username': {'type':'string', 'analyzer': 'name_analyzer', 'search_analyzer': 'name_search_analyzer'}}
		# )
		# search = es.create_index('users')
		# search = es.delete_index('users')
		# search = es.get_document('posts', 'post', '22')
		#search = es.add_field('name', 'ٳڢڪڸܢ','users', 'user', 15)

		# search = es.delete_index('users_index_0')
		# search = create_user_index(es)
		# search = create_user_document_v0(es)
		# search = update_user_document(es, {'age': 30, 'id': user['id']} )
		# search = es.test_analyser('users', 'name_analyzer', "ta390")
		# search = es.auto_complete('users', "t'b", 'user_suggest')
		# search = es.search_suggestions('users', "t")
		# search = es.debug_tokens('users', 'name')

		# search = es.delete_index_alias('_all', '*')
		# search = es.delete_index('posts_index_0')
		# search = create_post_index(es)
		# search = create_post_document_v0(es)
		# search = update_user_document(es, {'age': 30, 'id': user['id']} )
		# search = es.test_analyser('posts', 'post_analyzer', "Fetty Wap '679' feat. Remy Boyz [Official Video]")
		# search = es.auto_complete_all(q)
		# search = es.search_suggestions('posts', "k")
		# search = es.debug_tokens('posts', 'title_tokens')
		# kwargs['es'] = search

		# if q:
		# 	from django.http import JsonResponse
		# 	return JsonResponse({"html":search}, status=200)
		return super(SearchView, self).get(request, *args, **kwargs)

# ----------------------------------------------------------------------------------------

