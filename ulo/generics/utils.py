# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals
import re, requests

# core django imports
from django.db import DatabaseError, transaction

# thrid party app imports

# project imports
from .constants import *
from .serialisers import serialiser
from posts.utils import IMAGE, VIDEO


# ELASTICSEARCH 
# ----------------------------------------------------------------------------------------

FILTER_DATE = {
	DATE_DAY: '',
	DATE_WEEK: '-1w',
	DATE_MONTH: '-1M',
	DATE_YEAR: '-1y'
}

# IF FILTER_TYPE CHANGES, UPDATE UloElasticsearch get_search_function()
FILTER_TYPE = {
	TYPE_USERS: TYPE_USERS ,
	TYPE_POSTS:	TYPE_POSTS,
	TYPE_IMAGE: IMAGE,
	TYPE_VIDEO: VIDEO
}
FILTER_SORT = {
	SORT_RELEVANCE: None,
	SORT_DATE: [{POST_DATE_FIELD: {'order':'desc', 'unmapped_type':'date'}}],
	SORT_VIEWS: [{POST_VIEWS_FIELD: {'order':'asc', 'unmapped_type':'long'}}],
	#SORT_FOLLOWERS: {USER_FOLLOWERS_FIELD: {'order':'asc', 'unmapped_type':'integer'}},
}


# ELASTICSEARCH EXCEPTIONS
# ----------------------------------------------------------------------------------------

class EsException(Exception):
	pass

class EsIndexExists(EsException):
	pass

class EsIndexNotFound(EsException):
	pass

class EsMapper(EsException):
	pass

class BulkProcessing(EsException):
	pass


# ELASTICSEARCH WRAPPER
# ----------------------------------------------------------------------------------------

class UloElasticsearch(object):
	"""
	Wrapper around elasticsearch API.
	"""

	# VARIABLES
	# ------------------------------------------------------------------------------------

	# the url to use when querying the elasticsearch API
	url = 'http://localhost:9200/'
	# index.max_result_window defaults to 10000.
	max_result_window = 10000

	# ------------------------------------------------------------------------------------

	def __init__(self, url=None):
		"""
		Initialise the object with the url to query for each request.
		@param url: url in the format https://domain.com
		"""
		if url:
			if not re.match(r'^https?:\/\/[^/]+$', url):
				raise ValueError("Url must be in the format 'https://domain.com'")
			else:
				self.url = url
	
	# DATABASE FUNCTIONS
	# ------------------------------------------------------------------------------------
	def create_instance(self, instance, doc, index, index_type, **kwargs):
		"""
		Save a database instance and create its elasticsearch document as an atomic 
		action. Return the instance if successfull and None if not.
		"""
		
		try:
			kwargs['upsert'] = True
			callback = kwargs.pop('callback', None)
			with transaction.atomic():
				instance.save()
				if callback:
					doc.update( callback(doc, instance) )
				self.update_document(doc, index, index_type, instance.pk, **kwargs)

		except (DatabaseError, EsException) as e:
			raise DatabaseError(
				'Save instance to database and create elastic search document failed.'
			)

		return instance

	def update_instance(self, instance, doc, index, index_type, **kwargs):
		"""
		"""
		try:
			callback = kwargs.pop('callback', None)
			with transaction.atomic():
				instance.save()
				if callback:
					doc.update( callback(doc, instance) )
				self.update_document(doc, index, index_type, instance.pk, **kwargs)

		except (DatabaseError, EsException):
			raise DatabaseError(
				'Save instance to database and update elastic search document failed.'
			)

		return instance


	def delete_instance(self, instance, index, index_type):
		"""
		"""
		try:
			with transaction.atomic():
				instance.delete()
				self.delete_document(index, index_type, instance.pk)

		except (DatabaseError, EsException):
			raise DatabaseError(
				'Delete instance from database and delete elastic search document failed.'
			)

		return instance


	# HELPERS
	# ------------------------------------------------------------------------------------
	def process_response(self, response, uid='', fail_silently=False, return_data=False):
		"""
		Process the request response and return True/False or raise an exception.
		@param response: the response returned by a request (e.g. request.get(url)).
		@param uid: an id to add to the error message (typically the function name).
		@param fail_silently: if True return False for failed requests else raise an 
		exception.
		@param return_data: if True return the response as a json object else return
		True.
		"""
		# get the response as a json object
		json = response.json()

		# handle response error
		if 'error' in json.keys():

			if fail_silently == True:
				return False

			try:
				raise EsException(uid+': '+json['error']['reason'])
			except KeyError:
				raise EsException('process_response: Request Failed.')

		if return_data == True:
			return json

		return True
	# ------------------------------------------------------------------------------------
	def get_url(self, *args, params={}):
		"""
		Join arguments together to build a url.
		"""
		url_params ='?'+'&'.join(p+'='+params[p] for p in params)
		return self.url+'/'.join(u for u in args)+ url_params


	# SEARCH SCROLL FUNCTIONS
	# ------------------------------------------------------------------------------------
	def scroll(self, scroll_id, time='1m'):
		"""
		Return the next batch of a scroll request.
		@param scroll_id
		@param time: timeout of the scroll requests.
		"""
		data = serialiser.dumps({
			'scroll': time,
			'scroll_id': scroll_id
		})

		url = self.get_url('_search', 'scroll')
		return requests.get(url, data=data).json()
	# ------------------------------------------------------------------------------------
	def _build_header(self, hit, headers=('_index', '_type', '_id', '_routing')):
		header = {}
		for h in headers:
			try:
				header[h] = hit.pop(h)
			except KeyError:
				pass
		return header
	def _data_processing(self, method, header, doc):
		"""
		Helper function called by user defined data_processing functions
		@param method: request type (create, update, delete)
		@param header: bulk API header
		@param doc: the document.
		"""
		data =  serialiser.dumps({ method: header }) + '\n'
		data += serialiser.dumps({ 'doc': doc }) + '\n'
		return data
	def _scroll_bulk_processing(self, hits, scroll_id, time, **kwargs):
		"""
		Perform a bulk method for each batch returned by the scroll request. Raise
		BulkProcessing exception if any docs fail.
		@param method: bulk method (create, update, delete)
		@param hits: first batch returned by the scroll request.
		@param scroll_id: id of the scroll request currently in progress.
		@param time: timeout for the scroll request.
		@param **kwargs: passed to the _document_data function for custom data processing.
		"""

		# bulk url
		url = self.get_url('_bulk')
		# handle to response the response if an error occurs
		response = None

		data_processing = kwargs['callback']

		while hits != []:
			try:
				data = ''
				for hit in hits:
					data += data_processing(hit, **kwargs)

				# handle the response of a bulk request
				self._document_bulk_request(url, data)
				# get the next scroll response and extract the new batch
				response = self.scroll(scroll_id, time)
				hits = response['hits']['hits']
					
			except KeyError as e:
				error = '_scroll_bulk_processing: KeyError '
				if hasattr(response, 'status') and response.status == 404:
					e = 'Scroll Timed Out'
				raise BulkProcessing(error+str(e))

		return True
	# ------------------------------------------------------------------------------------
	def process_scroll(self, response, time, **kwargs):
		"""
		Process a scroll response via the bulk API.
		@param response: initial scroll response.
		@param time: timeout for the scroll request.
		@param index: index name to use for all documents (optional).
		"""
		try:
			scroll_id = response['_scroll_id']
			hits = response['hits']['hits']
		except KeyError:
			raise BulkProcessing(response)

		# create new documents in the new index
		return self._scroll_bulk_processing(hits, scroll_id, time, **kwargs)


	# FULL TEXT SEARCH
	# ------------------------------------------------------------------------------------
	
	# USER SEARCH QUERIES
	# ------------------------------------------------------------------------------------
	def _users_query(self, q):
		return {
			'multi_match': {
				'query': q,
				# best_fields takes the score of the field which produced the 
				# higest score and uses it as the overall score for that document.
				'type': 'cross_fields', 
				'fields': ['name', 'username'],
				# tie_breaker takes the score(s) of the fields that lost out in 
				# the best_fields scoring (i.e. was not the top scoring field), 
				# multiplies each score by tie_breaker and adds them to the top 
				# score to give an overall score for the doc.
				'tie_breaker': 0.3,
				'minimum_should_match': '80%',
			}
		}
	# ------------------------------------------------------------------------------------
	def _search_users(self, query, q, page, size=10):
		return {
			'query': {
				'function_score': {
					'query': query,
					'field_value_factor': {
						'field': 'is_verified',
						'modifier': 'log1p'
					},
					# cap the boost value given by field_value_factor
					'max_boost': 1.0,
					# how to combine the field_value_factor value with the query score
					'boost_mode': 'sum',
				},
			},
			'from': page*size,
			'size': size,
			'_source': ['name', 'username', 'picture', '_id'],
			# rescore the top N results, taking proximity of terms into account.
			'rescore': {
				'window_size': 40,
				'query':{
					'rescore_query':{
						'match_phrase':{
							'name':{
								'query': q,
								'slop': 1,
							}
						}
					}
				}
			}
		}


	# POST SEARCH QUERIES
	# ------------------------------------------------------------------------------------
	def _posts_query(self, q):
		"""
		Post base query.
		"""
		return {
			'multi_match': {
				'query': q,
				# best_fields takes the score of the field which produced the 
				# higest score and uses it as the overall score for that document.
				'type': 'best_fields', 
				'fields': ['title^4', 'tags^3', 'title.stemmed', 'user.name', 'user.username'],
				# tie_breaker takes the score(s) of the fields that lost out in 
				# the best_fields scoring (i.e. was not the top scoring field), 
				# multiplies each score by tie_breaker and adds them to the top 
				# score to give an overall score for this doc.
				'tie_breaker': 0.3,
				'minimum_should_match': '70%'
			}
		}
	# ------------------------------------------------------------------------------------
	def _posts_filter_query(self, q, filters):
		"""
		Post base query wrapped in a filter.
		"""
		return {
			'filtered': {
				'query': self._posts_query(q),
				'filter' : {
					'bool': {
						'must': filters
						# [
						# 	{ 'term': {'media_type': 0} },
						# 	{ 'range': {'published': {'gte': 'now/d'+'-1M'}} }
						# ]
					},
				},

			}
		}
	# ------------------------------------------------------------------------------------
	def _search_posts(self, query, q, page, size=10):
		"""
		Post query sorted by relevance (default).
		"""
		return {
			'query': {
				'function_score': {
					'query': query,
					'functions': [
						{
							'linear': {
								'published': {
									# origin: set to the current day.
									'origin': 'now/d',
									# offset: the decay function will only be computed for 
									# documents with a distance greater than 'offset' from 
									# the 'origin'. (i.e. it will only kick in after the 
									# offset so the score will always be 1.0 between 
									# origin and offset and then decay according to the 
									# values of scale and decay thereafter)
									'offset': '0d',
									# scale: defines the distance from the origin+offset 
									# at which the functions score will equal decay.
									# e.g. function=linear, scale=7d, decay=0.5. In 7 days 
									# from origin+/-offset the function will give a score 
									# of 0.5 and in 14 days it will be 0 (because it's 
									# linear)
									'scale': '7d',
									# decay: the function score value at scale.
									'decay' : 0.5
								}
							},
						},
						{
							'linear': {
								'views': {
									'origin': 2000000000, # 2 billion views
									'offset': 1000000000, # 1 billion either side
									'scale':  100000000, # 100 Million
									'decay':  0.5
								}
							}
						},
					],
					# cap the boost value given by the functions
					'max_boost': 1.0,
					# how to combine the function scores
					'score_mode': 'sum',
					# how to combine the function scores with the query score
					'boost_mode': 'sum',
					# exclude scores below this value.
					'min_score': 0

				},
			},
			'from': page*size,
			'size': size,
			'_source': ['title', 'thumbnail', 'user.name', 'user.username'],
			# place users that the current user is following into its own bucket.
			'aggs': {
				'following': {
						'terms': {
							'field': '_id'
						}
				
				}
			},
			#rescore the top N results, taking proximity of terms into account.
			'rescore': {
				'window_size': 40,
				'query':{
					'rescore_query':{
						'match_phrase':{
							'title':{
								'query': q,
								'slop': 2,
							}
						}
					}
				}
			}
		}
	# ------------------------------------------------------------------------------------
	def _search_sort(self, query, sort):
		"""
		Generic query (base or filtered) sorted by the user.
		"""
		return {
			'query': query,
			'sort': sort
		}

	# SEARCH FUNCTIONS
	# ------------------------------------------------------------------------------------
	def search_users(self, q, page, uid, sort, sc):
		"""
		"""
		query = self._users_query(q)

		if sort is None or sc in POST_ONLY_SORT:
			data = self._search_users(query, q, page) 
		else:
			data = self._search_sort(query, sort)

		data = serialiser.dumps(data).encode('utf-8')
		url = self.get_url(USER_SEARCH_ALIAS, '_search')
		response = requests.get(url, data=data).json()
		return self._get_user_results(response, uid) , None
	# ------------------------------------------------------------------------------------
	def search_posts(self, q, page, fltr, date, sort, sc):
		"""
		"""
		fltrs = []
		if fltr is not None and fltr != TYPE_POSTS:
			fltrs.append({'term':{POST_TYPE_FIELD:fltr} })

		if date is not None:
			fltrs.append({'range':{POST_DATE_FIELD:{'gte': 'now/d'+date}} })

		if fltrs == []:
			query = self._posts_query(q)
		else:	
			query = self._posts_filter_query(q, fltrs)

		if sort is None or sc in USER_ONLY_SORT:
			data = self._search_posts(query, q, page) 
		else:
			data = self._search_sort(query, sort)

		data = serialiser.dumps(data).encode('utf-8')
		url = self.get_url(POST_SEARCH_ALIAS, '_search')
		response = requests.get(url, data=data).json()
		return (None, None), self._get_post_results(response)
	# ------------------------------------------------------------------------------------
	def _get_user_results(self, response, uid):

		obj = []
		ids = []

		for r in response['hits']['hits']:
			obj.append({
				'name': r['_source']['name'],
				'username': r['_source']['username'],
				'picture': r['_source']['picture'],
				'pk': r['_id']
			})
			ids.append(r['_id'])

		if uid and ids != []:
			uid = str(uid)
			url = self.get_url(CONNECTION_SEARCH_ALIAS, '_search', params={'routing': uid})
			data = serialiser.dumps({
				'query': {
					'constant_score': {
						'filter': {
							'bool': {
								'must':[
									{ 'term': { 'user_id': uid} },
									{ 'terms': { 'following_id': ids } }
								],
							},
						}
					}
				},
				'sort': ['_doc'],
				'_source': ['following_id']
			})
			ids = requests.get(url, data=data).json()['hits']['hits']
			ids = [uid['_source']['following_id'] for uid in ids]

		return (obj, ids)

	def _get_post_results(self, response):

		obj = []
		for r in response['hits']['hits']:
			print(r)
			obj.append({
				'title': r['_source']['title'],
				'name': r['_source']['user']['name'],
				'username': r['_source']['user']['username'],
			})
		return obj

	def search_all(self, q, page, uid):
		"""
		"""
		print('Q:', q)
		uq = self._users_query(q)
		data = '{"index": "%s"}\n' %(USER_SEARCH_ALIAS)
		data += serialiser.dumps(self._search_users(uq, q, page)) + '\n'
		
		pq = self._posts_query(q)
		data += '{"index": "%s"}\n' %(POST_SEARCH_ALIAS)
		data += serialiser.dumps(self._search_posts(pq, q, page)) + '\n'

		url = self.get_url('_msearch')
		responses = requests.get(url, data=data).json()['responses']
		return self._get_user_results(responses[0], uid), self._get_post_results(responses[1])
	# ------------------------------------------------------------------------------------
	def get_filter_values(self, content_type, date, sort):
		content_type = FILTER_TYPE.get(content_type, None)
		date = FILTER_DATE.get(date, None)
		sort_value = FILTER_SORT.get(sort, None)
		return content_type, date, sort_value, sort
	# ------------------------------------------------------------------------------------
	# def get_search_function(self, content_type):
	# 	return {
	# 		TYPE_USERS: self.search_users,
	# 		TYPE_POSTS:  self.search_posts,
	# 		TYPE_IMAGE:  self.search_posts,
	# 		TYPE_VIDEO:  self.search_posts,
	# 	}.get(content_type, self.search_all)
	# ------------------------------------------------------------------------------------
	def _offset_value(self, offset, size):
		"""
		Return the offset value checking that it falls within the max limit.
		See:https://www.elastic.co/guide/en/elasticsearch/reference/2.2/breaking_21_search_changes.html#_from_size_limits
		@param offset: start index (0 based) for a query
		@param size: the number of results to return.
		"""
		if (offset+size) > self.max_result_window:
			return (self.max_result_window-size)
		return offset
	# ------------------------------------------------------------------------------------
	def search(self, indices, offset=0, size=10, **kwargs):
		"""
		Return the results of a search as a json object.
		@param indices: comma separated list of indices to search
		@param offset: start index for search results.
		@param size: number of results to return starting from 'offset'
		"""

		query = serialiser.dumps({

			'query': { 
				'match_all': {},
			},
			# list of field names to return the information for
			'_source': kwargs.get('fields', []),
			# sort defaults to asc (except for _score which defaults to desc)
			# "sort": [
			# 	#{"name": "desc"},
			# 	{"age": {"unmapped_type" : "short"}}
			# ],
			# start index (0 based) of the results to return
			'from': self._offset_value(offset, size),
			# the number of results to return
			'size': size,
			# return with hits found up to this point after a max of N
			'timeout': '10s',
			# The maximum number of documents to collect for each shard, upon reaching 
			# which the query execution will terminate early.
			'terminate_after': size,
		
		})


		url = self.get_url(indices, '_search', params={'pretty':'true', 'q':'request_cache:true'})
		response = requests.post(url, data=query)
		return response.json()['hits']['hits']
	# ------------------------------------------------------------------------------------
	def the_search(self, q, page, uid, content_type=None, date=None, sort=None):
		"""
		"""
		print(content_type, date, sort)
		# get the filter values and return the original sort value (sc)
		content_type, date, sort, sc = self.get_filter_values(content_type, date, sort,)

		print(content_type, date, sort, sc)
		# if no content_type then search all - all does not take any filters.
		if content_type == None:
			return self.search_all(q, page, uid)
		# if filtering by users sort is the only additonal filter options.
		if content_type == TYPE_USERS:
			return self.search_users(q, page, uid, sort, sc)
		# if filtering by posts all filters are available.
		return self.search_posts(q, page, content_type, date, sort, sc)


	# FILTERED SEARCH
	# ------------------------------------------------------------------------------------

	# def contains_filter(self, uid):
	# 	data = serialiser.dumps({
	# 		'query' : {
	# 			'filtered' : { 
	# 				'query' : {
	# 					'match_all' : {} 
	# 				},
	# 				'filter' : {
	# 					'term' : { 
	# 						'user_id' : 13
	# 					}
	# 				}
	# 			}
	# 		},
	# 		'from': 0,
	# 		'size': 1,
	# 	})

	# 	url = self.get_url('posts', 'post', '_search')
	# 	return requests.get(url, data=data).json()

	# def category_filter(self, category):
	# 	data = serialiser.dumps({
	# 		'query' : {
	# 			'filtered' : { 
	# 				'query' : {
	# 					'match_all' : {} 
	# 				},
	# 				'filter' : {
	# 					'term' : { 
	# 						'category' : category
	# 					},

	# 				}
	# 			}
	# 		},
	# 		'from': 0,
	# 		'size': 1,
	# 	})

	# 	url = self.get_url('posts', 'post', '_search')
	# 	return requests.get(url, data=data).json()
	
	# def contains_multi_filter(self, uid):
	# 	data = serialiser.dumps({
	# 		'query' : {
	# 			'filtered' : { 
	# 				'query' : {
	# 					'match_all' : {} 
	# 				},
	# 				'filter' : {
	# 					'terms' : { 
	# 						'user_id' : [13, 14, 15]
	# 					}
	# 				}
	# 			}
	# 		},
	# 		'from': 0,
	# 		'size': 1,
	# 	})

	# 	print(uid)
	# 	url = self.get_url('posts', 'post', '_search')
	# 	return requests.get(url, data=data).json()

	# def range_filter(self, date_math=""):
	# 	data = serialiser.dumps({
	# 		'query' : {
	# 			'filtered' : { 
	# 				'query' : {
	# 					'match_all' : {} 
	# 				},
	# 				'filter' : {
	# 					'range' : { 
	# 						'published' : {
	# 							# now/d rounds down to nearest day so the result is cached daily
	# 							'gte': 'now/d'+date_math 
	# 						}
	# 					}
	# 				}
	# 			}
	# 		},
	# 		'from': 0,
	# 		'size': 1,
	# 	})

	# 	url = self.get_url('posts', 'post', '_search')
	# 	return requests.get(url, data=data).json()


	# def bool_filter(self):
	# 	data = serialiser.dumps({
	# 		'query' : {
	# 			'filtered' : { 
	# 				'filter' : {
	# 					'bool': {
	# 						# AND
	# 						'must':[
	# 							{'term': {'media_type': 'image'}}
	# 						],
	# 						# OR
	# 						'should':[
	# 						],
	# 						# NOT
	# 						'must_not':[
	# 						],
	# 					}
	# 				}
	# 			}
	# 		},
	# 		'from': 0,
	# 		'size': 1,
	# 	})

	# 	url = self.get_url('posts', 'post', '_search')
	# 	return requests.get(url, data=data).json()


	# AUTO COMPLETE
	# ------------------------------------------------------------------------------------
	# Helpers
	def _auto_complete_query(self, q, suggest, field, **kwargs):
		"""
		Return the completions suggester query.
		@param q: query string
		@param field: suggest field
		@param **kwargs: use kwargs to alter the fuzziness parameters and size
		"""
		return serialiser.dumps({
			'size': 0, 
			'suggest':{
				'text': q,	
				suggest : {
					'completion': {	
						'field': field,
						'size': kwargs.get('size', 5),
						'fuzzy' : {
							'prefix_length': kwargs.get('prefix_length', 1),
							'min_length': kwargs.get('min_length', 3),
							'fuzziness': kwargs.get('fuzziness', 'AUTO')
						}
					}
				}
			}
		})
	def _auto_complete_users(self, q):
		"""
		Wrapper around _auto_complete_query passing the parameters for a users 
		autocomplete.
		@param q: query string
		"""
		return self._auto_complete_query(
			q, 'user_suggestions', USER_SUGGEST_0, fuzziness=1
		)
	def _auto_complete_posts(self, q):
		"""
		Wrapper around _auto_complete_query passing the parameters for a posts 
		autocomplete.
		@param q: query string
		"""
		return self._auto_complete_query(q, 'post_suggestions', POST_SUGGEST_0)

	def _get_suggestions(self, response):
		"""
		"""
		obj = {}
		suggestions = []
		for r in response:
			for s in r['suggest']:
				for options in r['suggest'][s]:
				# 	for o in options['options']:
					suggestions.append(options)

				obj[s] = suggestions
				suggestions = []


		return obj

	# Functions
	def auto_complete_users(self, q):
		"""
		Return a list of suggestions for users only based on the value of 'q'
		@param q: search query.
		"""
		data = self._auto_complete_users(q).encode('utf-8')
		url = self.get_url(USER_SEARCH_ALIAS, '_search')
		return requests.get(url, data=data).json()

	def auto_complete_posts(self, q):
		"""
		Return a list of suggestions for posts only based on the value of 'q'
		@param q: search query.
		"""
		data = self._auto_complete_posts(q).encode('utf-8')
		url = self.get_url(POST_SEARCH_ALIAS, '_search')
		return requests.get(url, data=data).json()

	def auto_complete_all(self, q):
		"""
		Return a list of suggestions for posts and users based on the value of 'q'
		@param q: search query.
		"""
		data =  '{ "index": "%s" }\n' %(POST_SEARCH_ALIAS)
		data += self._auto_complete_posts(q) + '\n'
		data += '{ "index": "%s" }\n' %(USER_SEARCH_ALIAS)
		data += self._auto_complete_users(q) + '\n'

		try:
			url = self.get_url('_msearch')
			response = requests.get(url, data=data.encode('utf-8')).json()['responses']
			return self._get_suggestions(response)
		except KeyError:
			return []

	# MAPPING
	# ------------------------------------------------------------------------------------
	def mapping(self, index_type, mappings, indices=None):
		"""
		Generate an index mapping and return the mapping or apply it to the indices
		directly. Raise an exception if the mapping was not applied.
		@param index_type: index type (e.g. domain.com/index/index_type/...).
		@param mappings: field mappings.
		@param indices: index names (optional).
		"""
		data = { 
			index_type: {
				'properties': mappings
			}
		}

		# if an index was give apply the mapping directly else return the mapping
		if indices:
			url = self.get_url(indices, index_type, '_mapping')
			response = requests.put(url, data=serialiser.dumps(data))
			return self.process_response(response, uid='mapping')
		else:
			return {'mappings': data}
	# ------------------------------------------------------------------------------------
	def document_mapping(self, value, suggest, **kwargs):
		"""
		Create and return the mapping for a document.
		@param value: input value for the document (e.g. ['The', 'Great', 'Gatsby'])
		@param suggest: name of the suggest object (should match name in index)
		@param kwargs: use kwargs to pass additional arguments that will be added to
		the 'suggest' object.
		"""
		if suggest is None: raise ValueError('Parameter "suggest" cannot be None')

		data = {
			suggest : {
				'input': value,
			}
		}
		# available parameters are: output', 'payload', 'weight'
		for param in kwargs:
			try:
				data[suggest][param] = kwargs[param]
			except KeyError:
				pass

		return data
	# ------------------------------------------------------------------------------------
	def update_mapping(self, indices, index_type, mapping):
		"""
		Add a new type/mapping or update an existing one.
		@param index: indices to apply the mapping to.
		@param index_type: index type.
		@param mapping: field mapping e.g. {'name':{'type':'string', 'analyzer':'simple'}}
		"""
		data = serialiser.dumps({
			'properties': mapping
		})
		
		url = self.get_url(indices, '_mapping', index_type)
		response = requests.put(url, data=data)
		return self.process_response(response, uid='update_mapping')

	# DOCUMENT HELPER FUNCTIONS
	# ------------------------------------------------------------------------------------
	def _document_bulk_request(self, url, data, verbose=True):
		"""
		Handle the call and response of a bulk update
		@param url: request url
		@param data: request data
		@param process: name of the process e.g. 'Update'
		"""

		response = requests.post(url, data=data.encode('utf-8')).json()
		try:
			if response['errors'] == True:
				error = '_document_bulk_request: Document Bulk Failed. '
				if verbose:
					error = (error, response)
				raise BulkProcessing(error)
		except KeyError:
			raise BulkProcessing(response)
		return True
	# ------------------------------------------------------------------------------------
	def _single_document_mi(self, indices, uid, **kwargs):
		"""
		**********************************************************************************
		_mi functions work across multiple indices
		**********************************************************************************
		Return a single document or raise an EsException the document could not be found.
		@params indices: indices to search for the document in.
		@param uid: document id.
		"""
		try:
			return self.get_document_mi(
				indices, [uid], fields=['_all'], size=1, **kwargs
			)['hits']['hits'][0]
		except IndexError:
			raise EsException('Document with id %s does not exist' %(uid))
		except KeyError as e:
			raise EsException(e)
	# ------------------------------------------------------------------------------------
	def get_document_mi(self, indices, uids, **kwargs):
		"""
		Return a list of documents found by searching multiple indices.
		@param indices: indices to search
		@param uid: array of document ids
		"""
		data = serialiser.dumps({
			'query':{
				'ids': {
					'values': uids
				}
			},
			'_source': kwargs.get('fields', []),
			'size': kwargs.get('size', 10)
		})

		url = self.get_url(indices, '_search', params=kwargs.get('params', {}))
		return requests.get(url, data=data).json()
	# ------------------------------------------------------------------------------------
	def get_document(self, index, index_type, uid, **kwargs):
		"""
		Return a single document as a json object or None if it cannot be found.
		@param index: index of the document.
		@param index_type: type of the document.
		@param uid: document id.
		@param kwargs: use kwargs to pass url paramters.
		the _source attribute.
		"""
		url = self.get_url(index, index_type, str(uid), params=kwargs.get('params', {}))
		response = requests.get(url).json()
		try:
			if response['found'] == True:
				return response
		except KeyError:
			pass
		return response
	# ------------------------------------------------------------------------------------
	def get_source(self, index, index_type, uid, **kwargs):
		"""
		Return the document _source data only.
		@param index: index of the document.
		@param index_type: type of the document.
		@param uid: document id.
		@param kwargs: use kwargs to pass url paramters.
		the _source attribute.
		"""
		# e.g. params={'_source':'title,tags', 'routing':'abc'}
		params = kwargs.get('params', {})
		url = self.get_url(index, index_type, str(uid), '_source', params=params)
		return requests.get(url).json()
	# ------------------------------------------------------------------------------------
	def document_exists_x(self, **kwargs):
		url = 'http://localhost:9200/users_index_0/user_0/_search/exists?q=username:ta390'
		return requests.get(url).json()

	def document_exists(self, index, index_type, uid, **kwargs):
		"""
		Check that the document exists (return a boolean).
		@param index: index of the document.
		@param index_type: type of the document.
		@param uid: document id.
		"""
		url = self.get_url(index, index_type, str(uid), params=kwargs.get('params', {}))
		return requests.head(url).status_code == 200
	# ------------------------------------------------------------------------------------
	def normalise_tokens(self, tokens, max_tokens=None, unique=True):
		"""
		Return a list with no duplicates in the same order.
		@param token_list: list of tokens.
		@param max_token: the maximum number of tokens return on the list.
		"""
		tmp = []
		for t in tokens:
			t = t.lower()
			if not unique or (t and t not in tmp):
				tmp.append(t)

		return tmp[:max_tokens]
	# ------------------------------------------------------------------------------------
	def document_object(self, data, kwargs, attr):
		"""
		Create a sub object from the data using the keys found in kwargs. Return the new
		object as a key,value tuple or None. Primarily used for adding payload data to 
		bulk document_creations. (Helper for bulk create/update methods)
		@param data: object containing the data (e.g. {'name': 'A', 'age': '30'})
		@param kwargs: kwargs object containing the keys and used to store the object.
		@param attr: name of the attribute to create.
		"""
		try:
			keys = kwargs[attr]
			# sub object
			obj = {}
			# create an entry in obj for each key
			for k in keys:
				obj[k] = data[k]
			# return the sub object as a tuple (key, value)
			return (attr, obj)
		except KeyError:
			pass
	# ------------------------------------------------------------------------------------
	def document_value(self, data, kwargs, attr):
		"""
		Take the key stored in kwargs and use it to retrieve the value stored in 'data'.
		Return the key, value as a tuple or None. (Helper for bulk create/update methods)
		@param data: object containing the data (e.g. {'name': 'A', 'age': '30'})
		@param kwargs: kwargs object containing the key and used to store the object.
		@param attr: name of the attribute to create.
		"""
		try:
			key = kwargs[attr]
			# return the sub object as a tuple (key, value)
			return (attr, data[key])
		except KeyError:
			pass
	# ------------------------------------------------------------------------------------
	def get_document_mapping_params(self, data, kwargs):
		"""
		Return a dictionary containing all the optional document mapping parameters if
		they have been given.
		@param data: document data e.g. {'name': 'A'}
		@param kwargs: dict containing the optional param keys (payload, weight, output)
		"""
		params = [
			# get payload data if available.
			self.document_object(data, kwargs, attr='payload'),
			# get weight data if available.
			self.document_value(data, kwargs, attr='weight'),
			# get output data if available.
			self.document_value(data, kwargs, attr='output')
		]

		return dict(p for p in params if p is not None)


	# DOCUMENT FUNCTIONS
	# ------------------------------------------------------------------------------------
	def create_document(self, doc, index, index_type, uid, **kwargs):
		"""
		Create a new document and return True if successful or raise an EsException if 
		not. The Elasticsearch API will create a new index and type if either does not 
		already exist.
		@param doc: the document to create (each doc must contain an id indexed by 'pk').
		@param index: index of the document.
		@param index_type: type of the document.
		@param uid: document id.
		"""
		# optional function that returns document mapping data
		callback = kwargs.get('callback')
		if callback:
			for args in callback(self, doc):
				tokens, suggest_name, params = args
				doc.update( self.document_mapping(tokens, suggest_name, **params) )

		url = self.get_url(index, index_type, str(uid), params=kwargs.get('params',{}))
		doc = serialiser.dumps(doc).encode('utf-8')

		response = requests.put(url, data=doc)
		return self.process_response(response)
	# ------------------------------------------------------------------------------------
	def create_document_bulk(self, docs, index, index_type, **kwargs):
		"""
		Create multiple documents at once and return True if successful or raise a 
		BulkProcessing exception if not.
		@param docs: a list of documents (each doc must contain an id indexed by 'pk').
		@param index: index name.
		@param index_type: index type to store the document in.
		"""
		# optional function that returns document mapping data
		callback = kwargs.get('callback')
		data = ''
		for doc in docs:
			# remove the the id from 'docs' and set it on the document
			data += serialiser.dumps({'create': self._build_header(doc)})+'\n'
			if callback:
				for args in callback(self, doc):
					tokens, suggest_name, params = args
					doc.update( self.document_mapping(tokens, suggest_name, **params) )

			data += serialiser.dumps(doc)+'\n'

		url = self.get_url(index, index_type, '_bulk', params=kwargs.get('params',{}))
		return self._document_bulk_request(url, data)
	# ------------------------------------------------------------------------------------
	def update_document(self, doc, index, index_type, uid, **kwargs):
		"""
		Update an existing document and return True if successful or raise an EsException 
		if not.
		@param doc: a partial document to update the existing document with.
		@param index: index name.
		@param index_type: index type.
		@param uid: document id.
		"""
		url_params = kwargs.get('params', {})
		url = self.get_url(index, index_type, str(uid), '_update', params=url_params)
		response = requests.post(url, data=serialiser.dumps({
				'doc': doc,
				# reindex only if the data has changed
				'detect_noop': 'true',
				# if the doc doesn't exist, should it be created
				'doc_as_upsert' : kwargs.get('upsert', False),
				# the number of times to try an update before throwing an exception
				'retry_on_conflict' : '3',
			}).encode('utf-8')
		)
		return self.process_response(response, uid='update_document')
	# ------------------------------------------------------------------------------------
	def update_document_mi(self, doc, indices, uid, **kwargs):
		"""
		**********************************************************************************
		_mi functions work across multiple indices
		**********************************************************************************
		Update an existing document and return True if successful or raise an EsException 
		if not.
		@param doc: a single partial document to update the existing document with.
		@param indices: indices where the document may exist. e.g. 'index1,index2'
		@param uid: document id.
		@param upsert: should a document be created if it doesn't exists. 
		"""
		info = self._single_document_mi(indices, uid)
		return self.update_document(doc, info['_index'], info['_type'], uid, **kwargs)

	# ------------------------------------------------------------------------------------
	def update_document_bulk(self, dicts, index, index_type, **kwargs):
		"""
		Update multiple documents at once and return True if successful or raise a 
		BulkProcessing exception if not.
		@param dicts: a list of dictionaries each containing a partial document. Each 
		document must contain its own id. e.g [{'_id': '1', name': 'B'}, ... ].
		@param index: index name.
		@param index_type: index type.
		"""
		data = ''
		for item in dicts:
			# remove the the id from 'item' and set it on the document
			data += serialiser.dumps({'update': self._build_header(item)})+'\n'
			data += serialiser.dumps({'doc': item})+'\n'

		url = self.get_url(index, index_type, '_bulk', params=kwargs.get('params',{}))
		return self._document_bulk_request(url, data)
	# ------------------------------------------------------------------------------------
	def _update_processing(self, hit, **kwargs):
		"""
		Custom data processing for process_scroll.
		Create each new document on the same index.
		"""
		try:
			uid = None
			uid = hit['_id']
			doc = kwargs['docs'][uid]
			header = self._build_header(hit)
			return self._data_processing('update', header, doc)

		except KeyError:
			raise BulkProcessing('_update_processing: A document with the uid %(uid)s '
				'does not exist OR multiple documents were found with the same uid - '
				'switch "pop" to "get" to force multiple updates for documents with the '
				'same uid.' 
				%{'uid': uid}
			) 
	def update_document_bulk_mi(self, indices, dicts, **kwargs):
		"""
		**********************************************************************************
		_mi functions work across multiple indices
		**********************************************************************************
		Update multiple documents at once and return True if successful or raise a 
		BulkProcessing exception if not.
		@param indices: indices where the documents may exists e.g. 'index1,index2'
		@param dicts: a list of dictionaries each containing a partial document. Each 
		document must contain its own id. e.g [{'id': '1', name': 'B'}, ... ].
		"""
		# size of each scroll batch.
		size = kwargs.get('size', 1000)
		# timeout for the scroll request.
		time = kwargs.get('time', '1m')
		url_params = kwargs.get('params', {})
		url_params.update({'scroll':time})
		# key for the id field.
		pk = kwargs.get('pk', 'id')
	
		docs = {}
		keys = []
	
		# restucture the list of documents and extract all ids
		dicts = list(dicts)
		while dicts:
			doc = dicts.pop()
			key = str(doc.pop(pk))
			docs[key] = doc
			keys.append(key)

		response = self.get_document_mi(
			indices, keys, fields=['_all'], size=size, params=url_params
		)

		if response['hits']['hits'] == []:
			raise BulkProcessing('update_document_bulk_mi: No Documents Found')

		return self.process_scroll(
			response, time, callback=self._update_processing, docs=docs
		)
	# ------------------------------------------------------------------------------------
	def delete_document(self, index, index_type, uid, **kwargs):
		"""
		Delete a single document and return True if successful or raise an EsException if 
		not.
		@param index: index of the document.
		@param index_type: type of the document.
		@param uid: document id.
		"""
		url = self.get_url(index, index_type, str(uid), params=kwargs.get('params',{}))
		if requests.delete(url).status_code != 200:
			raise EsException('Document with id %s does not exists' %(uid))
	# ------------------------------------------------------------------------------------		
	def delete_document_mi(self, indices, uid, **kwargs):
		"""
		**********************************************************************************
		_mi functions work across multiple indices
		**********************************************************************************
		Delete a single document and return True if successful or raise an EsException if 
		not.
		@param indices: indices where the document may exists
		@param uid: document id.
		"""
		info = self._single_document_mi(indices, uid, params=kwargs.get('params',{}))
		return self.delete_document(info['_index'], info['_type'], uid)

	# ------------------------------------------------------------------------------------
	def delete_document_bulk(self, dicts, index, index_type, **kwargs):
		"""
		Delete multiple documents at once and return True if successful or raise a 
		BulkProcessing exception if not.
		@param dicts: a list of dictionaries each containing the id of a document. 
		e.g [{'id': '1'}, {'id': '2'}, {'id': '3'}]
		@param index: index name.
		@param index_type: index type.
		@param pk: key for the id field.
		"""
		data = ''
		for item in dicts:
			data += serialiser.dumps({'delete': self._build_header(item)})+'\n'

		url = self.get_url(index, index_type, '_bulk', params=kwargs.get('params',{}))
		return self._document_bulk_request(url, data)
	# ------------------------------------------------------------------------------------	
	def _delete_processing(self, hit, **kwargs):
		"""
		Custom data processing for process_scroll.
		Return the header only
		"""
		header = self._build_header(hit)
		return serialiser.dumps({'delete': header}) + '\n'

	def delete_document_bulk_mi(self, indices, uids, **kwargs):
		"""
		**********************************************************************************
		_mi functions work across multiple indices
		**********************************************************************************
		Delete multiple documents at once and return True if successful or raise a 
		BulkProcessing exception if not.
		@param indices: indices where the document may exists e.g. 'index1,index2'
		@param uids: a list of document ids (uids can also be a list of dictionaries). 
		e.g. [1,23,4] or [{'id': '1'}, {'id': '23'}, {'id': '3'}]
		"""

		# size of each scroll batch.
		size = kwargs.get('size', 1000)
		# timeout for the scroll request.
		time = kwargs.get('time', '1m')
		url_params = kwargs.get('params', {})
		url_params.update({'scroll':time})
		# key for the id field.
		pk = kwargs.get('pk', 'id')

		
		if isinstance(uids[0], dict):
			uids = [uid[pk] for uid in uids]

		response = self.get_document_mi(
			indices, uids, fields=['_all'], size=size, params=url_params
		)

		return self.process_scroll(response, time, callback=self._delete_processing)

	# FIELD FUNCTIONS
	# ------------------------------------------------------------------------------------
	def remove_field(self, field, index, index_type, uid, **kwargs):
		"""
		Remove a field from a document and return True if successful and False if not.
		@param field: name of the field to remove.
		@param index: index name.
		@param index_type: index type.
		@param uid: document id.
		"""
		data = serialiser.dumps({ 
			'script': {
				'file': 'remove_field',
				'lang': 'groovy',
				'params': {
					'field_name': field
				}
			}
		})

		url_params = kwargs.get('params', {})
		url = self.get_url(index, index_type, str(uid), '_update', params=url_params)
		return requests.post(url, data=data).status_code in (200, 201)
	# ------------------------------------------------------------------------------------
	def _remove_field_processing(self, hit, **kwargs):
		"""
		Custom data processing for process_scroll.
		Remove a field from each document.
		"""
		field = kwargs['field']
		header = self._build_header(hit)
		
		data = serialiser.dumps({ 'update': header }) + '\n'
		data += serialiser.dumps({ 
			'script': {
				'file': 'remove_field',
				'lang': 'groovy',
				'params': {
					'field_name': field
				}
			}
		}) + '\n'
		return data
	# ------------------------------------------------------------------------------------
	def remove_field_from_all(self, field, indices, index_type=None, **kwargs):
		"""
		Remove 'field' from all documents found in the 'indices'.
		NOTE: Removing the mapping is only possible via reindexing.
		@param field: name of the field applied to the index mapping.
		@params indices: indices to remove 'field' from all their docs.
		@param index_type: index type (optional).
		@param kwargs: use kwargs to set the 'size' and 'time' of scroll.
		"""

		# retrieve all documents for the given indices and type
		size = kwargs.get('size', 1000)
		data = serialiser.dumps({ 
			'query': { 'match_all': {} }, 
			'_source': ['_all'],
			'size': size,
		})
		time = kwargs.get('time', '1m')

		url_args = [a for a in (indices, index_type) if a is not None]
		url = self.get_url(*url_args, '_search', params={'scroll':time})
		response = requests.get(url, data=data).json()

		return self.process_scroll(
			response, time, callback=self._remove_field_processing, field=field
		)
	# ------------------------------------------------------------------------------------
	def add_field(self, field, value, index, index_type, uid, **kwargs):
		"""
		Add a field to a document and return True if successful and False if not.
		@param field: name of the new field.
		@param value: value of the new field.
		@param index: index of the document.
		@param index_type: type of the document.
		@param uid: document id.
		"""
		data = serialiser.dumps({ 
			'script': {
				'file': 'add_field',
				'lang': 'groovy',
				'params': {
					'field_name': field,
					'value': value
				}
			}
		}).encode('utf-8')

		url_params = kwargs.get('params', {})
		url = self.get_url(index, index_type, str(uid), '_update', params=url_params)
		response = requests.post(url, data=data)
		return self.process_response(response)
	# ------------------------------------------------------------------------------------
	def _add_field_processing(self, hit, **kwargs):
		"""
		Custom data processing for process_scroll.
		Add a new field to each document.
		"""
		try:
			uid = hit['_id']
			# use ['values'].pop to ensure unique ids were use or get/[uid] if not required.
			value = kwargs['values'][uid]
			field = list(value.keys())[0]
			print(field)
			
			data =  serialiser.dumps({ 'update': self._build_header(hit) }) + '\n'
			data += serialiser.dumps({ 
				'script': {
					'file': 'add_field',
					'lang': 'groovy',
					'params': {
						'field_name': field,
						'value': value[field]
					}
				}
			}) + '\n'
			return data
		except KeyError:
			raise BulkProcessing(
				'Document with id %(uid)s has not been given a values' %{'uid': uid}
			)

	def add_field_to_all(self, field, values, indices, index_type, **kwargs):
		"""
		Add a single field mapping to all indicies and add a new field to all their
		documents.
		@param field: name of the field applied to the index mapping.
		@param values: a list of dictionaries containing the document id and field. The 
		name and value will be given to applied to the document.
		e.g. [{'id': 1, 'field_name': 'field_value'}, ...]
		@params indices: indices to apply the field mapping to and to update all docs for.
		@param index_type: index type required for the mapping.
		"""

		mapping = kwargs.get('mapping')
		size = kwargs.get('size', 1000)
		time = kwargs.get('time', '1m')
		url_params = kwargs.get('params', {})
		url_params.update({'scroll':time})
		pk = kwargs.get('pk', 'id')

		if mapping:
			# e.g. {'type': 'boolean'} - raises EsException if fails.
			self.update_mapping(indices, index_type, { field: mapping })

		# retrieve all documents for the given indices and type
		data = serialiser.dumps({ 
			'query': { 'match_all': {} }, 
			'_source': ['_all'],
			'size': size,
		})
		
		url = self.get_url(indices, index_type, '_search', params=url_params)
		response = requests.get(url, data=data).json()

		# restucture the list of values to be a dictionary indexed by their id.
		# i.e. { 'id_value': {'field_name': 'field_value'}, ...}
		dicts = {}
		values = list(values)
		while values:
			val = values.pop()
			dicts[str(val.pop(pk))] = val

		return self.process_scroll(
			response, time, callback=self._add_field_processing, values=dicts
		)

	# INDEX FUNCTIONS
	# ------------------------------------------------------------------------------------
	def create_index(self, index, aliases=None, analysers=None, filters=None, mappings=None, **kwargs):
		"""
		Create a new index.
		@param index: index name.
		@param aliases: index aliases (optional).
		@param mappings: index mappings (optional).
		@param analysers: index analysers (optional).
		@param filters: index filters (optional).
		@param kwargs: use kwargs to set shards/replicas 
		"""
		data = {
			'settings': {
				'index': {
					'number_of_shards': kwargs.get('shards', 5),
					'number_of_replicas':  kwargs.get('replicas', 1),

					'analysis': {
						'filter': {
							'unique_filter': {
								'type': 'unique',
								'only_on_same_position': True
							},
							'english_filter': {
								'type': 'stemmer',
								'name': 'english'
							},
							'token_limit16_filter': {
								'type': 'limit',
								'max_token_count': 16,
							},
							'token_limit32_filter': {
								'type': 'limit',
								'max_token_count': 32,
							},
							'word_delimiter_filter' : {
								'type': 'word_delimiter',
								'preserve_original': True,
								'stem_english_possessive': False,
							},
							'asciifolding_filter': {
								'type': 'asciifolding',
								'preserve_original': True
							},
							'shingle_filter': {
								'type': 'shingle',
								'output_unigrams': False
							},
							'nGram_filter': {
								'type': 'nGram',
								'min_gram': 1,
								'max_gram': 5,
								'token_chars': [
									'letter',
									'digit',
									'punctuation',
									'symbol'
								]
							},
							'edge_nGram_filter': {
								'type': 'edgeNGram',
								'min_gram': 1,
								'max_gram': 2,
							}
						},
					}
				}
			},
			'timeout': '30s'
		}

		if aliases:
			data['aliases'] = aliases
		if analysers:
			data['settings']['index']['analysis']['analyzer'] = analysers
		if filters:
			data['settings']['index']['analysis']['filters'].update( filters )
		if mappings:
			data['mappings'] = mappings


		url = self.get_url(index, params=kwargs.get('params', {}))
		response = requests.put(url, data=serialiser.dumps(data))
		return self.process_response(response, uid='create_index')

	# ALIASES AND REINDEXING
	# ------------------------------------------------------------------------------------
	def index_alias_mappings(self, search_alias, indices_alias, index_alias, index):
		"""
		Map a new index to the aliases used for search and indexing. For each subsequent
		index created for a particular model, remove the old index so all new documents
		will be created on the new index only.
		@param search_alias: alias name given to all indices used by search functions.
		@param indices_alias: alias name given to all indices for this particular model.
		@param index_alias: alias name given to the newest/current index for a particular model.
		@param index: new index to map alias names to.
		"""
		data = {
			'actions': [
				# add the new index to the search alias making all its docs searchable.
				{ 'add': { 'index': index, 'alias': search_alias }},
				# add the new index to the indices alias to have an alias for all indices.
				{ 'add': { 'index': index, 'alias': indices_alias }},
				# remove the current indices associated with the current index alias.
				{ 'remove': { 'index': '_all', 'alias': index_alias }},
				# make the new index the only index for the current index alias.
				{ 'add': { 'index': index, 'alias': index_alias }}
			]
		}

		try:
			url = self.get_url('_aliases')
			return requests.post(url, data=serialiser.dumps(data)).json()['acknowledged']
		except KeyError:
			raise EsException(response)

	def create_alias(self, index, alias):
		"""
		Create an alias for an existing index.
		@param index: index name.
		@param alias: alias name.
		"""
		url = self.get_url(index, '_alias', alias)
		return requests.put(url).json()

	def replace_alias(self, alias, index_remove, index_add):
		"""
		Use the aliases API to replace the index for a given alias.
		@param alias: alias name.
		@param index_remove: index name to remove alias name from.
		@param index_add: index name to add alias name to.
		"""
		data = serialiser.dumps({
			'actions': [
				{ 'remove': { 'index': index_remove, 'alias': alias }},
				{ 'add':    { 'index': index_add, 'alias': alias }}
			]
		})

		url = self.get_url('_aliases')
		response = requests.post(url, data=data)
		return self.process_response(response, uid='replace_alias')

	def delete_index_alias(self, index, alias):
		"""
		Delete index alias and return True if successful and False if not.
		@param index: index name.
		@param alias: alias name.
		"""
		url = self.get_url(index, '_alias', alias)
		return requests.delete(url).status_code == 200

	def get_alias_indices(self, alias):
		"""
		Return the indices an alias refers to.
		@param alias: alias name.
		"""
		url = self.get_url('*', '_alias', alias)
		return requests.get(url).json()

	def get_index_aliases(self, index):
		"""
		Return the aliases of an index.
		@param index: index name.
		"""
		url = self.get_url(index, '_alias', '*')
		return requests.get(url).json()
	# ------------------------------------------------------------------------------------
	def _reindex_processing(self, hit, **kwargs):
		"""
		Custom data processing for process_scroll.
		Create each new document on the same index.
		"""
		# force index to be the same for all.
		hit['_index'] = kwargs['index']
		header = self._build_header(hit)
		return self._data_processing('create', header, hit['_source'])

	def reindex(self, old_index, new_index, query=None, **kwargs):
		"""
		Reindex the documents from the old index into the new index in batches specified 
		by batch_range. The function will raise an EsException if it fails.
		@param old_index: name of the index to get all docs from.
		@param new_index: name of the index to transfer all docs to.
		"""

		size = kwargs.get('size', 1000)
		time = kwargs.get('time', '1m')
		url_params = kwargs.get('params', {})
		url_params.update({'scroll':time})

		data = serialiser.dumps({
			'query': query or { 'match_all': {} },
			# most efficient way to sort
			'sort': ['_doc'],
			'size':  size
		})

		# get scroll initial response
		url = self.get_url(old_index, '_search', params=url_params)
		response = requests.get(url, data=data).json()
		
		# create new documents in the new index
		return self.process_scroll(
			response, time, callback=self._reindex_processing, index=new_index
		)
	# ------------------------------------------------------------------------------------
	def update_index_dynamic(self, index, data):
		"""
		Update the settings of an existing index.
		See list of settings that can be updated while an index is open:
		https://www.elastic.co/guide/en/elasticsearch/reference/current/index-modules.html#dynamic-index-settings
		@param index: index name.
		@param data: e.g. 
			'index': {
				'number_of_replicas': 5,
			}
		"""
		url = self.get_url(index, '_settings')
		response = requests.put(url, data=serialiser.dumps(data))
		return response.status_code in (200, 201)
	# ------------------------------------------------------------------------------------
	def update_index_static(self, index, data):
		"""
		Update the settings of an existing index.
		See list of settings that can only be updated while an index is closed:
		https://www.elastic.co/guide/en/elasticsearch/reference/current/index-modules.html#_static_index_settings
		@param index: index name.
		@param data: e.g. 
			'analysis' : {
				'analyzer':{
					'new_analyzer':{
						'type':'custom',
						'tokenizer':'whitespace'
					}
				}
			}
		"""
		self.close_index(index)
		url = self.get_url(index, '_settings')
		response = requests.put(url, data=serialiser.dumps(data))
		self.open_index(index)
		return response.status_code in (200, 201)
	# ------------------------------------------------------------------------------------
	def delete_index(self, index):
		"""
		Delete an index and return True if the index was deleted and False if not.
		@param index: index name.
		"""
		url = self.get_url(index)
		return requests.delete(url).status_code == 200
	# ------------------------------------------------------------------------------------
	def list_all_indices(self):
		"""
		Return a list of all the indices.
		"""
		url = self.get_url('_cat', 'indices?v')
		return requests.get(url).json()
	# ------------------------------------------------------------------------------------
	def open_index(self, index):
		"""
		Open an index and return the request response.
		See: https://www.elastic.co/guide/en/elasticsearch/reference/2.2/
			indices-open-close.html#indices-open-close
		@param index: index name.
		"""
		url = self.get_url(index, '_open')
		return requests.post(url).json()
	# ------------------------------------------------------------------------------------
	def close_index(self, index):
		"""
		Close an index and return the request response.
		WARNING: Closed indices consume a significant amount of disk-space.
		This can be disabled via the cluster settings API by setting 
		cluster.indices.close.enable to false
		See: https://www.elastic.co/guide/en/elasticsearch/reference/2.2/
			indices-open-close.html#indices-open-close
		@param index: index name.
		"""
		url = self.get_url(index, '_close')
		return requests.post(url).json()

	def test_analyser(self, index, analyser, text):
		"""
		Run the analayser with sample text and return its output.
		@param index: index name.
		@param analyser: analyser name.
		@param text: text to run through analyser.
		"""
		url = self.get_url(index,'_analyze?analyzer='+analyser)
		return requests.post(url, data=text).json()

	def validate_query(self, q, fields, query='best_fields', op='or'):
		"""
		Run a query and return the explanation.
		@param q: query string.
		@param fields: array of field names to run the query on.
		@param query: the type of query to run.
		@param op: the operator ('or', 'and').
		"""
		data = serialiser.dumps({
			'query': {
				'multi_match': {
					'query': q,
					'type': query,
					'operator': op,
					'fields': fields,
				}
			}
		})

		url = self.get_url('_validate', 'query?explain')
		return requests.get(url, data=data).json()

	def get_mapping_data(self, index):
		"""
		Return the mapping data for a given index.
		@param index: index name.
		"""
		url = self.get_url(index, '_mapping', params={'pretty': 'true'})
		return requests.get(url).json()

	def debug_tokens(self, indices, field, query=None, size=10):
		"""
		Return all the tokens generated for a field by its index analyzer.
		@param indices: one or more indices (field must exist on all indices).
		@param field: field name.
		@param query: search query.
		@param size: max number of results to return.
		"""
		data = {
			'script_fields': {
				'terms': {
					'script': {
						'file': 'debug_tokens',
						'lang': 'groovy',
						'params': {
							'field_name': field,
						}
					}
				}
			},
			'size': size,
		}
		data.update({ 'query': query or {'match_all' : {}} })


		url = self.get_url(indices, '_search', params={'pretty': 'true'})
		return requests.post(url, data=serialiser.dumps(data)).json()

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------

# ****************************************************************************************
es = UloElasticsearch()
# ****************************************************************************************

# ----------------------------------------------------------------------------------------
# ----------------------------------------------------------------------------------------

