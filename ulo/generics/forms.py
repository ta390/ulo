# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals
from PIL import Image

# core django imports
from django import forms
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports
from .constants import (
	DATE_DAY, DATE_WEEK, DATE_MONTH, DATE_YEAR,
	TYPE_USERS, TYPE_POSTS, TYPE_IMAGE, TYPE_VIDEO,
	SORT_RELEVANCE, SORT_DATE, SORT_VIEWS, SORT_FOLLOWERS
)
from ulo.widgets import UloUlWidget
from ulo.forms import UloForm


# USER UPLOAD FORMS
# ----------------------------------------------------------------------------------------

class SearchFilters(UloForm):

	date_filter = forms.CharField(
		widget=UloUlWidget(
			heading='Date',
			choices=['Today', '1 week', '1 month', '1 year'],
			li_attrs = {
				'data-value': (DATE_DAY, DATE_WEEK, DATE_MONTH, DATE_YEAR),
			}
		),
	)

	type_filter = forms.CharField(
		widget=UloUlWidget(
			heading='Type',
			choices=['Account', 'Post', 'Image', 'Video'],
			li_attrs = {
				'data-value': (TYPE_USERS, TYPE_POSTS, TYPE_IMAGE, TYPE_VIDEO),
				'data-type': ('account', )
			}
		),
	)

	sort_filter = forms.CharField(
		widget=UloUlWidget(
			heading='Sort',
			choices=['Date', 'Views'],
			li_attrs = {
				'data-value': (SORT_RELEVANCE, SORT_DATE, SORT_VIEWS)
			}
		),
	)


# ----------------------------------------------------------------------------------------

