# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports

# thrid party app imports

# project imports


# ELASTICSEARCH CONSTANTS
# ----------------------------------------------------------------------------------------

# DB/DOCUMENT FIELDS USED BY ES SEARCH FUNCTIONS
POST_TYPE_FIELD = 'media_type'
POST_DATE_FIELD = 'published'
POST_VIEWS_FIELD = 'views'
USER_FOLLOWERS_FIELD = 'followers'


# INDEX INFO
# user index aliases
USER_INDICES_ALIAS = 'users_alias'
USER_INDEX_ALIAS = 'users_index_alias'
USER_SEARCH_ALIAS = 'users_search_alias'
# user v0 index, type and suggestion
USER_INDEX_0 = 'users_index_0'
USER_TYPE_0  = 'user_0'
USER_SUGGEST_0 = 'users_suggest_0'

# connection index aliases
CONNECTION_INDICES_ALIAS = 'connections_alias'
CONNECTION_INDEX_ALIAS = 'connections_index_alias'
CONNECTION_SEARCH_ALIAS = 'connections_search_alias'
# user v0 index, type and suggestion
CONNECTION_INDEX_0 = 'connections_index_0'
CONNECTION_TYPE_0  = 'connection_0'
CONNECTION_SUGGEST_0 = 'connections_suggest_0'

# post index aliases
POST_INDICES_ALIAS = 'posts_alias'
POST_INDEX_ALIAS = 'posts_index_alias'
POST_SEARCH_ALIAS = 'posts_search_alias'
# post v0 index, type and suggestion
POST_INDEX_0 = 'posts_index_0'
POST_TYPE_0  = 'post_0'
POST_SUGGEST_0 = 'posts_suggest_0'


# SEARCH FILTERS
# date
DATE_DAY = '0'
DATE_WEEK = '1'
DATE_MONTH = '2'
DATE_YEAR = '3'
# category/type
TYPE_USERS = '0'
TYPE_POSTS = '1'
TYPE_IMAGE = '2'
TYPE_VIDEO = '3'
# sort
SORT_RELEVANCE = '0'
SORT_DATE = '1'
SORT_VIEWS = '2'
SORT_FOLLOWERS = '3'
# index specific sort fields
POST_ONLY_SORT = (SORT_DATE, SORT_VIEWS)
USER_ONLY_SORT = (SORT_FOLLOWERS,)

