"""
Accounts URL Configuration
The `urlpatterns` list routes URLs to views.
"""
# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.conf.urls import include, url

# thrid party app imports

# project imports
from . import views

# ----------------------------------------------------------------------------------------


urlpatterns = [

	# Search autocomplete
	url(r'^autocomplete/(?P<q>.+)/$', 
		views.AutocompleteView.as_view(), name='autocomplete'),

	
]

# ----------------------------------------------------------------------------------------