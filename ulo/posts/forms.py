# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals
from PIL import Image

# core django imports
from django import forms
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports
from .models import Post, PostReport
from ulo.forms import UloModelForm, UloSecureModelForm
from ulo.handlers import get_error

from posts.search import create_post, update_post
from posts.utils import POST_REPORTS

# USER UPLOAD FORMS
# ----------------------------------------------------------------------------------------

class PostForm(UloModelForm):

	tags = forms.CharField(required=False, widget=forms.Textarea())
	description = forms.CharField(required=False, widget=forms.Textarea())

	class Meta:
		model = Post
		fields = ('title', 'vote0', 'vote1', 'category', 'tags', 'description', 
			'comment_settings', 'upload0')
		

	def __init__(self, *args, **kwargs):
		"""
		"""
		self.request = kwargs.pop('request', None)
		super(PostForm, self).__init__(*args, **kwargs)
		self.unknown_error = ('We could not create your post at this moment in time. '
			'Please try again later, we are looking into the problem.')

		# Add field specific attributes
		self.fields['title'].widget.attrs.update({
			'autofocus': 'autofocus',
		})
		# Add capture functionality to image uploads
		self.fields['upload0'].widget.attrs.update({
			'accept': 'image/gif,image/jpeg,image/jpg,image/png,video/*',
			'capture': 'capture',
			'multiple': 'multiple'
		})

	def is_valid(self):
		if not self.files:
			error = get_error(self.request) or _('Please upload photos or a video.')
			self.add_error('upload0', error)

		return super(PostForm, self).is_valid()

	def save(self, commit=True):
		# assign the user to the post instance being created
		self.instance.user = self.request.user
		post = super(PostForm, self).save(commit=False)
		if commit:
			# will return None if the post or elasticsearch doc was not created.
			post = create_post(post, self.request.user)
		return post

# ----------------------------------------------------------------------------------------

class PostEditForm(UloSecureModelForm):

	class Meta:
		model = Post
		fields = ('title', 'category', 'tags', 'description')

	def __init__(self, *arg, **kwargs):
		super(PostEditForm, self).__init__(*arg, **kwargs)
		# UloSecureModelForm requires self.instance to be a user object or self.user to be 
		# set to check the user's password.
		self.user = self.instance.user

	@cached_property
	def changed_data(self):
		"""
		Generate a list of changed field. This is a case insensitive, simplified version 
		of Django's own changed_data. 
		https://docs.djangoproject.com/en/1.9/_modules/django/forms/forms/#Form
		"""
		changed = []
		for name, field in self.fields.items():

			try:
				data = self.cleaned_data.get(name, '')
				data = data.lower()
			except AttributeError:
				pass

			if field.has_changed(self.initial.get(name, field.initial), data):
				changed.append(name)
				
		return changed

	def save(self, commit=True):
		"""
		Save changes and send verification email to the user if they changed their 
		emails address. NOTE: UloSecureModelForm stored the instance variable as
		instance_copy. See ulo.formmixins.UloBaseModelFormMixin for full explanation.
		"""

		post = super(PostEditForm, self).save(commit=False)
		
		if commit:
			# update elasticsearch and save post to db
			post = update_post(post, self.changed_data)

		return post

# ----------------------------------------------------------------------------------------

class PostReportForm(UloModelForm):

	post_id = forms.CharField(
		widget=forms.HiddenInput(),
		error_messages={
			'required': _('We could not determine which post you are looking to report.')
		}
	)

	report = forms.ChoiceField(
		widget=forms.RadioSelect(),
		initial=0, 
		choices=POST_REPORTS,
		help_text=_('What is wrong with this post?'),
	)

	class Meta:
		model = PostReport
		fields = ('report', 'information')

	def __init__(self, *args, **kwargs):
		self.user_id = kwargs.pop('user_id', None)
		super(PostReportForm, self).__init__(*args, **kwargs)

	def save(self, commit=True):
		# assign the user and post ids to the PostReport instance being created
		self.instance.post_id = self.cleaned_data['post_id']
		self.instance.user_id = self.user_id
		return super(PostReportForm, self).save(commit=commit)

# ----------------------------------------------------------------------------------------

