# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# standard library imports
from __future__ import unicode_literals

# core django imports
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports


# CATEGORY OPTIONS
# ----------------------------------------------------------------------------------------

ANIMALS = 1
ART_AND_DESIGN = 2
COMEDY = 3
EDUCATION = 4
ENTERTAINMENT = 5
FASHION = 6
FOOD = 7
GAMING = 8
MOVIES = 9
MUSIC = 10
NEWS = 11
PHOTOGRAPHY = 12
SPORTS = 13
VLOG = 14

CATEGORIES = (
	(ANIMALS, _('Animals')),
	(ART_AND_DESIGN, _('Art & Design')),
	(COMEDY, _('Comedy')),
	(EDUCATION, _('Education')),
	(ENTERTAINMENT, _('Entertainment')),
	(FASHION, _('Fashion')),
	(FOOD, _('Food')),
	(GAMING, _('Gaming')),
	(MOVIES, _('Movies')),
	(MUSIC, _('Music')),
	(NEWS, _('News')),
	(PHOTOGRAPHY, _('Photography')),
	(SPORTS, _('Sports')),
	(VLOG, _('Vlog')),
)


# COMMENT SETTINGS
# ----------------------------------------------------------------------------------------

ENABLED = 1
ANONYMOUS = 2
DISABLED = 3

COMMENT_SETTINGS = (
	(ENABLED, _('Enabled')),
	(ANONYMOUS, _('Anonymous')),
	(DISABLED, _('Disabled')),
)

# MEDIA TYPE SETTINGS
# ----------------------------------------------------------------------------------------
IMAGE = 1
VIDEO = 2

MEDIA_TYPES = (
	(IMAGE, 'Image'),
	(VIDEO, 'Video')
)

# REPORTS OPTIONS
# ----------------------------------------------------------------------------------------

SPAM = 1
HATE = 2
EXPLICIT = 3
ABUSIVE = 4
VIOLENT = 5
DISTASTEFUL = 6

COMMENT_REPORTS = (
	(SPAM, _('Spam')),
	(HATE, _('Hate speech')),
	(EXPLICIT, _('Sexually explicit')),
	(ABUSIVE, _('Harassment or bullying')),
	(VIOLENT, _('Violent or threatening')),
)

POST_REPORTS = (
	(SPAM, _('Spam')),
	(HATE, _('Hateful or abusive')),
	(EXPLICIT, _('Sexually explicit')),
	(ABUSIVE, _('Harassment or bullying')),
	(VIOLENT, _('Violent or threatening')),
	(DISTASTEFUL, _('Distasteful or repulsive'))
)

# PRIVACY SETTINGS
# ----------------------------------------------------------------------------------------

PUBLIC = 1
PRIVATE = 2
FOLLOWERS_ONLY = 3
FOLLOWING_ONLY = 4
TWO_WAY_FOLLOW = 5

PRIVACY_SETTINGS = (
	(PUBLIC, _('Public')),
	(FOLLOWERS_ONLY, _('They follow me')),
	(FOLLOWING_ONLY, _('I follow them')),
	(TWO_WAY_FOLLOW, _('We follow each other')),
	(PRIVATE, _('Only me')),
)


# VOTING SETTINGS
# ----------------------------------------------------------------------------------------

LIKE_CHAR = 'Like'
DISLIKE_CHAR = 'Dislike'
YES_CHAR = 'Yes'
NO_CHAR = 'No'
# user defined text
TEXT_CHAR = ''

# PostVote db values for the type of vote made by the user
VOTE0_VALUE = 1
VOTE1_VALUE = 2
# number of unique votes that a post can have
NUM_OF_VOTES = 2 # currently 2 (vote0 or vote1)

# The text values are stored in the Post table. The individual int values are no longer 
# used. PostVote stores vote0 as 0 (VOTE0_VALUE) and vote1 as 1 (VOTE1_VALUE) without 
# regard for its its text value. stored in Post. See posts.models.Post for fields vote0 
# and vote1.
VOTE_CHARS = (
	(LIKE_CHAR, _('Like')),
	(DISLIKE_CHAR, _('Dislike')),
	(YES_CHAR, _('Yes')),
	(NO_CHAR, _('No')),
	(TEXT_CHAR, _('Text')),
)

# ----------------------------------------------------------------------------------------
