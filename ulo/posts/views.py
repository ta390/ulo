# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.db.models import Count, Prefetch
from django.http import Http404
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
from django.views.generic import View

# thrid party app imports

# project imports
from .forms import PostForm, PostEditForm, PostReportForm
from .models import Post, PostVote
from .utils import NUM_OF_VOTES, VOTE0_VALUE, VOTE1_VALUE
from comments.forms import CommentPostForm, CommentReportForm
from comments.models import Comment, CommentVote
from ulo.viewmixins import OwnerRequiredMixin
from ulo.handlers import (
	FileSizeAndTypeHandler, MemJpegCompressionHandler, TmpJpegCompressionHandler
)
from ulo.sessions import LinkViewsSession
from ulo.utils import get_referer_path
from ulo.views import UloView, UloFormView, UloUpdateView, UloUploadView



# ----------------------------------------------------------------------------------------

class PostListView(View):
	template_name = 'posts/all.html'
	def get(self, request, *args, **kwargs):
		return render(request, self.template_name, {'posts': Post.objects.all()})

# ----------------------------------------------------------------------------------------

class _PostUploadView(UloFormView):
	"""
	Handle the post request of an image or video. Post requests are first sent to 
	PostFormView (images) or PostVideoFormView (videos). Once the upload handlers have
	been added the request is passed to this view for processing.
	"""

	template_name = 'posts/post_form.html'
	form_class = PostForm

	def get_success_url(self):
		messages.success(self.request, _('Post created!'))
		return reverse('posts:form')

	def get_form_kwargs(self):
		"""
		Django's default implementation with 'instance' added to the form on Post 
		requests.
		https://github.com/django/django/blob/master/django/views/generic/edit.py
		"""
		if self.request.method in ('POST', 'PUT'):
			return {
				'data': self.request.POST,
				'files': self.request.FILES,
				'request': self.request,
			}
		return {}

	def form_valid(self, form):
		if not form.save():
			return redirect(reverse('server_error'))

		return super(_PostUploadView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class PostFormView(UloUploadView):
	"""
	NOTE: Make sure Django processes the files LAST! by making it the last element in the 
	QueryDict. Now if an error is raised because of the file the form data for all other
	fields will remain in the request object.
	"""

	template_name = 'posts/post_form.html'
	view_handler = _PostUploadView
	form_class = PostForm

	def set_handlers(self):
		self.request.upload_handlers = [
			FileSizeAndTypeHandler(request=self.request),
			MemJpegCompressionHandler(request=self.request),
			TmpJpegCompressionHandler(request=self.request)
		]

# ----------------------------------------------------------------------------------------

class PostVideoFormView(UloUploadView):
	"""
	NOTE: Make sure Django processes the files LAST! by making it the last element in the 
	QueryDict. Now if an error is raised because of the file the form data for all other
	fields will remain in the request object.
	"""

	template_name = 'posts/post_form.html'
	view_handler = _PostUploadView
	form_class = PostForm

	def get(self, request, *args, **kwargs):
		raise Http404()

	def set_handlers(self):
		self.request.upload_handlers.insert(0, FileSizeAndTypeHandler(
			request=self.request, type='video', max_bytes=73400320, # 70 MB
		))

# ----------------------------------------------------------------------------------------

class PostEditView(OwnerRequiredMixin, UloUpdateView):
	"""
	"""
	redirect_field_name = 'redirect_to'
	template_name = 'posts/post_edit.html'
	form_class = PostEditForm

	
	def is_owner(self):
		post = self.get_object()
		return post.user.pk == self.request.user.pk

	def get_object(self):
		return self._get_object

	@cached_property
	def _get_object(self):
		pk = self.kwargs.get('pk', None)
		return Post.objects.select_related('user').get(pk=pk)

	def get_context_data(self, **kwargs):
		# get default context information
		context = super(PostEditView, self).get_context_data(**kwargs)
		context.update({ 'pk': self.kwargs.get('pk', '') })
		return context

	def get_success_url(self):
		return reverse('posts:post', args={self.post_pk})

	def form_valid(self, form):
		post = form.save()
		self.post_pk = post.pk

		messages.success(self.request, _('Post Edited!'))
		return super(PostEditView, self).form_valid(form)


# ----------------------------------------------------------------------------------------

class PostView(UloView):
	"""
	Render a post adding user specific information to the context for a logged in user.
	"""
	template_name = 'posts/post.html'

	def get_initial(self):
		return {'http_referer': self.request.path_info}

	def get_votes(self, pk):
		# post vote count (use list() to force evaluation)
		votes = list(
			PostVote.objects.filter(post_id=pk).values('vote').annotate(Count('vote'))
		)
		# initialise v to the number of possible votes +1 as votes start from 1 not zero
		v = [0]*(NUM_OF_VOTES+1)
		for vote in votes:
			# add each vote to its index position. Votes are stored as integer starting
			# from 1 to NUM_OF_VOTES (so index 0 is unused)
			try:
				v[vote['vote']] = vote['vote__count']
			except KeyError:
				pass
		return v

	def get_context_data(self, **kwargs):
		# get default context information
		context = super(PostView, self).get_context_data(**kwargs)
		
		try:
			pk = self.kwargs.get('pk')
			# retrieve post
			post = Post.objects.select_related('user').get(pk=pk)
			# This method requires an additional db query but limits the user fields
			# from django.db.models import Prefetch
			# queryset = get_user_model().objects.only('username')
			# post = Post.objects.prefetch_related(
			# 	Prefetch('user', queryset=queryset, to_attr='user_info')
			# ).get(pk=pk)

			# comments limit
			LIMIT = 10
			# retrieve comments and see if user has 'liked' any of these comments
			comments = Comment.objects.select_related('user').filter(post_id=pk)[:LIMIT] \
				.prefetch_related(
					Prefetch(
						'commentvote_set',
						queryset=CommentVote.objects.filter(user_id=self.request.user.pk),
						to_attr='vote'
					)
				)

			# post votes count
			votes = self.get_votes(pk)

			# info on how the user has voted on this post (if logged in)
			user_vote = PostVote.objects.only('vote').filter(
				post_id=pk, user_id=self.request.user.pk
			)
			
		except Post.DoesNotExist:
			raise Http404()

		# comment forms initial data
		form_kwargs = self.get_initial()
		# template context
		context.update({
			'post': post, # the post
			'comments': comments,
			'votes': votes, # the votes count for this post
			'vote0_value': VOTE0_VALUE, # the db value to give vote0
			'vote1_value': VOTE1_VALUE, # the db value to give vote1
			'user_vote': user_vote, # info on the users vote for this post
			'form': CommentPostForm(initial=form_kwargs), # comment form
			'report_form': CommentReportForm(initial=form_kwargs) # report a comment form
		})

		return context

# ----------------------------------------------------------------------------------------

class PostVoteView(UloView):
	"""
	Add or remove a user's vote from PostVote.
	"""

	def get(self, request, *args, **kwargs):
		raise Http404()

	def post(self, request, *args, **kwargs):
		# redirect user to login page if not already logged in.
		if not request.user.is_authenticated():
			return redirect( get_referer_path(request, redirect=True) )

		try:
			# get the vote value (VOTE0_VALUE or VOTE1_VALUE)
			vote = int(request.POST['vote'])
			# get the post id
			post_id = request.POST['post_id']
			# get the existing entry
			post_vote = PostVote.objects.get(user_id=request.user.pk, post_id=post_id)

			if vote == post_vote.vote:
				# if the user has clicked the vote they previously selected, delete it
				post_vote.delete()
			else:
				# else update the vote field to its new value.
				post_vote.vote = vote
				post_vote.save()

		except PostVote.DoesNotExist:
			# if no entry exists, create one
			PostVote.objects.create(user_id=request.user.pk, post_id=post_id, vote=vote)

		return redirect( get_referer_path(request) )

# ----------------------------------------------------------------------------------------

class PostReportView(LoginRequiredMixin, UloFormView):
	"""
	Render a form that allows the user to report an issue with a post.
	"""
	
	template_name = 'posts/report.html'
	form_class = PostReportForm

	def get_success_url(self):
		LinkViewsSession(self.request).set('post_report', True)
		return reverse('posts:report_complete')

	def get(self, request, pk, *args, **kwargs):
		# check that the pk is valid
		if Post.objects.filter(pk=pk).exists():
			# add the pk as a hidden field to the form
			self.initial = {'post_id': pk}
			return super(PostReportView, self).get(request, *args, **kwargs)
		raise Http404()

	def get_form_kwargs(self):
		kwargs = super(PostReportView, self).get_form_kwargs()
		if self.request.method in ('POST', 'PUT'):
			kwargs.update({ 'user_id': self.request.user.pk })
		return kwargs

	def form_valid(self, form):
		form.save()
		return super(PostReportView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class PostReportCompleteView(LoginRequiredMixin, UloView):
	"""
	Display page informing the user that their report has been received.
	"""
	template_name = 'posts/report_complete.html'

	def get(self, request, *args, **kwargs):
		if LinkViewsSession(request).get('post_report'):
			return super(PostReportCompleteView, self).get(request, *args, **kwargs)
		raise Http404()

# ----------------------------------------------------------------------------------------

class PostActionsView(LoginRequiredMixin, UloView):
	"""
	Javascript Disabled.
	Rendered when a user does not have javascript enabled and tries to perform an action
	on a post (e.g. report an issue or share)
	"""
	
	# render all post actions on one page.
	template_name = 'posts/actions.html'

	# add the post id to the form(s)
	def get_initial(self, **kwargs):
		return {'post_id': kwargs.get('pk', '')}

	def get_context_data(self, **kwargs):
		# get default context information
		context = super(PostActionsView, self).get_context_data(**kwargs)
		
		try:
			# render a limited view of the post
			post = Post.objects.only('pk', 'title', 'upload0').get(
				pk=self.kwargs.get('pk', None)
			)

		except Post.DoesNotExist:
			raise Http404()

		# add post and form(s) to the context
		context.update({ 
			'post': post, 
			'form': PostReportForm(initial=self.get_initial(**kwargs))
		})

		return context

# ----------------------------------------------------------------------------------------

