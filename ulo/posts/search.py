# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals
import re, regex, requests

# core django imports

# thrid party app imports

# project imports

from .utils import IMAGE, VIDEO
from generics.constants import (POST_INDICES_ALIAS, POST_INDEX_ALIAS, POST_SEARCH_ALIAS, 
	POST_INDEX_0, POST_TYPE_0, POST_SUGGEST_0)
from generics.utils import es, BulkProcessing
from generics.serialisers import serialiser




# POST DOCUMENT FIELDS
# ----------------------------------------------------------------------------------------

# ****************************************************************************************
# CALLBACK FUNCTION BELOW USES HARDCODED FIELD NAMES - SO UPDATE IT IF FIELDS CHANGE
# ****************************************************************************************

# all fields excluding id and user
ES_POST_FIELDS = ('title', 'tags', 'published', 'category',)
# fields that have been used to generate document mapping
ES_POST_MAPPED_FIELDS = ('title', )
# user data fields
ES_POST_USER_FIELDS = ('id', 'name', 'username')

ES_IMAGE_FIELD = 'thumbnail'
# name given to the user object on each post document
ES_USER_FIELD_NAME = 'user'
ES_USER_ID_FIELD_NAME = 'user.id'


# POST DOCUMENT SETTINGS
# ----------------------------------------------------------------------------------------

def _post_settings_v0():

	from users.search import user_analysers, NAME_ANALYSER
	
	# default post analyser
	analyser = 'post_analyser'
	# default search analyser
	search_analyser = 'post_search_analyser'

	# title field
	title_analyser = 'title_analyser'
	# title.stemmed field
	english_analyser = 'english_analyser'
	# tags field
	tags_analyser = 'tags_analyser'
	
	# extra analysers
	in_situ_analyser = 'in_situ_analyser'
	shingle_analyser = 'shingle_analyser'

	# INDEX ALIASES
	aliases = {
		# alias for all post indices
		POST_INDICES_ALIAS: {},
		# alias for all searchable post indices
		POST_SEARCH_ALIAS: {}
	}

	# INDEX CUSTOM ANALYSERS
	analysers = {
		analyser: {
			'type': 'custom',
			'tokenizer': 'whitespace',
			'filter': [
				'lowercase',
				'asciifolding_filter',
				'unique',
			]
		},
		search_analyser: {
			'type': 'custom',
			'tokenizer': 'whitespace',
			'filter': [
				'lowercase',
			]
		},
		title_analyser: {
			'type': 'custom',
			'tokenizer': 'whitespace',
			'filter': [
				'lowercase',
				'word_delimiter_filter',
				'asciifolding_filter',
				'unique_filter', # only_on_same_position: True
				'token_limit32_filter',
			]
		},
		english_analyser: {
			'type': 'custom',
			'tokenizer': 'whitespace',
			'filter': [
				'lowercase',
				'english_filter',
				'unique'
			]
		},
		tags_analyser: {
			'type': 'custom',
			'tokenizer': 'whitespace',
			'filter': [
				'word_delimiter_filter',
				'lowercase',
				'asciifolding_filter',
				'unique',
				'token_limit32_filter',
			]
		},
		# users name analyser: applied to the user object field
		NAME_ANALYSER: user_analysers[NAME_ANALYSER],

		# keeps the original stemmed words
		# in_situ_analyser: {
		# 	'tokenizer': 'whitespace',
		# 	'filter': [
		# 		'lowercase',
		# 		'keyword_repeat',
		# 		'kstem',
		# 		'unique'
		# 	]
		# },

		# shingle_analyser: {
		# 	'type': 'custom',
		# 	'tokenizer': 'whitespace',
		# 	'filter': [
		# 		'lowercase',
		# 		'shingle_filter',
		# 		'unique',
		# 	]
		# }
	}

	# INDEX MAPPINGS
	mappings = {

		'title': {
			'type':'string',
			'analyzer': title_analyser,
			'search_analyzer': search_analyser,
			'fields': {
				'stemmed': {
					'type': 'string',
					'analyzer': english_analyser
				},
			}
		},
		'tags': {
			'type':'string',
			'analyzer': tags_analyser,
			'search_analyzer': search_analyser,
			# set index_options to docs to not include term frequency (the number of times
			# a term appears in the text) in the score
			#'index_options': 'docs',
			# norms specify whether the field length impacts the score (i.e. shorter
			# fields have higher weights). Disable on analyzed fields if not needed 
			# as it consumes memory
			'norms': { 'enabled': False }
		},
		ES_USER_FIELD_NAME: {
			'type': 'object',
			'properties': {
				'name': {
					'type': 'string',
					'analyzer': NAME_ANALYSER,
					'search_analyzer': search_analyser,
				},
				'username': {
					'type': 'string',
					'analyzer': NAME_ANALYSER,
					'search_analyzer': search_analyser,
				},
				'id': {
					'type':'string',
					'index' : 'not_analyzed',
					'include_in_all': False
				}
			}
		},
		'views': {
			'type':'long',
			'index' : 'not_analyzed',
			'include_in_all': False
		},
		'published': {
			'type':'date',
			'index' : 'not_analyzed',
			'include_in_all': False,
		},
		'category': {
			'type':'string',
			'index' : 'not_analyzed',
			'include_in_all': False
		},
		'media_type': {
			'type':'string',
			'index' : 'not_analyzed',
			'include_in_all': False
		},
		'thumbnail': {
			'type':'string',
			'index' : 'not_analyzed',
			'include_in_all': False
		},
		POST_SUGGEST_0: {
			'type': 'completion',
			'analyzer': analyser,
			'search_analyzer': search_analyser,
			'payloads': True,
			'preserve_position_increments': False,
			'max_input_length': 50
		},
	}

	return aliases, analysers, mappings

# CREATE A NEW POST INDEX (VERSION 0)
# ----------------------------------------------------------------------------------------

def create_post_index(es):

	aliases, analysers, mappings = _post_settings_v0()

	index_mappings = {
		# make routing a requirement for posts to prevent the indexing of a doc on more 
		# than one shard.
		POST_TYPE_0: {
			'_routing': {
				'required': True 
			}
		},
	}

	# create the new index - raises an exception if fails.
	es.create_index(POST_INDEX_0, aliases=aliases, analysers=analysers, mappings=index_mappings)
	# map the new index to the current index alias - raises an exception if fails.
	es.replace_alias(POST_INDEX_ALIAS, index_remove='_all', index_add=POST_INDEX_0)
	# apply field mappings to the index and specific type - raises an exception if fails.
	es.mapping(index_type=POST_TYPE_0, mappings=mappings, indices=POST_INDEX_0)

	return True
	
# ----------------------------------------------------------------------------------------

def doc_mapping(doc, post):
	# split into groups of character consiting of letters, numbers and punctuation. 
	# regexp =  regex.compile(r'[\p{p}\w]+', regex.U)
	# split on whitespace
	# \u002d\u058A\u05be\u2010\u2011\u2012\u2013\u2014\u2015\ufe58\ufe63\uff0d #hyphens
	regexp = re.compile(r'\b[^\s\u200b]+\b', re.U)
	# parse the title
	title = ' '.join(regexp.findall(doc['title']))
	# a dictionay containing document mapping parameters/values
	params = { 'payload': {'id': post.pk} }
	# return document mapping parameters.
	return es.document_mapping([title,], POST_SUGGEST_0, **params)

# ----------------------------------------------------------------------------------------

def create_post(post, user, **kwargs):
	"""
	Call save() on a new post instance to commit it to the database and then create its 
	elasticsearch document. The two operations are atomic.
	@param post: new post instance.
	@param user: post creator. 
	"""

	# build the post document
	doc = {}
	for f in ES_POST_FIELDS:
		doc[f] = getattr(post, f).url if f == ES_IMAGE_FIELD else getattr(post, f)

	# build the user object held in each post document
	user_doc = {}
	for f in ES_POST_USER_FIELDS:
		user_doc[f] = getattr(user, f)

	# add the user object to the post document
	doc[ES_USER_FIELD_NAME] = user_doc
	

	# route post document based on the user's id
	kwargs['params'] = {'routing': str(user.pk)}
	kwargs['callback'] = doc_mapping
	return es.create_instance(post, doc, POST_INDEX_0, POST_TYPE_0, **kwargs)

# ----------------------------------------------------------------------------------------

def update_post(post, changed, **kwargs):
	"""
	"""

	if not changed:
		return post	

	doc = {}
	update_mapping = False

	#if any(field in ES_POST_FIELDS for field in changed):
	for f in ES_POST_FIELDS:
		if f in changed:
			doc[f] = getattr(post, f).url if f == ES_IMAGE_FIELD else getattr(post, f)

			if f in ES_POST_MAPPED_FIELDS:
				update_mapping = True

	if doc:
		if update_mapping:
			kwargs['callback'] = doc_mapping
		# route post document based on the user's id
		kwargs['params'] = {'routing': str(post.user.pk)}
		return es.update_instance(post, doc, POST_INDEX_0, POST_TYPE_0, **kwargs)

	return post
# ----------------------------------------------------------------------------------------

