"""
Accounts URL Configuration
The `urlpatterns` list routes URLs to views.
"""
# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.conf.urls import include, url

# thrid party app imports

# project imports
from . import views

# ----------------------------------------------------------------------------------------


urlpatterns = [

	# Post Form
	url(r'^$', views.PostFormView.as_view(), name='form'),
	# post only requests for videos
	url(r'^video/$', views.PostVideoFormView.as_view(), name='video_form'),

	url(r'^all/$', views.PostListView.as_view(), name='test'),

	# Users Post
	url(r'^(?P<pk>[\d]+)/$', views.PostView.as_view(), name='post'),
	url(r'^(?P<pk>[\d]+)/edit/$', views.PostEditView.as_view(), name='post_edit'),

	# Vote on a post
	url(r'^vote/$', views.PostVoteView.as_view(), name='vote'),


	# Post actions menu when JS is disabled
	url(r'^(?P<pk>[\d]+)/actions/$', views.PostActionsView.as_view(), name='actions'),
	# User reporting issue with post
	url(r'^report/complete/$', views.PostReportCompleteView.as_view(), name='report_complete'),
	url(r'^report/(?P<pk>[\d]+)/$', views.PostReportView.as_view(), name='report'),


]

# ----------------------------------------------------------------------------------------