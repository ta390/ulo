# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.conf import settings
from django.core import validators
from django.db import models
from django.utils import timezone
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes, force_text
from django.utils.translation import ugettext_lazy as _


# thrid party app imports

# project imports
from .utils import (
	CATEGORIES, COMMENT_SETTINGS, DISLIKE_CHAR, ENABLED, IMAGE, LIKE_CHAR, MEDIA_TYPES, 
	POST_REPORTS, VOTE_CHARS
)

# ----------------------------------------------------------------------------------------

class Post(models.Model):
	
	def file_path(self, filename):
		"""
		Create file path for the images/video in the user's post
		"""
		return 'posts/%s/%s' %(timezone.now().strftime("%m-%y"), filename)

	# POST (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------
	upload0 = models.FileField(
		_('add photos or video'),
		upload_to=file_path,
		default='default/post.jpg',
	)
	title = models.CharField(
		_('title'), 
		max_length=100, 
		blank=False,
		help_text=_('Title your post.'),
	)
	vote0 = models.CharField(
		_('option one'),
		max_length=30,
		choices=VOTE_CHARS,
		default=LIKE_CHAR,
		help_text=_('Select vote options one.'),
	)
	vote1 = models.CharField(
		_('option two'),
		max_length=30,
		choices=VOTE_CHARS,
		default=DISLIKE_CHAR,
		help_text=_('Select vote options two.'),
	)

	# END POST (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------


	# POST (OPTIONAL) FIELDS
	# ------------------------------------------------------------------------------------
	
	category = models.PositiveSmallIntegerField(
		_('categories'), 
		choices=CATEGORIES,
		blank=True,
		null=True,
		help_text=_('Categorize your post for better search results.'),
	)
	# VARCHAR is faster when searching the entire contents of a field
	tags = models.CharField(
		_('hash tags'), 
		max_length=400,
		blank=True,
		help_text=_('E.g. #London #Summer #PhotoOfTheDay'),
	)
	# VARCHAR is faster when searching the entire contents of a field
	description = models.CharField(
		_('description'), 
		max_length=600,
		blank=True,
		help_text=_('Tell others what your post is about.'),
	)

	# END POST (OPTIONAL) FIELDS
	# ------------------------------------------------------------------------------------


	# SETTINGS FIELDS
	# ------------------------------------------------------------------------------------
	
	comment_settings = models.PositiveSmallIntegerField(
		_('comment settings'),
		choices=COMMENT_SETTINGS,
		default=ENABLED
	)

	# location = models.CharField(
	# 	_('location'),
	# 	max_length=200,
	# 	blank=True,
	# 	help_text=_('Add a location to your post.'),
	# )

	# views = models.BigIntegerField(
	# 	_('views'),
	# 	blank=False,
	# 	default=0,
	# )

	# END SETTINGS FIELDS
	# ------------------------------------------------------------------------------------


	# ACCESS FIELDS
	# ------------------------------------------------------------------------------------
	
	is_removed = models.BooleanField(
		_('post removed'),
		default=False,
		help_text=_('This post has been removed due to reports of misconduct.'),
	)
	
	# END ACCESS FIELDS
	# ------------------------------------------------------------------------------------


	# INFO FIELDS
	# ------------------------------------------------------------------------------------
	published = models.DateTimeField(
		_('published'), 
		default=timezone.now,
	)
	# views = models.BigIntegerField(
	# 	_('views'),
	# 	default=0
	# )
	# media_type = models.PositiveSmallIntegerField(
	# 	_('media type'),
	# 	choices=MEDIA_TYPES,
	# 	default=IMAGE
	# )
	# END INFO FIELDS
	# ------------------------------------------------------------------------------------
	

	# DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------
	
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

	### Post has a one-to-many relations with PostReport.
	### Post has a one-to-many relations with PostVote.
	### Post has a one-to-many relations with Comment.

	# END DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------


	class Meta:
		verbose_name = _('post')
		verbose_name_plural = _('posts')
		ordering = ['-published']

	def __str__(self):
		return self.user.username+': '+self.title


# ----------------------------------------------------------------------------------------

class PostVote(models.Model):

	# POSTVOTE (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------
	vote = models.PositiveSmallIntegerField(
		_('vote')
	)
	# END POSTVOTE (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------

	# INFO FIELDS
	# ------------------------------------------------------------------------------------
	# published = models.DateTimeField(
	# 	_('published'), 
	# 	default=timezone.now,
	# )
	# END INFO FIELDS
	# ------------------------------------------------------------------------------------


	# DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------

	post = models.ForeignKey(Post, on_delete=models.CASCADE)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

	# END DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------


	class Meta:
		verbose_name = _('post vote')
		verbose_name_plural = _('post votes')
		unique_together = ('post', 'user')

	def __str__(self):
		return str(self.vote)


# ----------------------------------------------------------------------------------------

class PostReport(models.Model):
	"""
	Report abuse found in a post.
	"""

	# POSTREPORT (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------
	report = models.PositiveSmallIntegerField(
		_('report'),
		choices=POST_REPORTS,
		help_text=_('What is wrong with this post?')
	)
	# END POSTREPORT (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------


	# POSTREPORT (OPTIONAL) FIELDS
	# ------------------------------------------------------------------------------------
	information = models.TextField(
		_('additional information'),
		max_length=400,
		blank=True,
		help_text=_('Please provide any additional information that will help us resolve '
			'the issue.')
	)
	# END POSTREPORT (OPTIONAL) FIELDS
	# ------------------------------------------------------------------------------------


	# INFO FIELDS
	# ------------------------------------------------------------------------------------
	published = models.DateTimeField(
		_('published'), 
		default=timezone.now,
	)
	# END INFO FIELDS
	# ------------------------------------------------------------------------------------


	# DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------

	post = models.ForeignKey(Post, on_delete=models.CASCADE)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

	# END DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------


	class Meta:
		verbose_name = _('post report')
		verbose_name_plural = _('post reports')

	def __str__(self):
		return self.post.title+': '+self.report+' (by: '+self.user.username+')'


# ----------------------------------------------------------------------------------------

