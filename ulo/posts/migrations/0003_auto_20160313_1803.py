# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-13 18:03
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('posts', '0002_post_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostReport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('report', models.CharField(blank=True, choices=[('0', 'Spam'), ('1', 'Displays sensitive material'), ('2', 'Abusive, harassing or hateful'), ('3', 'Violent or threatening'), ('4', 'Distasteful or repulsive'), ('5', 'Copyright violation')], help_text='What is wrong with this post?', max_length=2, verbose_name='report')),
            ],
            options={
                'verbose_name': 'post report',
                'verbose_name_plural': 'post reports',
            },
        ),
        migrations.AddField(
            model_name='post',
            name='category',
            field=models.CharField(blank=True, choices=[('0', 'Animals'), ('1', 'Art & Design'), ('2', 'Comedy'), ('3', 'Education'), ('4', 'Entertainment'), ('5', 'Fashion'), ('6', 'Food'), ('7', 'Gaming'), ('8', 'Movies'), ('9', 'Music'), ('10', 'News'), ('11', 'Photography'), ('12', 'Sports'), ('13', 'Vlog')], help_text='Categorize your post for better search results.', max_length=2, verbose_name='categories'),
        ),
        migrations.AddField(
            model_name='post',
            name='comment_settings',
            field=models.CharField(choices=[('0', 'Enabled'), ('1', 'Anonymous'), ('2', 'Disabled')], default='0', max_length=2, verbose_name='comment settings'),
        ),
        migrations.AddField(
            model_name='post',
            name='description',
            field=models.CharField(blank=True, help_text='Tell others what your post is about.', max_length=600, verbose_name='description'),
        ),
        migrations.AddField(
            model_name='post',
            name='is_removed',
            field=models.BooleanField(default=False, help_text='This post has been removed due to reports of misconduct.', verbose_name='post removed'),
        ),
        migrations.AddField(
            model_name='post',
            name='tags',
            field=models.CharField(blank=True, help_text='E.g. #London #Summer #PhotoOfTheDay', max_length=400, verbose_name='hash tags'),
        ),
        migrations.AddField(
            model_name='postreport',
            name='post',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='posts.Post'),
        ),
        migrations.AddField(
            model_name='postreport',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
