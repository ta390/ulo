# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


# thrid party app imports

# project imports
from .utils import COMMENT_VOTE_VALUE
from posts.models import Post
from posts.utils import COMMENT_REPORTS


# ----------------------------------------------------------------------------------------

class Comment(models.Model):	

	# COMMENT (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------
	text = models.TextField(
		_('comment'), 
		max_length=200,
		help_text=_('Comment on the post'),
	)
	# END COMMENT (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------


	# COMMENT (OPTIONAL) FIELDS
	# ------------------------------------------------------------------------------------
	agree = models.PositiveIntegerField(
		_('agree'), 
		default=0,
	)
	disagree = models.PositiveIntegerField(
		_('disagree'), 
		default=0,
	)
	# END COMMENT (OPTIONAL) FIELDS
	# ------------------------------------------------------------------------------------


	# INFO FIELDS
	# ------------------------------------------------------------------------------------
	published = models.DateTimeField(
		_('published'), 
		default=timezone.now,
	)
	# END INFO FIELDS
	# ------------------------------------------------------------------------------------
	

	# DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------
	
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	post = models.ForeignKey(Post, on_delete=models.CASCADE)
	### Comment has a one-to-many relations with CommentVote.

	# END DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------
	

	class Meta:
		verbose_name = _('comment')
		verbose_name_plural = _('comments')
		#ordering = ['-published']

	def __str__(self):
		return self.text + ' ' + str(self.published)

# ----------------------------------------------------------------------------------------

class CommentVote(models.Model):

	# COMMENTVOTE (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------
	vote = models.PositiveSmallIntegerField(
		_('vote'),
		default = COMMENT_VOTE_VALUE
	)
	# END COMMENTVOTE (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------

	# INFO FIELDS
	# ------------------------------------------------------------------------------------
	# published = models.DateTimeField(
	# 	_('published'), 
	# 	default=timezone.now,
	# )
	# END INFO FIELDS
	# ------------------------------------------------------------------------------------


	# DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------

	comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

	# END DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------


	class Meta:
		verbose_name = _('comment vote')
		verbose_name_plural = _('comment votes')
		unique_together = ('comment', 'user')

	def __str__(self):
		return str(self.vote)

# ----------------------------------------------------------------------------------------

class CommentReport(models.Model):
	"""
	Report abuse found in a comment.
	"""

	# COMMENTREPORT (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------
	report = models.PositiveSmallIntegerField(
		_('report'),
		choices=COMMENT_REPORTS,
		help_text=_('What is wrong with this comment?')
	)
	# END COMMENTREPORT (REQUIRED) FIELDS
	# ------------------------------------------------------------------------------------

	# INFO FIELDS
	# ------------------------------------------------------------------------------------
	published = models.DateTimeField(
		_('published'), 
		default=timezone.now,
	)
	# END INFO FIELDS
	# ------------------------------------------------------------------------------------


	# DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------

	comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

	# END DB RELATIONSHIPS
	# ------------------------------------------------------------------------------------


	class Meta:
		verbose_name = _('comment report')
		verbose_name_plural = _('comment reports')

	def __str__(self):
		return self.comment.text+': '+self.report+' (by: '+self.user.username+')'


# ----------------------------------------------------------------------------------------
