# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django import forms
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports
from .models import Comment, CommentReport
from posts.utils import COMMENT_REPORTS
from ulo.forms import UloModelForm

# USER UPLOAD FORMS
# ----------------------------------------------------------------------------------------

class CommentPostForm(UloModelForm):

	class Meta:
		model = Comment
		fields = ('text',)

	def __init__(self, *args, **kwargs):
		self.user_id = kwargs.pop('user_id', None)
		self.post_id = kwargs.pop('post_id', None)
		super(CommentPostForm, self).__init__(*args, **kwargs)

	def save(self, commit=True):
		# assign the user to post ids to the comment instance being created
		self.instance.post_id = self.post_id
		self.instance.user_id = self.user_id
		return super(CommentPostForm, self).save(commit=commit)

# ----------------------------------------------------------------------------------------

class CommentReportForm(UloModelForm):

	comment_id = forms.CharField(
		widget=forms.HiddenInput(),
		error_messages={
			'required': _('We could not determine which comment you are looking to report.')
		}
	)

	report = forms.ChoiceField(
		widget=forms.RadioSelect, 
		choices=COMMENT_REPORTS,
		initial=0,
		help_text=_('What is wrong with this comment?')
	)

	class Meta:
		model = CommentReport
		fields = ('report',)

	def __init__(self, *args, **kwargs):
		self.user_id = kwargs.pop('user_id', None)
		super(CommentReportForm, self).__init__(*args, **kwargs)

	def save(self, commit=True):
		# assign the user to comment ids to the comment report instance being created
		self.instance.comment_id = self.cleaned_data['comment_id']
		self.instance.user_id = self.user_id
		return super(CommentReportForm, self).save(commit=commit)

# ----------------------------------------------------------------------------------------

