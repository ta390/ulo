# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------

# standard library imports
from __future__ import unicode_literals

# core django imports

# thrid party app imports

# project imports


# COMMENT VOTE
# ----------------------------------------------------------------------------------------

# the value to give a comment vote entry to signal that a user 'liked' a comment
COMMENT_VOTE_VALUE = 1

# ----------------------------------------------------------------------------------------
