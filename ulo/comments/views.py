# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.translation import ugettext_lazy as _

# thrid party app imports

# project imports
from .forms import CommentPostForm, CommentReportForm
from .models import Comment, CommentReport, CommentVote
from ulo.sessions import LinkViewsSession
from ulo.utils import get_referer_path
from ulo.views import UloView, UloFormView, UloUploadView


# ----------------------------------------------------------------------------------------

class CommentPostView(UloFormView):

	form_class = CommentPostForm

	def get(self, request, *args, **kwargs):
		raise Http404()

	def get_success_url(self):
		return get_referer_path(self.request)

	def get_form_kwargs(self):
		kwargs = super(CommentPostView, self).get_form_kwargs()
		if self.request.method in ('POST', 'PUT'):
			kwargs.update({ 
				'user_id': self.request.user.pk, 
				'post_id': self.request.POST.get('post_id', None) 
			})
		return kwargs

	def post(self, request, *args, **kwargs):
		if not request.is_ajax():
			messages.error(request, _('Please enable javascript to comment on this post.'))
			login_required = not request.user.is_authenticated()
			return redirect( get_referer_path(request, redirect=login_required) )

		return super(CommentPostView, self).post(request, *args, **kwargs)

	def form_valid(self, form):
		form.save()
		return super(CommentPostView, self).form_valid(form)

# ----------------------------------------------------------------------------------------

class CommentVoteView(UloView):
	"""
	Add or remove a user's vote from a comment.
	"""

	def get(self, request, *args, **kwargs):
		raise Http404()

	def post(self, request, *args, **kwargs):
		# redirect user to login page if not already logged in.
		if not request.user.is_authenticated():
			return redirect( get_referer_path(request, redirect=True) )

		# toggle a user's vote on a comment by deleting it or creating it
		try:
			comment_id = request.POST['comment_id']
			CommentVote.objects.get(comment_id=comment_id, user_id=request.user.pk).delete()
		except CommentVote.DoesNotExist:
			CommentVote.objects.create(comment_id=comment_id, user_id=request.user.pk)

		return redirect( get_referer_path(request) )

# ----------------------------------------------------------------------------------------

class CommentReportView(LoginRequiredMixin, UloFormView):
	
	template_name = 'comments/report.html'
	form_class = CommentReportForm

	def get_success_url(self):
		LinkViewsSession(self.request).set('comment_report', True)
		return reverse('comments:report_complete')

	def get(self, request, *args, **kwargs):
		pk = kwargs.get('pk', None)
		if Comment.objects.filter(pk=pk).exists():
			self.initial = {'comment_id': pk}
			return super(CommentReportView, self).get(request, *args, **kwargs)
		raise Http404()

	def get_form_kwargs(self):
		kwargs = super(CommentReportView, self).get_form_kwargs()
		if self.request.method in ('POST', 'PUT'):
			kwargs.update({ 'user_id': self.request.user.pk })
		return kwargs

	def form_valid(self, form):
		form.save()
		return super(CommentReportView, self).form_valid(form)


# ----------------------------------------------------------------------------------------

class CommentReportCompleteView(LoginRequiredMixin, UloView):
	
	template_name = 'comments/report_complete.html'

	def get(self, request, *args, **kwargs):
		if LinkViewsSession(request).get('comment_report'):
			return super(CommentReportCompleteView, self).get(request, *args, **kwargs)
		raise Http404()

# ----------------------------------------------------------------------------------------
