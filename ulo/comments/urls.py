"""
Accounts URL Configuration
The `urlpatterns` list routes URLs to views.
"""
# LIBRARY IMPORTS
# ----------------------------------------------------------------------------------------
# standard library imports
from __future__ import unicode_literals

# core django imports
from django.conf.urls import include, url

# thrid party app imports

# project imports
from . import views

# ----------------------------------------------------------------------------------------


urlpatterns = [

	# Post a comment
	url(r'^post/$', views.CommentPostView.as_view(), name='post'),
	
	# Vote on a comment
	url(r'^vote/$', views.CommentVoteView.as_view(), name='vote'),

	# Report a comment
	url(r'^report/complete/$', 
		views.CommentReportCompleteView.as_view(), name='report_complete'),
	url(r'^report/(?P<pk>[\d]+)/$', views.CommentReportView.as_view(), name='report'),

]

# ----------------------------------------------------------------------------------------